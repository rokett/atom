---
title: "Configuring the Worker"
weight: 3
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

The following flags are available for configuring the worker.

| Flag                      | Description                                                                                                                                                          | Default Value                                                           | Required |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | -------- |
| application.environmment  | Name of the environment the application is running in; for example test, development or production                                                                   | dev                                                                     | No       |
| default_user              | Default user to run jobs as, if not whatever the worker is running as                                                                                                |                                                                         | No       |
| nats.host                 | The name or IP address that the NATS server is available on                                                                                                          | 127.0.0.1                                                               | No       |
| nats.port                 | The port that the NATS server is listening on                                                                                                                        | 4222                                                                    | No       |
| nats.nkey_seed            | The NKey seed for the Atom user.  Recommended to store in an environment variable which is passed into the flag.  The seed should be kept securely as it is private. |                                                                         | No       |
| job.groups                | Comma separated list of job groups that worker will listen for                                                                                                       |                                                                         | No       |
| job.timeout               | Set a default timeout after which a job should be forcibly cancelled; for example 1h for 1 hour.  Valid units are 's', 'm' or 'h'.                                   | 1h                                                                      | No       |
| execution.logs_path       | Path to folder where execution logs should be stored                                                                                                                 | `./execution logs` on Windows.  `/var/log/atom/execution_logs` on Linux | No       |
| ps.scripts_path           | Path to the folder where PowerShell scripts are stored                                                                                                               | `./`                                                                    | No       |
| vault.address             | Address of the Vault API where secrets are stored                                                                                                                    | http://127.0.0.1:8200                                                   | No       |
| vault.token               | Token for read-only access to Vault                                                                                                                                  | `dev-only-token`                                                        | No       |
| vault.secrets_engine_path | Path to the secrets engine K/V store                                                                                                                                 | `secrets`                                                               | No       |
| vault.secrets_path        | Path to secrets stored for use by Atom, once within the secrets engine                                                                                               | `atom`                                                                  | No       |
| version                   | Display the version number only                                                                                                                                      | false                                                                   | No       |

To enable debug logging, set an environment variable called `atom_debug` to anything you want and restart the worker; the value is not important, just that the environment variable exists.

# Logging job executions
The `stdout` and `stderr` output of job executions are logged to timestamped log files.  Before running the worker, create a folder called `execution_logs` in the folder that the worker resides in.

You will want to setup some housekeeping of the `execution_logs` folder to ensure the log files do not eventually fill up the disk.
