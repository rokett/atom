---
title: "Metrics"
weight: 6
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

Metrics are available in Prometheus format.

# Server

| Metric                          | Description                                | Type |
| ------------------------------- | -----------                                | ------------- |
| atom_server_job_runs_total      | Counts the total number of job runs; dimensioned by script, job type (ad-hoc or scheduled), and job status.<br><br>Jobs failed because no workers are available may indicate that more workers are required, or that there is a problem with the workers.  This metric can also be used as a rudimentary dead mans switch assuming you expect to see some jobs run in x hours, by looking for 0 increase in that time.        | Counter |
| atom_server_jobs_running        | Indicates the number of jobs currently running | Gauge |
| atom_server_jobs_queued        | Indicates the number of jobs currently queued | Gauge |
| atom_server_job_duration_seconds | Tracks the duration of jobs, partitioned by the script name | Histogram |
| atom_server_workers_total       | Online workers per group.<br><br>Can be used to alert on not having enough online workers for the different groups you need. | Gauge |
| atom_server_worker_status | Dimensioned by worker name; is the worker idle (0), busy (1) or in maintenance mode (2)? | Gauge |
| atom_server_maintenance_mode    | Tracks maintenance mode status; 1 if enabled and 0 if disabled | Gauge |
