---
title: "Receiving job events"
weight: 5
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

When a job finishes an event will be fired, targetting any previously configured event targets.

Event targets can be configured by sending the following request.

```
POST http://localhost:9999/api/config/event_targets
{
    "name": "Alerting",
    "target": "http://localhost:2314/event/",
    "event_type": "*"
}
```

In this instance an event target called `Alerting` is configured to receive an event for any job completion as denoted by the `event_type` being a wildcard, `*`.  The server will send a `POST` request to the URL configured in the `target` field with a the job execution details as the JSON payload.

```
{
    "id": 195,
    "script": "svc.ps1",
    "execution_id": "7c17e028-bca2-3537-32cb-b0d4859b78dd",
    "parameters": "-Name wsearch -verbose",
    "executor": "powershell",
    "scope": "powershell:@any",
    "status": "COMPLETED_WITH_WARNINGS",
    "execution_time_ms": 5873,
    "execution_requested_by": "name",
    "execution_username": "",
    "client_ip": "",
    "worker_name": "sweet-resonance",
    "worker_ip": "192.168.1.72",
    "worker_server_name": "PC",
    "worker_os": "windows",
    "trace_id": "",
    "started_at": "2021-02-14T10:31:42Z",
    "finished_at": "2021-02-14T10:31:48Z",
    "created_at": "0001-01-01T00:00:00Z",
    "updated_at": "0001-01-01T00:00:00Z",
    "log": "here is the job log"
}
````

Valid events to listen to are:

- `*`
- `job.complete`
- `job.complete.warning`
- `job.error`
- `job.unauthorised_script`
- `job.error_starting`
- `job.expired`
- `job.cancelled_timeout`
