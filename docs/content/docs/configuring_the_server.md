---
title: "Configuring the Server"
weight: 2
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

The following flags are available for configuring the server.

| Flag                         | Description                                                                                                                                                          | Default Value          | Required?                              |
| ---------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | -------------------------------------- |
| application.environmment     | Name of the environment the application is running in; for example test, development or production                                                                   | dev                    | No                                     |
| log.file                     | Name of file to alog Atom events to                                                                                                                                  | Log to console         | No                                     |
| nats.host                    | IP address/FQDN of NATS server to connect to                                                                                                                         | 127.0.0.1              | No                                     |
| nats.nkey_seed               | The NKey seed for the Atom user.  Recommended to store in an environment variable which is passed into the flag.  The seed should be kept securely as it is private. |                        | No                                     |
| nats.server_port             | Port that NATS server is listening for clients on                                                                                                                    | 4222                   | No                                     |
| nats.stream_replicas         | How many stream replicas should Atom create?                                                                                                                         | 1                      | No                                     |
| api.port                     | The port that the server will listen on for API and web requests                                                                                                     | 9999                   | No                                     |
| cors.allowed_origins         | List of allowed origins for CORS purposes                                                                                                                            | CORS is disabled       | No                                     |
| cors.allowed_headers         | List of allowed headers for CORS purposes                                                                                                                            | *                      | No                                     |
| db.host                      | The IP address of the database host, including instance name if needed                                                                                               | 127.0.0.1              | No                                     |
| db.port                      | Port to connect to database server on                                                                                                                                | 1433                   | No                                     |
| db.name                      | Name of the database to use                                                                                                                                          | atom                   | No                                     |
| db.username                  | Username of account with read/write access to database                                                                                                               |                        | Yes - If not using integrated security |
| db.password                  | Password for database user                                                                                                                                           |                        | Yes - If not using integrated security |
| auth.hosts                   | Comma separated string of LDAP hosts for authentication purposes                                                                                                     |                        | Yes                                    |
| auth.base_dn                 | Base LDAP DN to search for user to be authenticated                                                                                                                  |                        | Yes                                    |
| auth.bind_dn                 | Distinguished name of user account to use to search the LDAP directory                                                                                               |                        | Yes                                    |
| auth.bind_pw                 | Password for the user used to search the LDAP directory                                                                                                              |                        | Yes                                    |
| auth.port                    | Port to connect to LDAP directory                                                                                                                                    | 389                    | No                                     |
| auth.use_ssl                 | Use a secure connection?                                                                                                                                             | false                  | No                                     |
| auth.start_tls               | Initialise secure connection using StartTLS                                                                                                                          | false                  | No                                     |
| auth.ssl_skip_verify         | Skip verification of certificate when using a secure connection                                                                                                      | false                  | No                                     |
| disable_stale_worker_cleanup | Disable the scheduled job to cleanup stale workers                                                                                                                   | false                  | No                                     |
| version                      | Display the version number only                                                                                                                                      | false                  | No                                     |
| expire_queued_job_after      | Set a default length of time after which a job should not be processed (s, m or h)                                                                                   | 720h                   | No                                     |

To enable debug logging, set an environment variable called `atom_debug` to anything you want and restart the server; the value is not important, just that the environment variable exists.

# Configuring users and roles
There are really only two roles in Atom; `user` or `admin`.  The only real difference between the two is that only `admins` can manage scheduled jobs and update the allowed scripts.

There are two different types of users, real people and application users.  Application users are created in order to allow other applications to send requests to execute a job by passing an application token in the `VND.atom.application_token` header.  Create them by sending a `POST` request to the `/config/application_users` endpoint; see the [API]({{< relref "getting_started#api" >}} "API") documentation for more details.

Users are managed by groups in your directory.  Good practice would be to create two groups, `atom_users` and `atom_admins` for example, and to populate those groups with users.  Admin users can be a member of both groups, although it is not necessary; the priority assigned to the roles will ensure admin permissions are applied properly.

Now you can add those groups to the `roles` table.  Add the distinguished name of the group, the role you want to give it, and the priority.  Ensure that the `admin` group has priority `1` with the `user` group having priority `2`.

# Configuring allowed scripts
Only scripts defined in the server are allowed to be run.  It's not foolproof as an admin, or anyone with write access to the databsae, can add whatever script they want, but it helps to ensure that rogue scripts cannot be run.

To add a new script you need to send a request to the API to update the list.  The request **must** include all allowed scripts as it will overwrite them all.

So an update process looks like this.

1. Get all of the existing allowed scripts.

```
GET http://localhost:9999/api/config/approved_scripts

returns

[
    "script1.ps1",
    "script2.ps1",
    "script3.ps1"
]
```

2. Form a new JSON request using the response and whatever new scripts you want to add.  To remove a script, just leave it out of the next request.

3. Send an update request to the API to, in this example, remove `script2.ps1` and add `script4.ps1`.

```
PUT http://localhost:9999/api/config/approved_scripts

[
    "script1.ps1",
    "script3.ps1",
    "script4.ps1"
]
```

New scripts need to exist on the workers before they can be run; this is intentionally an out of band process.

It is not strictly necessary to remove scripts which are no longer allowed, but it would be good practice to do so.

# CORS
If you need to call the API from a different URL via a browser, you need to enable CORS; by default CORS is disabled for security reasons.

Enabling CORS is as simple as passing in the allowed origins via the `cors_allowed_origins` flag.

```
server.exe --cors_allowed_origins "http://origin1,http://origin2"
```

You can also pass in `*` as a wildcard to allow any origin, although this isn't recommended from a security perspective.

By default any header is allowed, but you can restrict the allowed headers by listing them using the `cors_allowed_headers` flag.
