---
title: "Scheduling Jobs"
weight: 4
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

Jobs are scheduled using a Cron syntax, but run by the Atom scheduler.  There is no need to explain cron here, use [Crontab Guru](https://crontab.guru/) to define and validate your cron expression.

# Predefined schedules
You may also use one of several pre-defined schedules in place of a cron expression.

| Entry                  | Description                                | Equivalent To |
| -----                  | -----------                                | ------------- |
| @yearly (or @annually) | Run once a year, midnight, Jan. 1st        | 0 0 1 1 *     |
| @monthly               | Run once a month, midnight, first of month | 0 0 1 * *     |
| @weekly                | Run once a week, midnight between Sat/Sun  | 0 0 * * 0     |
| @daily (or @midnight)  | Run once a day, midnight                   | 0 0 * * *     |
| @hourly                | Run once an hour, beginning of hour        | 0 * * * *     |

# Intervals
You may also schedule a job to execute at fixed intervals, starting at the time it's added
or cron is run. This is supported by formatting the cron spec like this:

```
@every <duration>
```

where `duration` is a string accepted by `time.ParseDuration` (http://golang.org/pkg/time/#ParseDuration).

For example, `@every 1h30m10s` would indicate a schedule that activates after 1 hour, 30 minutes, 10 seconds, and then every interval after that.

**Note** The interval does not take the job runtime into account.  For example, if a job takes 3 minutes to run, and it is scheduled to run every 5 minutes, it will have only 2 minutes of idle time between each run.

**Note** Be aware of clock changes.  Although they only happen twice a year (in the UK), if you have a job scheduled for 00:30 (as an example) it will actually run twice during the clock change.  Unless you really need it to run between those hours, or it doesn't matter if it runs twice, schedule jobs for outside of those times.
