---
title: "Getting Started"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

You should probably read about the [architecture](/architecture) before starting things up.

# Let's go
What follows is a basic getting started guide.  Check the server and worker configuration documentation for information on the flags available to be used.

## Pre-requisites
Atom requires a database, currently only MS SQL Server, an external NATS service , and an LDAP directory for authentication, MS Active Directory.

## NATS Configuration
You need to create an account in NATS for Atom.  Download the NK tool from https://github.com/nats-io/nkeys/nk or, if you have Go installed, you can run `go install github.com/nats-io/nkeys/nk@latest` to install it directly.

Generate a user NKey by running `nk -gen user -pubout`.  The first output line starts with the letter S for Seed. Seeds are private keys; you should treat them as secrets and guard them with care.  The second line starts with the letter U for User, and is the public key which can be safely shared.

Edit the NATS config file, or create a new one if you don't have one already, and add an Atom account to the accounts map, using the public user key you just generated.

```
accounts: {
    Atom: {
        jetstream: enabled,
        users: [
            { nkey: UCOWSP6PT77DSIKIZV6KBEE4GFAD7L2ZMOBVTGDLVKKBTYS6LZQNUBQN }
        ]
    },
}
```

If this is a brand new config file, you'll also need to add the `jetstream` map at the top of the file to enable JetStream; See https://docs.nats.io/running-a-nats-service/configuration#jetstream for configuration options.

```
jetstream {}
```

## Database configuration
Create a blank database. The default name is `Atom` but you can call it whatever you want and pass the name in to the server using a flag, `--db.name`, if you wish.

Now you can create a local database user with read/write access to the database.  The username and password are passed to the server using the `--db.username` and `--db.password` flags respectively.

## LDAP authentication configuration
Atom hands off authentication to an LDAP directory.  Create a service account in your LDAP directory for authentication purposes; no special permissions are required.  You'll need the distinguished name of the account to pass to the server.  The DN and password can be passed in using the `--auth.bind_dn` and `--auth.bind_pw` respectively.

## Atom server configuration
Add an environment variable to store the NKey seed in; for this example it is stored in an env var called `ATOM_NKEY_SEED`.  Remember to protect the seed; ideally the environment variable should be populated dynamically from a secret store.

Before you can run Atom for real, you need to run the executable in order to create the database struture and validate configuration.  Here's an example command.

```
server.exe --application.environment "production" --auth.base_dn "OU=Users,DC=my,DC=domain" --auth.bind_dn "CN=Atom_Ldap,CN=Users,DC=my,DC=domain" --auth.bind_pw "my-really-strong-password" --auth.hosts "192.168.1.22" --db.username "atom" --db.password "another-strong-password" --nats.host "nats.my.domain" --nats.server_port 4222 --nats.nkey_seed %ATOM_NKEY_SEED%
```

We need to configure the initial permissions now; that requires delving into the DB.  Edit the `roles` table and add the distinguished name of two groups from your LDAP directory, one for `public` access and one for `admin` access.  Ensure the priority on the `admin` group is set to `1`.  Admin permissions allow for managing schedules and approved scripts.

You'll probably want to run Atom as a service, in which case you need to use something like [NSSM](https://nssm.cc/) to execute the command shown above.

## Atom worker configuration
Add an environment variable to store the NKey seed in; for this example it is stored in an env var called `ATOM_NKEY_SEED`.  Remember to protect the seed; ideally the environment variable should be populated dynamically from a secret store.

Create a folder called `execution_logs` in the same folder that the worker executable resides in, and ensure the user account running the worker has permissions to write into the directory.

Now you'll want to run the workers as a service too.  See an example command below which will configure the worker to act as a generic PowerShell, catch-all, worker.

```
worker.exe --nats.host "nats.mydomain.com" --nats.nkey_seed %ATOM_NKEY_SEED% --job.groups "powershell"
```

If you want to run a worker to support Ansible, the command should look like so.

```
/path/to/worker --nats.host "nats.mydomain.com" --nats.nkey_seed $ATOM_NKEY_SEED --job.groups "ansible"
```

Validate that the worker(s) have checked in with the server by opening the interface in a web browser, http://localhost:9999/ if using the default port, and selecting the `workers` page.

Ensure that any required PowerShell modules and scripts, or Ansible playbooks and supporting files, are deployed to the workers.  You'll want to work out a way of ensuring these are deployed and updated; this is intentionally an out of band process.  By default the scripts live in the same folder as the worker executable, but that can be changed by configuring the worker with the `ps_scripts_path` flag.

## Application configuration
Scripts/Playbooks you want the workers to run need to be explicitly allowed via the server; this helps to ensure rogue jobs cannot be run.  Send a request to the API like so to define the allowed scripts.

```
PUT http://localhost:9999/api/config/approved_scripts

[
    "script1.ps1",
    "script2.ps1",
    "script3.ps1",
    "/tmp/playbook1.yml",
    "/tmp/playbook2.yml"
]
```

## First job execution
You're now ready to execute your first job.  Send a request to the API like so to run a script on any listening worker.

```
POST http://localhost:9999/api/jobs/execute

{
    "script": "script2.ps1",
    "parameters": "-Param1 xxx -Param2 yyy",
    "scope": "powershell:@any",
    "executor": "powershell"
}

or

{
    "script": "/tmp/playbook2.yml",
    "parameters": "--limit server1,server2",
    "scope": "ansible:@any",
    "executor": "ansible"
}

or

{
    "script": "script2.ps1",
    "parameters": "-Param1 xxx -Param2 yyy",
    "scope": "powershell:@any",
    "executor": "pwsh"
}

or

{
    "script": "c:\path\to\binary.exe",
    "parameters": "--Param1 xxx --Param2 yyy",
    "scope": "binary:@any",
    "executor": "binary"
}
```

## Configuring scheduled jobs
Finally we can setup a scheduled job via the GUI; note that only users with admin privileges can setup a new scheduled job.

Log in to http://localhost:9999/ and select the `Scheduled Job` page.  Add a new job by entering the script name, parameters, and schedule.  The schedule is in cron format; [Crontab Guru](https://crontab.guru/) is a good resource for checking your cron string is correct.  There are also a number of preset options which are explained in [Scheduling Jobs]({{< relref "scheduling_jobs" >}} "Cron").

# Targeting jobs against workers
Jobs can be targeted at specific workers through the use of the `scope` parameter when executing a job.

| Scope                     | Description                                                                |
| ------------------------- | -------------------------------------------------------------------------- |
| `@all`                    | Publishes job to **all** workers                                           |
| `<groupname>:@any`        | Publishes jobs to **any** worker registered with the specified group name  |
| `<groupname>:@all`        | Publishes jobs to **all** workers registered with the specified group name |
| `<hostname1>,<hostname2>` | Publishes jobs to **one** random worker on **each** specified host         |

## @all
![@all](../../job_scoping_all.png "@all")

## group:@any
![group:@any](../../job_scoping_group_any.png "group:@any")

## group:@all
![group:@all](../../job_scoping_group_all.png "group:@all")

## hostnames
![hostnames](../../job_scoping_hostname.png "hostnames")

# Executing a job
Executing a job is either done via an API request or by schedule.

The below request will execute `script2.ps1 -param1 xxx -param2 yyy` on any listening worker. See the [API](#api) documentation for full details.

```
POST http://localhost:9999/api/jobs/execute

{
    "script": "script2.ps1",
    "parameters": "-Param1 xxx -Param2 yyy",
    "scope": "powershell:@any",
    "executor": "powershell"
}
```

When the server receives the request, the following process is initiated.  You can view status updates via the GUI.  When the job finishes an event is fired against any previously specified targets; see [Receiving job events]({{< relref "receiving_job_events" >}}) for more information.

See [Targeting jobs against workers](#targeting-jobs-against-workers) for details around how to set the scope to target workers.

![Job execution](../../job_execution.png "Job execution")

# API
Browse to the Atom frontend and click on the link to go to the API Docs.

# Development
Almost all dependencies are version controlled, so contributing to development is as easy as pulling the repo locally.
