---
title: "Filtered consumer not unique on workqueue stream"
weight: 1
---

## Symptoms
When starting a worker, you may find that it immediately exits with the error `nats: filtered consumer not unique on workqueue stream`; the error log entry would look similar to the following, with the subject you are subscribing to depending on the job group.

```json
{
   "level":"fatal",
   "ts":"2023-02-01T12:36:59.288Z",
   "caller":"worker/main.go:258",
   "msg":"unable to subscribe to 'atom_stream.job:execute:powershell:@any' queue",
   "app":"Atom",
   "version":"0.21.0",
   "build":"e9fc7e502d6c6af9dfcab263daff885d3334d28f",
   "error":"nats: filtered consumer not unique on workqueue stream",
   "stacktrace":"main.main\n\tC:/Development/atom/cmd/worker/main.go:258\nruntime.main\n\tC:/Program Files/Go/src/runtime/proc.go:250"
}

```

## Cause
The problem here is that there is already a consumer (worker) registered with a different name.  What is meant by name here isn't the random worker name that is generated, it is the job group name.

For example, for the subject `atom_stream.job:execute:powershell:@any`, all workers which are configured to use the `powershell` job group will subscribe to the subject using the consumer name `powershell`.

If a consumer has already subscribed to that subject, but using a different consumer name, the error will appear because it would result in two consumers on the same filtered subject with different names; streams using the `WorkQueuePolicy` retention policy do not allow this (see https://pkg.go.dev/github.com/nats-io/nats.go#RetentionPolicy).

This isn't an error that should be able to happen as the job group name is used as the subject and cosumer name, but it could potentially occur during development or testing.

## Resolution
The problem is some old consumers, using an incorrect naming convention, are registered to the stream so the fix is to remove them.

You can do this using the `nats` CLI.

```bash
# Find which consumers are using the filtered subject
nats.exe --server x.x.x.x:xxxx consumer info

# Delete the consumers you don't want
nats.exe --server x.x.x.x:xxxx consumer rm
```

Now restart the worker(s) and all should be good.
