---
title: "Failed to reconnect to NATS server"
weight: 2
---

## Symptoms
Atom is online but has stopped processing jobs. Jobs can be submitted, and scheduled jobs will try to run, but they will all have a queued status.

Both the Atom server and workers will log an error with the message `Failed to reconnect to NATS server; stopping attempts`. For example:

```json
{
    "level": "error",
    "ts": "2023-03-22T11:44:26.273Z",
    "caller": "server/setup_nats_client.go:34",
    "msg": "Failed to reconnect to NATS server; stopping attempts",
    "app": "Atom",
    "version": "0.24.0",
    "build": "655db4b195000754d8e44fd2c84b1b2df614c2df",
    "error": "nats: no servers available for connection",
    "stacktrace": "main.setupNATSClient.func3\n\tC:/Development/atom/cmd/server/setup_nats_client.go:34\ngithub.com/nats-io/nats%2ego.(*Conn).close.func3\n\tC:/evelopment/atom/vendor/github.com/nats-io/nats.go/nats.go:5029\ngithub.com/nats-io/nats%2ego.(*asyncCallbacksHandler).asyncCBDispatcher\n\tC:/Development/atom/vendor/github.com/nats-io/nats.go/nats.go:2796"
}
```

## Cause
The Atom server and workers talk to each other via NATS. If NATS is unavailable for any reason, jobs will queue whilst the server and worker attempts to reconnect. They will retry for 10 minutes before stopping with the above error.

At this point only restarting the Atom services will cause Atom to attempt to reconnect.

## Resolution
Assuming NATS is online, fix that problem first if it is not, restart the Atom server service followed by the Atom worker services.

You should find that the workers register themselves and queued jobs start to be processed.

Given that NATS is designed, and should be implemented, to be highly available, ensure you open an investigation into why Atom wasn't able to connect.
