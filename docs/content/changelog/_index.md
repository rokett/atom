# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.31.1] - 2025/01/29
### Fixed
- #223 Fixed validation error when trying to add an event target for the `job.cancelled_timeout` event.

## [0.31.0] - 2025/01/28
### Added
- #222 Added option to send an event for jobs that have been cancelled due to a job timeout (`job.cancelled_timeout`).

## [0.30.0] - 2024/12/01
### Added
- #220 Displaying server version and build reference on admin screen.

## [0.29.0] - 2024/11/18
### Changed
- #216 Ensured that logging is in JSON format if the `application.environment` flag is set to anything other than `dev`.  In `dev` mode the logging is output using the slog `TextHandler` format.
- #217 If the worker is running in a `dev` environment, we want to be able to see the job output in the terminal. If it is running in any other environment, we want to hide it.  If we don't, whatever sucks up the logs from stdout will also suck up the unstructured job output which will more than likely fail to be stored, or could cause an issue storing any of the structured logs.

## [0.28.0] - 2024/11/02
### Added
- #183 It is now possible to cancel a queued job; a new button appears in the UI if a job is not complete which, when clicked, will cancel it.
- #188 It is now possible to cancel a running job; a new button appears in the UI if a job is not complete which, when clicked, will cancel it.
- #191 Added the following new metrics.

  - `atom_server_jobs_running` - Total number of job currently running
  - `atom_server_jobs_queued` - Total number of jobs currently queued
  - `atom_server_job_duration_seconds` - Duration of jobs, partitioned by the script name
  - `atom_server_worker_status` - Is worker idle (0) or busy (1) or in maintenance mode (2)
- #204 Given that the maximum message size in NATS is 1MB, and allowing for metadata on top of any message Atom generates, we now log an error if the job update payload being sent from the worker to NATS is more than 900KB in size.

  Longer term the message would need to be chunked, but for now we'll watch for those errors to determine how soon refactoring needs to happen.
- #212 Added `command` and `requestor` fields to log entries written by the worker when executing a job.

### Changed
- #206 Refactored to remove the deprecated nats.EncodedConn type.

### Fixed
- #213 Resolved a problem where API docs were not displaying the schema definition for endpoints correctly.

## [0.27.0] - 2024/09/20
### Added
- #186 New frontend option to run a scheduled job from the scheduled jobs detail page.  The job will be executed as per the scheduled definition.
- #189 Refactored job execution in order to capture the Process ID of the command being executed and log it.  The Process ID is now also logged in the job entry in the database, although it is only really of use when the command is running.
- #202 Added additional information to worker logs.

  - Added worker name to all worker log entries.
  - Added log entry when starting to execute a job.
  - Added log entry when a job has completed.
- #209 Added the last heartbeat timestamp to the list of workers.  This is helpful for seeing where a worker is stale and not checking in, yet has not aged out yet and so may still be displaying as idle.

### Changed
- #202 Removed job log from log entry that the server writes when a job has completed.
- #203 The worker needs to publish a message to NATS, and await a response, when updating job execution status, updating the worker status, and deregistering a worker.  Previously if the there was any error, the call would immediately error, therefore not accounting for transient issues.

  These calls have now been refactored to backoff and retry, up to a maximum of 10 attempts, before failing, therefore making the workers more resilient to transient issues.
- #205 To support NATS multitenancy, the server and worker will now connect to NATS using a dedicated Atom account.  See https://rokett.gitlab.io/atom/docs/getting_started/ for how to configure NATS and how to generate and specify the NKey used for authentication to the server and worker executables.
- #207 **BREAKING** - Refactored server and worker command line flags to namespace.  For example, instead of `nats_host`, it is now `nats.host`.  A full list of changed flags are below; ensure you change them.

  #### Server
  - `nats_host` to `nats.host`
  - `nats_server_port` to `nats.server_port`
  - `nats_stream_replicas` to `nats.stream_replicas`
  - `api_port` to `api.port`
  - `cors_allowed_origins` to `cors.allowed__origins`
  - `cors_allowed_headers` to `cors.allowed_headers`
  - `db_host` to `db.host`
  - `db_port` to `db.port`
  - `db` to `db.name`
  - `db_username` to `db.username`
  - `db_password` to `db.password`
  - `auth_hosts` to `auth.hosts`
  - `auth_base_dn` to `auth.base_dn`
  - `auth_bind_dn` to `auth.bind_dn`
  - `auth_bind_pw` to `auth.bind_pw`
  - `auth_port` to `auth.port`
  - `auth_use_ssl` to `auth.use_ssl`
  - `auth_start_tls` to `auth.start_tls`
  - `auth_ssl_skip_verify` to `auth.ssl_skip_verify`
  - `log_file` to `log.file`

  #### Worker
  - `nats_host` to `nats.host`
  - `nats_port` to `nats.port`
  - `job_groups` to `job.groups`
  - `vault_address` to `vault.address`
  - `vault_token` to `vault.token`
  - `vault_secrets_engine_path` to `vault.secrets_engine_path`
  - `vault_secrets_path` to `vault.secrets_path`
  - `log_file` to `log.file`
  - `execution_logs_path` to `execution.logs_path`
  - `ps_scripts_path` to `ps.scripts_path`

- #208 Updated all JS and Go dependencies.
- #210 **BREAKING** - Refactored logging to use stdlib slog package in place of Zap to remove a dependency.  Resulting in the removal of the `log.file` flag.

## [0.26.0] - 2023/11/04
### Fixed
- Removed documentation about metrics which no longer exist; `atom_server_requeued_jobs_total` and `atom_server_pending_jobs_total`.  These are better retrieved through the native NATS metrics, using the `jsz` endpoint.  There is no longer the concept of requeued jobs.  To see queued jobs you should use the `jetstream_consumer_num_pending{consumer_name="xxxxxxx"}` where `xxxxxx` is the name of the group you are interested in.
- #184 Corrected some API docs.
- #195 Corrected application token name in API docs.
- #197 Corrected redirect to `\admin\event_targets` when adding or editing an event target.
- #198 Added inactivity timeout when creating worker specific subscriptions.  In normal circumstances, when a worker exits gracefully it is unsubscribed from subjects in NATS at which point NATS removes the consumers automatically. In situations where the worker does not exit gracefully, although the worker itself is cleaned up in Atom the consumer would remain in NATS because nothing has told it to remove the consumer.

  Adding an inactivity timeout (which is set to the same 6 minute threshold as to identify a stale worker for cleanup), resolves that problem as NATS will now remove stale consumers automatically.

### Added
- #192 New server flag, `nats_stream_replicas`, to allow the operator to set a number of replicas that Atom created NATS streams should have.  This would be used where NATS is running as a cluster and you therefore want a stream replica on each server.  Defaults to `1`, and allowed values are `3` or `5` for a 3 or 5 node NATS cluster.
- #193 Log error if workers are not able to ack a message before executing a retrieved job.

### Changed
- #199 Updated all Go and JS dependencies to latest versions, and require Go version 1.21.x for building Atom.

## [0.25.0] - 2023/04/26
### Changed
- #179 To avoid situations where the worker starts, registers with the server, but then a problem happens and it fatally exits without deregistering, all relevant potential exits now call the worker shutdown routine.  The shutdown routine ensures that the worker is unsubscribed from all subscriptions, and that it sends a deregistration request to the server before exiting.
- #181 When requesting to run an existing job again, instead of just running the job as-is, the user is now presented with a form to change the execution parameters if they wish.
- #182 Updated all frontend and backend dependencies.
- #190 Job log updates are now published to NATS and stored in a stream.  When viewing job detail for a *running* job, the log is pulled from NATS.  When a job completes, the worker publishes a status update along with the full job log which the server writest to the DB.  After the job details are written to the DB, the messages are ack'd and they will be removed when the maximum age is reached for the log stream.

  Previously all job log updates were written to the DB as they happened, causing the DB logfile to grow extremely large.

## [0.24.0] - 2023/02/01
### Fixed
- #171 When the worker is disconnected from NATS for any reason, and stops trying to connect because it has reached the maximum number of retries, the connection is marked as closed.  The goroutines to fetch messages are still running at this point, and so the logs fill up very quickly with error messages as the worker is unable to fetch messages from a closed connection.

  To resolve this logging problem, when the maximum number of retries have been reach, the worker will now cancel any running goroutines.  Recovering from a closed connection like this requires a worker restart anyway.

### Changed
- #139 Refactored Atom to render all pages server side instead of being an SPA.  This allows us to provide easy links to things like job runs; `GET /jobs/{id}`.
- #159 Refactored server NATS client to have a maximum number of reconnect attempts (currently 60), and with `Disconnect`, `Reconnect` and `Closed` handlers for logging purposes.

  Also changed worker NATS client to not exit on `Closed` handler, but just to log an error instead.
- #161 Refactored worker to server comms to use NATS instead of the dedicated REST API on the server.  This removes an extra API server and unifies worker and server comms over NATS.
- #173 Instead of the server sending a heartbeat request to the workers, logging if a worker does not reply, and then cleaning up anything which has failed to reply six times, the process is now simplified.

  The workers now publish a heartbeat message every 1m which the server is subscribed to.  When the server receives a heartbeat, it updates a timestamp field in the worker record with the time it received the message.  A scheduled job runs every minute to delete any workers with a heartbeat timestamp older than 6 minutes.
- #174 Unified flag formats by removing `-` in favour of `_`.  the following changes have happened.

  #### Server
  - `nats-host` to `nats_host`
  - `nats-server-port` to `nats_server_port`
  - `api-port` to `api_port`
  - `cors-allowed-origins` to `cors_allowed_origins`
  - `cors-allowed-headers` to `cors_allowed_headers`
  - `log-file` to `log_file`

  #### Worker
  - `nats-host` to `nats_host`
  - `nats-port` to `nats_port`
  - `job-groups` to `job_groups`
  - `vault-address` to `vault_address`
  - `vault-token` to `vault_token`
  - `vault-secrets-engine-path` to `vault_secrets_engine_path`
  - `vault-secrets-path` to `vault_secrets_path`
  - `log-file` to `log_file`
  - `execution-logs-path` to `execution_logs_path`
  - `ps-scripts-path` to `ps_scripts_path`
  - `default-user` to `default_user`
- #175 Removed `max-job-execution-attempts` flag from server as it was unused.
- #176 Refactored the job execution code, which was duplicated in 4 different places, to unify things around a single way of working.  There are no changes in functionality, but maintenance is now simpler.
- #177 Refactored code to hang functions off of the `application` struct as methods.  Simplifies function calls and is a step forward in terms of unifying functions which do the same job.

### Added
- #94 Adding a new schedule now offers autocompletion of the script name when typing.
- #95 Added button to create scheduled job from existing job execution.  User is taken to the form to add a scheduled job, with job execution data pre-filled.
- #123 Added support for PowerShell 7.x, `pwsh.exe`, as another executor.
- #158 Workers now report their version when registering with the server.  The version number is displayed on the workers page.
- #160 New option to put the system into maintenance mode.  When in maintenance mode, the server continues to accept job requests and sends them to NATS, but the workers are unsubscribed from job queues so will not attempt to pick up any jobs until maintenance mode is disabled; they will queue in NATS.

  The workers continue to listen on system related subjects in order to pick up updated lists of approved scripts, respond to liveness checks, and to subscribe back to job queues when maintenance mode is disabled.

  Maintenance mode can be toggled either through the UI, or via `POST` to the `/api/config/maintenance_mode` route.

  A new metrics is exposed, `atom_server_maintenance_mode`, to track whether maintenance mode is enabled, `1`, or disabled, `0`.
- #162 Added initial support for running arbitrary binaries.  Use the `binary` executor.
- #170 Added new parameter to jobs to set a duration after which the job should expire and not be run if a worker has not already run it.

  The server sets a default duration, which can be user configured using the `expire_queued_job_after` flag.  If the job request does not specify its own duration, the server default will be used.  For backwards compatibility the default value is set to `720h` if the operator does not configure a different default.  Valid units are `h`, `m`, `s`.

  When a job execution is created by the server, it sets an expiry timestamp based on the `time job requested + expiry duration`.  When a worker picks up a job, it will check that the current time is NOT after the expiry timestamp before executing the job.  If the current time IS after the expiry timestamp, the job is terminated and will not be run.

  This change stops jobs from queuing up indefinitely if workers are offline for some time.  Generally jobs should be idempotent so it shouldn't matter if they run some time after they were queued, but that isn't always easy to do so this new parameter creates a safety net.

## [0.23.0] - 2022/11/19
### Added
- #154 Exposing key RED metrics via the `/metrics` endpoint in Prometheus format;

  - `http_errors_total` partitioned by status code,.
  - `http_request_duration_seconds` partitioned by path and method.
  - `http_requests_total` partitioned by path, method and status.

### Changed
- #156 Updated dependencies.

## [0.22.0] - 2022/11/13
### Added
- #153 Now passing some Atom specific environment variables into the process being executed.  These can be used within whatever is being executed to do things like include a reference to the Atom job ID in logs.

  Added environment variables are as follows:

  - `ATOM_JOB_ID`
  - `ATOM_JOB_REQUESTED_BY`
  - `ATOM_JOB_EXECUTION_USERNAME`

## [0.21.0] - 2022/11/13
### Changed
- #150 Server container will now run as a non-root user with UID 10000 and GID 10001.

### Added
- #25 It is now possible to run a job as a different user for a Windows based worker.  This means that the user account running the worker does not need to have any privileges other than `LOCAL SYSTEM`.  If that does not provide adequate privileges, you can execute a job with the `execution_username` field and specify a user who does have privileges.  If it is a domain based user, ensure you use a UPN (`username@contoso.com`), and if it is a local user use a `.\` prefix (`.\username`).

  You can also specify a default user for the worker, to override whatever the process is running as, and which would take effect if the job request **does not** specify a user.

  This functionality requires somewhere to store the password so as to ensure we are not passing secrets around in plaintext.  At the time of this change, only Hashicorp Vault is supported.  Add new secrets where the key is the username (`username` if a local user and `username@contoso.com` if a domain user), and the value is the password.  Use the `vault` flags when starting the worker to tell it where to look and how to authenticate with Vault.

## [0.20.0] - 2022/09/03
### Changed
- #149 Allow for the path to PS scripts to be user customisable using the `--ps-scripts-path` flag; previously it was assumed that the scripts were local to the binary.

  Paths on Windows **must** escape the slashes; for example `C:\\ProgramData\\Atom\\`.

## [0.19.0] - 2022/07/25
### Changed
- #120 Migrated away from `gorilla/mux` for routing to `go-chi`.
- #121 Removed all references to a worker being generic.  Generic workers have been deprecated for a long time.
- #125 Ensure that process can write to execution logs directory during application initialisation phase.
- #127 Allow for worker execution logs directory to be user customisable through the use of the `--execution-logs-path` flag.
- #146 Removed custom queueing logic in favour of using NATS Jetstream.  Any job now sent to one of a group will go via JetStream to allow for reliable queuing.  Refer to the documentation regarding how to configure NATS; no changes are required to Atom flags outside of pointing to the NATS server as was already required.
- #147 Removed all support for `@any` scope, which has been deprecated for a long time anyway.

## [0.18.0] - 2022/07/18
### Changed
- #138 Removed support for embedded NATS in favour of using it as an external service.  Refer to documentation for how to configure the server and workers.

## [0.17.0] - 2022/06/12
### Changed
- #143 Reconfigured migrations to strip line breaks in order to ensure that the resulting hash is the same on Windows and Linux.  This allows for porting between the two, which may be unlikely, but without this change running the migrations would fail.

  **Important:** To support this change you must delete all records from the migrations table.  This will force migrations to run again, which are idempotent from a schema perspective, although be aware that all user records will be deleted.  That doesn't really matter, it will just force users to log in again.

## [0.16.1] - 2022/06/08
### Fixed
- #142 Increased the length of the `username` field to account for the fact that Atom now requires a UPN which is generally longer than a standard username.

## [0.16.0] - 2022/06/05
### Changed
- Refactored UI into a folder called `UI` instead of `frontend`.
- Updated dependencies.
- #136 Replaced Swagger API doc UI with Rapidoc.
- #140 Replaced `https://github.com/denisenkom/go-mssqldb` with new official package, `https://github.com/microsoft/go-mssqldb`.
- #141 Require UPN for logging in instead of username.  Facilitates support for multiple domains, although authentication for multiple domains is not currently enabled.

## [0.15.1] - 2022/05/25
### Fixed
- #137 Heimdall now has an input processor for an Atom payload.  Reconfigured Atom to send the job execution details natively.

### Changed
- Migrating to Tailwind v3 broke some colours in the UI because I had previously used a custom colour palette due to changes in v2.  Have now removed the custom colours to more easily integrate with v3.  The result is a slightly darker tone for the UI overall, but that is no bad thing.

## [0.15.0] - 2022/05/03
### Changed
- #135 Clustering the server is no longer supported.  Certain elements, specifically maintaining a lock on scheduled or running jobs, was proving to be tricky to ensure the functionality was stable, leading to random issues.  Investigation determined that the best approach right now would be to move back to a single server approach.

## [0.14.6] - 2022/03/21
### Fixed
- #134 Paused scheduled jobs were getting run following a service restart.  On restart the server would retrieve all scheduled jobs, not just those which are not paused.  Now resolved by ensuring it only retrieves unpaused scheduled jobs.

## [0.14.5] - 2022/01/05
### Fixed
- #130 Previous change to implement date picker broke the quick dates; this is now resolved.

## [0.14.4] - 2022/01/04
### Fixed
- #33 Implemented a proper date picker for choosing timerange to view jobs.
- #129 Targeting a job at `group1:@all` would target every worker which is a member of that group, as well as any worker which is a member of `xgroupx`, where the `x` is any other character or set of characters.  Have now fixed this by checking for the exact group name before selecting workers to target.

### Changed
- Added a new flag, `disable_stale_worker_cleanup` to disable that scheduled job.  This isn't really there for production use; it is helpful during development.

## [0.14.3] - 2021/12/31
### Fixed
- #128 The execution log filename for the Ansible executor was the whole path to the playbook.  Have trimmed it to just the filename now.

## [0.14.2] - 2021/12/31
### Fixed
- #126 Corrected execution logs path on Linux from `/var/logs/atom/execution_logs` to `/var/log/atom/execution_logs`.  Note the `logs` vs `log`.

## [0.14.1] - 2021/12/31
### Fixed
- #124 The execution logs path was incorrect for both Windows and Linux.  Have now changed it based on the OS in use, and carry out a check at worker startup to ensure it is there.  If the directory does not exist, or is not a directory, the worker will exit with an error.

## [0.14.0] - 2021/12/30
### Fixed
- Resolved JS error when clicking on the button to edit a scheduled job. JS threw an error because it couldn't set the deleted property of the job, because the job was not being passed to the function which did that task.

### Added
- #29 Added options to run jobs using additional binaries.  Previously Atom would only run Windows PowerShell scripts, but with this change it can now also run Ansible playbooks.  The structure introduced here also allows for additional binaries in the future.  A new mandatory parameter has been added to the job spec named `executor` which can be either `powershell` or `ansible`.

    A consequence of this change is that generic workers, and therefore also the `@any` job scope, are now deprecated; support will be removed in a future release.  All workers should now join a group.  Convention is that generic PowerShell workers join the `powershell` group and generic Ansible workers join the `ansible` group.
- #81 There is now an option to pause/unpause scheduled jobs.  If an operator pauses a scheduled job, it is completely removed from the cron schedule; unpausing will add it back in.
- #115 Senstive secrets, such as Vault tokens, are now masked when returning job information from the API.

### Changed
- #113 Add stacktrace to log entries for errors and above.
- #117 Ensuring actions are clearer when adding a new scheduled job.  Confirm is now a green tick, and cancel is a red cross.

## [0.13.1] - 2021/10/12
### Changed
- #111 Updated module path from `atom` to `rokett.me/atom` to better align with Go standards.
- #112 Updated dependencies.
- #116 Removed `remote_address` log field from `validateAuth` function.  It was a duplicate of `client_ip` and was causing some logging errors due to the format it came in.

## [0.13.0] - 2021/08/26
### Added
- #110 Ensured that Atom shuts down gracefully when the process is stopped.  Connections are closed, NATS subscriptions are ended, and the lock on the stale worker check is forceably removed.

## [0.12.0] - 2021/07/25
### Fixed
- #103 Resolved JS error when trying to expand scheduled job details if you click on the text.
- #106 Job completion events are now properly sent.  Previously the list of acceptable job status was incorrect.

### Changed
- #105 Added a failed status check counter to the workers.  Instead of deleting a worker the instant it fails a status check, Atom now requires 5 failed status checks before deleting.  With each status check being 1 minute apart, that means the worker has to be unresponsive for at least 5 minutes before it can be deleted.
- #105 Only a single server node in the cluster can now run the job to cleanup stale workers.
- #107 Updated frontend and backend dependencies.

### Added
- #108 Output `stdout` and `stderr` of executed script to timestamped log file in the `execution_logs` directory.

## [0.11.0] - 2021/06/27
### Added
- #43 New metric to show online workers per group; `atom_server_workers_total`.  Can be used to alert on not having enough online workers for the different groups you need.
- #43 New metric to show number of pending (queued) jobs; `atom_server_pending_jobs_total`.  Pending jobs may be an indication of needing more online workers.
- #99 New frontend option to run a scheduled job.  The job will be executed as per the scheduled definition.
- #101 New endpoint, `POST /api/config/approved_scripts`, to add a single script to the approved list.  A duplicate of an existing script will return an error.  Sending a payload with more than one script will return an error.

### Changed
- Editing a scheduled job is now done by expanding the job details and clicking on the button to `edit`.
- #97 Moved the confirmation dialog when deleting a scheduled job to be within the schedule details.  This is a consequence of adding the option to run a scheduled job in #99, however it also makes things easier to manage when there are many scheduled jobs in the list.
- #102 Updated frontend and backend dependencies.

## [0.10.0] - 2021/06/21
### Added
- #98 New metrics added.

    `atom_server_job_runs_total` counts the total number of job runs; dimensioned by script, job type (ad-hoc or scheduled), and job status.  Jobs failed because no workers are available may indicate that more workers are required, or that there is a problem with the workers.  This metric can also be used as a rudimentary dead mans switch assuming you expect to see some jobs run in x hours, by looking for 0 increase in that time.

    `atom_server_requeued_jobs_total` counts the total number of requeued jobs.  High numbers of jobs being requeued may indicate that more workers are required.

### Changed
- #100 Changed logging timestamps to include milliseconds for more precision.

## [0.9.0] - 2021/06/14
### Added
- #73 Enabling in-place editing of scheduled jobs.
- #77 Scheduled job checks for stale workers and deletes any that it finds.  A stale worker is one who doesn't respond to a `ping` message.
- #84 It is now possible to re-run a job execution.  Re-running a job is available from the jobs screen and uses the same configuration as the original run.
- #91 Display worker groups in UI.

### Changed
- Reconfigured log field names to be unified across Atom.
- Added `/status` route for use with monitoring.  Returns a `200` status code with `message: ok` in the body.
- #63 Reconfigure job execution requests such that the server queues jobs within the DB instead of fire and forget messaging.  A goroutine's process attempts to dequeue jobs and execute them.  If a worker does not pick a published job up, it is returned to the queue until it reaches the maximum number of retries (default 20), at which point the job is failed.

   This removes the scenario where a worker is not available when a job is published which results in a job not executing.
- #85 Ensure that only the cluster leader can fail queued jobs to avoid race conditions.
- #86 Removed Raft in favour of using the database for queueing and ensuring only one node owns a job at any point.
- #87 Removed the need for gRPC by making the workers communicate with the server over a private HTTP API for worker and job updates.  Future changes will ensure this connection is secured and authenticated through the use of mTLS.
- #89 Updated frontend and backend dependencies.
- #92 The job completion status will no longer log a warning if the only warning is from PowerShell saying that the names of imported commands from a module are unapproved.  If there are any further warnings in the log, the job status will continue to reflect the fact that the job completed with warnings.
- #93 Making the NATS host configurable, instead of being hardcoded to 127.0.0.1 to allow for Atom running in a container to connect to itself.

## [0.8.3] - 2021/04/05
### Fixed
- #82 Since British Summer Time came into effect, newly created jobs are not displayed in the jobs list until more than the last 24 hours was selected. Storing the dates within the DB in UTC format ensures a standard time which can be dealt with appropriately by the consumer.  The frontend will convert this time into the locale of the client.
- #83 When viewing job detail the `Requested At` date was blanked out.  As this was a calculated field, it needed to be re-calculated when viewing job detail.

## [0.8.2] - 2021/02/21
### Fixed
- #78 Following the change to `go embed` migrations were trying to run again because the filename had changed to `migrations\xxxx.sql` instead of `xxxx.sql`.  That was because the way the file was referenced during the migration was different.  Now fixed by ensuring the migration is checked against `xxxx.sql` only.
- #79 Firing job events errored because of an mistyped database field name.  Now corrected.

## [0.8.1] - 2021/02/17
### Changed
- Removed [Packr](https://github.com/gobuffalo/packr) in favour of the `embed` feature in Go 1.16.

## [0.8.0] - 2021/02/15
### Fixed
- #75 Filtering for job executions by script name was not working; now resolved.
- #76 Scheduled jobs added after the server nodes started up, in a cluster configuration, did not always run.  This was because they were only scheduled on the node which received the request to add a new job.  This bug is now resolved; the receiving server now publishes a message to all other servers in the cluster so that they can also schedule the job.  Functions to update and delete scheduled jobs have also been updated to ensure all server stay in sync.

### Added
- #74 It is now possible to configure targets to receive notifications when a job finishes. See https://rokett.gitlab.io/atom/docs/receiving_job_events/ for more information.

## [0.7.0] - 2021/02/07
### Changed
- General performance updates by ensuring that static content is cached.  Static content is pretty much anything other than HTML.  We use hashing of the file contents, embedded into the filename, to bust the cache if something changes.
- #70 Wrapping job logs so that they do not break out of the page.
- #71 Set default time range for jobs on initial application load to last 24 hours.
- #72 Updated to Tailwind CSS v2.x.

## [0.6.0] - 2021/02/03
### Fixed
- #68 Log lines were being inserted into the DB record multiple times because the loop which allows for the DB to not be immediately available never exited as early as possible.  Now fixed.
- #69 Ensuring that parameter fields wrap in the GUI in order to not break the table layout.

### Changed
- #66 Load job detail only when a user wants to look at the detail.  When just showing the summary table, only summary fields are pulled down and displayed.  This goes some way to speeding up the first load.
- #67 Load records for each panel only when they are selected, not on first load.  This goes a little way to speeding up the first load, and also ensures that the records for each panel are refreshed when the panel is opened.

## [0.5.0] - 2021/02/01
### Added
- #37 It is now possible to execute a job via the GUI from the scripts panel.
- #38 Added GUI to view approved scripts.
- #65 You can now add approved scripts to the configuration from within the GUI.  Ensuring the script is available on the worker is still up to you to determine how to make that happen.

## [0.4.0] - 2021/01/23
### Added
- #39 Return Job ID to caller when job execution request is scoped to `@any`.
- #52 `cors-allowed-origins` and `cors-allowed-headers` flags have been added to the server in order to allow a customised CORS configuration.  If omitted, the server defaults to disabling CORS.
- #53 Job output is now sent to the server log in addition to the database job execution record.  This allows easy slurping by log collectors should the need be there.
- #56 The `/api/status` endpoint now includes the leader of the Raft cluster in the response.
- #56 The application metrics now include the leader of the Raft cluster.
- #58 Flags have been added to set logfiles for Atom and Raft; they are separate because Raft logs in a different format, and separate files makes things easier for ingestion into centralised logging solutions.  The flags are `log-file` and `raft-log-file` respectively, and the logs are saved into the same folder as the executable.  Files are set to automatically rotate when they reach 100MB in size, to retain 3 versions, and to have a maximum age of 7 days for those old versions.

### Changed
- #51 Workers are now removed from the database when they go offline.  Worker details are also now logged with the job execution record; worker name, IP, server name and OS.
- #54 Raft logs are now output as JSON.
- #59 Raft config has been tweaked to remove some of the logging noise due to things like Raft trying to take a snapshot of data that doesn't exist.  It now tries to take a snapshot less frequently, and only if there is any data to actually snapshot, which there isn't as Atom just uses Raft for leader elections.

## [0.3.0] - 2020/12/13
### Fixed
- #48 Fixed bug when trying to update scheduled job due to missing field in DB query.

### Changed
- #34 Show job history when clicking on row instead of only on down arrow.
- #36 Require confirmation when deleting a scheduled job.
- #40 Jobs scoped to `@any` now only run on workers which have been run using the `--generic-worker` flag.  Jobs scoped to `@any` may have requirements, such as access to resources, which workers installed on specific servers do not, so we allow for workers to be 'generic' for this purpose.  The alternative would be that any server which has a worker running would need to have access to **all** resources that any job may need.
- #41 Removed the requirement to specify the operator when requesting a job is executed.  This was a free text field which meant anything could be entered, so it was essentially worthless.  The operator is now completed automatically based on the session token passed to Atom in the request.
- #46 Ensure that workers unsubscribe from all subjects before closing down.  Previously they would only unsubscribe from the `job:execute` subject, leaving the potential for them to pick up other jobs on other subjects before they were properly closed down.
- #47 Script output now only appears in stdout and stderr when the application is running interactively.  When running non-interactively, as a service, the output will not be sent to either.  This removes the possibility of script output appearing in log files when a sidecar is running to scrape stdout and stderr from the running service.
- #49 Debug logging is now enabled by setting an environment variable called `atom_debug`.  The value does not matter, just that it exists.  This saves us having to reconfigure the service to enable debugging.

### Added
- #3 Allow for jobs to be targeted at workers running on specific server(s) but specifying the hostname(s) as the scope.  For example, setting the scope to `server1,server2` will execute the job on both `server1` and `server2`.
- #42 Applied colours to the script output to highlight debug (green), verbose (yellow), warning (orange) and error (red) messages.  It is not quite perfect as due to limitations when running as a service some lines run over onto multiple lines, even though they should be a single line; the regex can't cope with this as we don't know for sure that those lines which overrun are related.
- #44 Logging has now been added for completed jobs with a non-successful status.  This allows for alerts to be created, in an external systems, based on problem jobs.
- #45 Added option to pass token in `VND.atom.application_token` header instead of cookie.  This allows for applications to authenticate which do not support cookies.
- #50 The server, or at least the NATS part of it, can now be clustered by using the `--nats-cluster-routes` flag.  This allows for multiple servers to run in an active/active mode for resilience.

## [0.2.0] - 2020/11/15
### Added
- #14 Log who requestes the job to be executed.  Set to `scheduled` if run as a scheduled job.
- #15 Workers are either `online` or `offline`.  On startup they generate a random name and register themselves with the server, thus helping to track down execution problems if they occur.
- #16 Allow scheduled jobs to be deleted if the user is an admin.
- #17 Ensure validation happens when adding jobs to ensure we don't get rubbish that can't work.  Checks that the script being set is allowed, and that the schedule is valid.
- #19 Ensure that the worker which ran a job is displayed in the UI.
- #27 Noticed that if the DB is not available when a job is trying to start execution, it just fails with no notification.  Add some retry logic with a maximum retry time before it really does fail, and then log it accordingly.
- #30 Added options to run jobs on all workers, any workers, any/all workers in a specified group, or on specified servers based on name.
- #31 Only returning the last 7 days of job executions by default.  Added a date picker to filter the job executions based on dates.
- #32 To support #30, the workers can be part of groups where the group names are specified by the `job-groups` passed to the executable.

## [0.1.0] - 2020/09/28
- #2 When a worker is stopped it immediately makes itself unavailable for new jobs, and drains any currently running jobs before fully stopping.
- #4 Show the worker that ran a job in the execution logs.
- #5 Manage scheduled jobs via the GUI.
- #8 Allow filtering of the job execution and scheduled jobs tables.
- #8 Allow sorting of the job execution and scheduled jobs tables.
- #9 Ensure that the workers sense the output of running jobs back to the server in realtime.  This is a pre-requisite for making sure that the job execution updates in the GUI in realtime.
- #18 Add timestamps for when a job was started and finished.
- #20 Require authentication to access the GUI.  Access relies on membership of groups defined within the database.
- #21 Admin permissions are needed in order to create, change, or delete scheduled jobs.
