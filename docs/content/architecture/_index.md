---
title: "Architecture"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

{{< toc >}}

Atom consists of two main components; Server(s) and Worker(s).  Here's an architecture overview.

![Logical architecture diagram](../logical_architecture.png "Logical Architecture")

# Workers
There can be as many workers as you want.  Each worker subscribes to a set of standard subjects in NATS, and can optionally subscribe to a set of user defined subjects.

Workers support multiple binaries; currently Windows PowerShell and Ansible.  Note that is **Windows PowerShell**, of which the latest version is 5.1, not what is now known as PowerShell where the current version is 7.x.

Workers supporting Windows PowerShell needs to run on Windows, and the server should have Windows PowerShell 5.1 installed.

Workers supporting Ansible need to run on Linux.

PowerShell (7.x and beyond) and other binaries for Linux, such as bash, may be supported in a future release.

PowerShell workers can only run scripts that exist locally to them in the same directory, and which have been defined at the server as being allowed.

Ansible workers can run playbooks in any location; the full path should be specified when defining the script to be run.

If a request is made to run a script or playbook which has not been pre-approved, the job execution will fail.

When a worker starts it goes through the following process.

![Worker startup](../worker_startup.png "Worker startup")

When a shutdown is initiated for a worker, it goes through the following process.

![Worker shutdown](../worker_shutdown.png "Worker shutdown")

Workers are inherently resilient, you just need to have more than one.  As each job is executed serially, you can easily work out how many workers you need by determining how many jobs you want to be able to run at once.

## Running a job as a specific user
**The following is applicable to Windows based workers only**

If you have a need to run a job as a specific user, maybe some permissions are needed which you do not want to, or cannot, give to the account running the worker, it is possible to pass a username to the job request using the `execution_username` field.

Credentials must be stored in a supported secret store, currently only Hashicorp Vault.  The key of the secret should be the username, and the value will be the password.

The worker will need to be started with the relevant `vault` flags in order to be able to access the secret.

At the time of execution, the worker will lookup the password, authenticate as the specified user which will return a token, and then execute the job using that token.

# Server
The server has three main components.

## REST API
The API provides an interface for managing jobs, allowed scripts, viewing workers, and authentication. See [API]({{< ref "/docs/getting_started#api" >}} "API").

## NATS
The [NATS](https://nats.io) message queue is the conduit for job execution requests between the server and worker.  Jobs to be executed are published to NATS using a specific [subject]({{< ref "/docs/getting_started#targeting-jobs-against-workers" >}} "Cron"), and any active subscriber listening on that subject receives the message.

It is also possible to create queue groups which means that as messages on the registered subject are published, one member of the group is chosen randomly to receive the message; although queue groups have multiple subscribers, each message is consumed by only one.  To create a queue subscription, subscribers register a queue name. All subscribers with the same queue name form the queue group.

The message queue is non-persistent; if a worker is not online to retrieve a message when it is published, the message will be lost.

# Database
The database stores scheduled job definitions, job execution history, worker status, allowed scripts, and user details.

# Ports
A number of different ports are required to be open for Atom to work properly.

![Ports diagram](../ports.png "Atom ports")

| Port | Purpose                                                                    |
| ---- | -------------------------------------------------------------------------- |
| 9999 | Public HTTP API access                                                     |
| 4222 | TCP based comms between workers/servers and the servers for NATS           |
