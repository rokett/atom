FROM golang:alpine as builder

ENV VERSION="0.31.1"

WORKDIR $GOPATH/src/gitlab.com/rokett
RUN \
    apk add --no-cache git nodejs npm && \
    git clone --branch $VERSION --depth 1 https://gitlab.com/rokett/atom.git atom && \
    cd atom && \
    chmod -R 777 ./node_modules && \
    npx tailwindcss -i ui/src/app.css -o ui/tmp/app.css && \
    npx cleancss -o ui/tmp/app.min.css ui/tmp/app.css && \
    npm run build && \
    BUILD=$(git rev-list -1 HEAD) && \
    CGO_ENABLED=0 GOOS=linux go build -a -mod=vendor -ldflags "-X main.version=$VERSION -X main.build=$BUILD -s -w -extldflags '-static'" -o atomd ./cmd/server

FROM scratch
LABEL maintainer="rokett@rokett.me"
COPY --from=builder /go/src/gitlab.com/rokett/atom/atomd /

# Create a local user so we can run as non-root
ARG UID=10000
ARG GID=10001
RUN groupadd --gid $GID atomd && useradd --system --create-home --shell /sbin/nologin --uid $UID --gid $GID atomd

# Run with user atomd
USER atomd

EXPOSE 4000

ENTRYPOINT ["./atomd"]
