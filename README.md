![Atom](ui/src/android-chrome-192x192.png)

Atom is a distributed task runner.  It allows you to run jobs (or tasks) anywhere where an Atom Worker exists.

An Atom Worker is just an executable that runs on a server and facilitates the execution of a job.  In Atom's case, that job is a script written in PowerShell.  There is nothing stopping Atom from being extended to be able to run scripts in other languages, or other executables, but right now the scope is limited to PowerShell.

Atom gives you a reliable and resilient method for running any script you want, either programmatically via the API or on a schedule; think event driven type automation for example.  The workers are disposable, outside of ensuring any scripts you want exist on the server running the worker, and the server component can be clustered for resilience.

See https://rokett.gitlab.io/atom for documentation.
