module.exports = {
    content: [
        'ui/src/*.html',
        'ui/src/**/*.html',
        'ui/src/*.js'
    ],
    safelist: [
        'text-orange-500',
        'text-amber-500',
        'text-emerald-500',
        'text-red-500',
        'text-blue-500',
        'text-violet-500'
    ],
    theme: {
        extend: {
            spacing: {
                '72': '18rem',
                '84': '21rem',
                '96': '24rem',
            },
            inset: {
                '116': '116%',
            },
            width: {
                '546px': '546px',
            }
        },
        borderWidth: {
            DEFAULT: '1px',
            '0': '0',
            '2': '2px',
            '3': '3px',
            '4': '4px',
            '6': '6px',
            '8': '8px',
        }
    },
    plugins: [],
}
