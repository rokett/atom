package model

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

// Script contains the representation of a script as stored in the database
type Script struct {
	ID        int64
	Name      string    `db:"script"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type ScriptModel struct {
	DB     *sqlx.DB
	Script Script
}

func (s *ScriptModel) List(filters map[string]string) ([]Script, error) {
	var scripts []Script

	var params []interface{}

	q := "SELECT id, script FROM scripts"

	paramIndex := 1

	for k, v := range filters {
		if paramIndex == 1 {
			q = fmt.Sprintf(`%s WHERE %s LIKE @p%d`, q, k, paramIndex)
		} else {
			q = fmt.Sprintf(`%s AND %s LIKE @p%d`, q, k, paramIndex)
		}
		params = append(params, fmt.Sprintf(`%%%s%%`, v))
		paramIndex++
	}

	q = fmt.Sprintf(`%s ORDER BY script`, q)

	err := s.DB.Select(&scripts, q, params...)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return scripts, ErrNoRecords
	}
	if err != nil {
		return scripts, fmt.Errorf("unable to retrieve scripts: %w", err)
	}

	return scripts, nil
}

func (s *ScriptModel) Get(id int) (Script, error) {
	var script Script

	err := s.DB.Get(&script, "SELECT id, script FROM scripts WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return script, ErrNoRecords
	}
	if err != nil {
		return script, fmt.Errorf("unable to get script with id %d: %w", id, err)
	}

	return script, nil
}

func (s *ScriptModel) SearchByName(query string) ([]Script, error) {
	var scripts []Script

	err := s.DB.Select(&scripts, "SELECT script FROM scripts WHERE script LIKE @p1 ORDER BY script", fmt.Sprintf(`%%%s%%`, query))
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return scripts, ErrNoRecords
	}
	if err != nil {
		return scripts, fmt.Errorf("unable to get scripts: %w", err)
	}

	return scripts, nil
}

func (s *ScriptModel) GetByName(query string) (Script, error) {
	var script Script

	err := s.DB.Get(&script, "SELECT id, script FROM scripts WHERE script = @p1", query)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return script, ErrNoRecords
	}
	if err != nil {
		return script, fmt.Errorf("unable to get '%s' script: %w", query, err)
	}

	return script, nil
}

func (s *ScriptModel) Insert(script Script) error {
	_, err := s.DB.NamedExec("INSERT INTO scripts (script) VALUES (:script);", script)
	if err != nil {
		return fmt.Errorf("error adding new approved script: %w", err)
	}

	return nil
}
