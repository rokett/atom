package model

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/robfig/cron/v3"
)

// JobSchedule contains the representation of a scheduled job
type JobSchedule struct {
	ID                           int64     `json:"id"`
	Executor                     string    `json:"executor"`
	Script                       string    `json:"script"`
	Parameters                   string    `json:"parameters"`
	Scope                        string    `json:"scope"`
	ExecutionUsername            string    `json:"execution_username" db:"execution_username"`
	Schedule                     string    `json:"schedule"`
	ExpireQueuedJobAfterDuration string    `json:"expire_queued_job_after_duration" db:"expire_queued_job_after_duration"`
	Paused                       bool      `json:"paused"`
	CreatedAt                    time.Time `json:"created_at" db:"created_at"`
	UpdatedAt                    time.Time `json:"updated_at" db:"updated_at"`
}

type JobScheduleModel struct {
	DB          *sqlx.DB
	JobSchedule JobSchedule
}

// UnmarshalJSON implements a custom unmarshaller for the job schedule JSON payload to ensure fields are formatted as we need them.
func (j *JobSchedule) UnmarshalJSON(data []byte) error {
	// Creating an Alias type prevents an endless loop
	type Alias JobSchedule
	tmp := (*Alias)(j)

	err := json.Unmarshal(data, tmp)
	if err != nil {
		return err
	}

	tmp.Scope = strings.ToLower(tmp.Scope)

	return nil
}

// Validate ensures that the job requested to be scheduled is valid
func (j *JobSchedule) Validate(allowedScripts []Script) (ve []ValidationError, err error) {
	if j.Script == "" {
		ve = append(ve, ValidationError{
			Parameter: "script",
			Error:     "REQUIRED field",
		})
	} else {
		var allowed bool
		for _, v := range allowedScripts {
			if v.Name == j.Script {
				allowed = true
				break
			}
		}

		if !allowed {
			ve = append(ve, ValidationError{
				Parameter: "script",
				Error:     fmt.Sprintf("Not an allowed script; %s", j.Script),
			})
		}
	}

	if j.Schedule == "" {
		ve = append(ve, ValidationError{
			Parameter: "schedule",
			Error:     "REQUIRED field",
		})
	} else {
		// We attempt to parse the schedule in order to validate that it is allowed.  We're not interested in the parsed config, just whether there is an error or not.
		_, err := cron.ParseStandard(j.Schedule)
		if err != nil {
			ve = append(ve, ValidationError{
				Parameter: "schedule",
				Error:     err.Error(),
			})
		}
	}

	if j.Scope == "" {
		ve = append(ve, ValidationError{
			Parameter: "scope",
			Error:     "REQUIRED field; must be one of <group>:@any, <group>:@all, @all, @linux, @windows or a comma separated list of servers",
		})
	}

	//TODO Can we validate group names?
	if strings.HasPrefix(j.Scope, "@") && (j.Scope != "@all" && j.Scope != "@linux" && j.Scope != "@windows") {
		ve = append(ve, ValidationError{
			Parameter: "scope",
			Error:     "must be one of <group>:@any, <group>:@all, @all, @linux, @windows or a comma separated list of servers",
		})
	}

	validExecutors := map[string]bool{
		"ansible":    true,
		"powershell": true,
		"pwsh":       true,
		"binary":     true,
	}

	if !validExecutors[strings.ToLower(j.Executor)] {
		ve = append(ve, ValidationError{
			Parameter: "executor",
			Error:     "REQUIRED field; must be one of 'ansible', 'powershell', 'pwsh' or 'binary'",
		})
	}

	if j.ExpireQueuedJobAfterDuration != "" {
		_, err := time.ParseDuration(j.ExpireQueuedJobAfterDuration)
		if err != nil {
			ve = append(ve, ValidationError{
				Parameter: "expire_queued_job_after_duration",
				Error:     fmt.Sprintf("Invalid duration %s; %s", j.ExpireQueuedJobAfterDuration, err.Error()),
			})
		}
	}

	if len(ve) > 0 {
		return ve, errors.New("validation failed")
	}

	return ve, nil
}

func (j *JobScheduleModel) List(filters map[string]string) ([]JobSchedule, error) {
	var jobs []JobSchedule

	var params []interface{}

	q := "SELECT id, executor, script, parameters, scope, execution_username, schedule, expire_queued_job_after_duration, paused, created_at, updated_at FROM jobs"

	paramIndex := 1

	for k, v := range filters {
		if paramIndex == 1 {
			q = fmt.Sprintf(`%s WHERE %s LIKE @p%d`, q, k, paramIndex)
		} else {
			q = fmt.Sprintf(`%s AND %s LIKE @p%d`, q, k, paramIndex)
		}
		params = append(params, fmt.Sprintf(`%%%s%%`, v))
		paramIndex++
	}

	q = fmt.Sprintf(`%s ORDER BY script`, q)

	err := j.DB.Select(&jobs, q, params...)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return jobs, ErrNoRecords
	}
	if err != nil {
		return jobs, fmt.Errorf("unable to retrieve scheduled jobs: %w", err)
	}

	for k, job := range jobs {
		jobs[k] = j.sanitiseSecrets(job)
	}

	return jobs, nil
}

func (j *JobScheduleModel) Get(id int) (JobSchedule, error) {
	var job JobSchedule

	err := j.DB.Get(&job, "SELECT id, executor, script, parameters, scope, execution_username, schedule, expire_queued_job_after_duration, paused, created_at, updated_at FROM jobs WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return job, ErrNoRecords
	}
	if err != nil {
		return job, fmt.Errorf("unable to get scheduled job with id %d: %w", id, err)
	}

	job = j.sanitiseSecrets(job)

	return job, nil
}

func (j *JobScheduleModel) sanitiseSecrets(job JobSchedule) JobSchedule {
	var re = regexp.MustCompile(`(?m)-VaultToken ["'].*?["']`)
	var substitution = "-VaultToken '**********'"

	job.Parameters = re.ReplaceAllString(job.Parameters, substitution)

	return job
}

func (j *JobScheduleModel) Update(job JobSchedule) error {
	q := "UPDATE jobs SET executor = @p1, script = @p2, parameters = @p3, scope = @p4, execution_username = @p5, expire_queued_job_after_duration = @p6, schedule = @p7, paused = @p8 WHERE id = @p9"

	var args []interface{}
	args = append(args, job.Executor)
	args = append(args, job.Script)
	args = append(args, job.Parameters)
	args = append(args, job.Scope)
	args = append(args, job.ExecutionUsername)
	args = append(args, job.ExpireQueuedJobAfterDuration)
	args = append(args, job.Schedule)
	args = append(args, job.Paused)
	args = append(args, job.ID)

	_, err := j.DB.Exec(q, args...)
	if err != nil {
		return err
	}

	return nil
}
