package authentication

import (
	"fmt"

	"github.com/go-ldap/ldap/v3"
)

// ConnectLdap opens a connection to an LDAP server
func ConnectLdap(DCs []string, port int) (*ldap.Conn, error) {
	var connError error
	connError = nil

	for _, dc := range DCs {
		conn, err := ldap.DialURL(fmt.Sprintf("ldap://%s:%d", dc, port))
		if err != nil {
			connError = err

			continue
		}

		return conn, nil
	}

	return nil, fmt.Errorf("error connecting to LDAP server: %w", connError)
}
