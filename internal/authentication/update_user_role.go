package authentication

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/go-ldap/ldap/v3"
	"github.com/jmoiron/sqlx"
)

// UpdateUserRole checks whether the user's role has changed (because they have been placed into difference groups), and updates the user record if needed
func UpdateUserRole(token string, db *sqlx.DB, ldapConn *ldap.Conn, baseDN string) (userRole string, err error) {
	changed, groupMembership, groupHash, err := hasRoleChanged(token, db, ldapConn, baseDN)
	if err != nil {
		return userRole, err
	}

	userRole, err = calculateUserRole(db, groupMembership)
	if err != nil {
		return userRole, err
	}

	if changed {
		var u User
		err = db.Get(&u, "SELECT firstname, surname, username, token, token_expiry, token_max_age, last_login FROM users WHERE token = @p1", token)
		if err != nil && errors.Is(err, sql.ErrNoRows) {
			return userRole, fmt.Errorf("user record does not exist in database: %w", err)
		}
		if err != nil {
			return userRole, fmt.Errorf("problem retrieving user record: %w", err)
		}

		u.GroupHash = groupHash
		u.Role = userRole

		_, err = u.Upsert(db)
		if err != nil {
			return userRole, fmt.Errorf("unable to update user role: %w", err)
		}
	}

	return userRole, nil
}
