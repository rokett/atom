package authentication

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
)

func calculateUserRole(db *sqlx.DB, groupMembership []string) (userRole string, err error) {
	var roles []role
	err = db.Select(&roles, "SELECT [group], role, preference FROM roles")
	if err != nil {
		return userRole, fmt.Errorf("error checking application roles: %w", err)
	}

	groups := make(map[string]string)
	for _, role := range roles {
		p := strconv.Itoa(role.Preference)
		groups[role.Group] = fmt.Sprintf("%s:%s", role.Role, p)
	}

	rolePreference := 9999

	for _, g := range groupMembership {
		// If the user is a member of a group which is also defined in the application group mappings then they are allowed access
		if role, ok := groups[g]; ok {
			r := strings.Split(role, ":")

			preference, err := strconv.Atoi(r[1])
			if err != nil {
				return userRole, fmt.Errorf("unable to parse role preference: %w", err)
			}

			if preference < rolePreference {
				userRole = r[0]
				rolePreference = preference
			}
		}
	}

	return userRole, nil
}
