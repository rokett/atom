package authentication

import "strings"

// Creds contains credentials passed to the application for authentication
type Creds struct {
	AuthUsername string `json:"username"`
	AuthPassword string `json:"password"`
}

// CredsValidationError represents a validation error for passed credentials
type CredsValidationError struct {
	Parameter string `json:"parameter"`
	Error     string `json:"error"`
}

// Validate ensures that a valid credentials object is passed
func (c *Creds) Validate() (ve []CredsValidationError) {
	if c.AuthUsername == "" {
		ve = append(ve, CredsValidationError{
			Parameter: "username",
			Error:     "You must provide a username",
		})
	}

	if c.AuthPassword == "" {
		ve = append(ve, CredsValidationError{
			Parameter: "password",
			Error:     "You must provide a password",
		})
	}

	if len(ve) > 0 {
		return ve
	}

	return ve
}

// Sanitise the input to remove wildcard character from username
func (c *Creds) Sanitise() {
	c.AuthUsername = strings.Replace(c.AuthUsername, "*", "", -1)
}
