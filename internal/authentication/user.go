package authentication

import (
	"database/sql"
	"fmt"
	"time"

	"errors"

	"github.com/jmoiron/sqlx"
)

// User represents an application user
type User struct {
	ID          int64
	FirstName   string    `json:"firstname"`
	Surname     string    `json:"surname"`
	Username    string    `json:"username"`
	GroupHash   string    `json:"-" db:"group_hash"`
	Role        string    `json:"role"`
	Token       string    `json:"token"`
	TokenExpiry time.Time `json:"-" db:"token_expiry"`
	TokenMaxAge int       `json:"-" db:"token_max_age"`
	LastLogin   time.Time `json:"-" db:"last_login"`
	CreatedAt   time.Time `json:"-" db:"created_at"`
	UpdatedAt   time.Time `json:"-" db:"updated_at"`
}

// ValidationError contains the parameter with the error and a friendly error message
type ValidationError struct {
	Parameter string `json:"parameter"`
	Error     string `json:"error"`
}

// Upsert updates a user record if it already exists, or creates a new one if it doesn't.
func (u *User) Upsert(db *sqlx.DB) (id int64, err error) {
	err = db.Get(&u.ID, "SELECT id FROM users WHERE username = @p1", u.Username)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return 0, fmt.Errorf("unable to check for user: %w", err)
	}

	if u.ID == 0 {
		id, err = u.create(db)
		if err != nil {
			return 0, fmt.Errorf("unable to create new user record: %w", err)
		}

		return id, nil
	}

	err = u.update(db)
	if err != nil {
		return 0, fmt.Errorf("unable to update user record: %w", err)
	}

	return u.ID, nil
}

func (u *User) create(db *sqlx.DB) (id int64, err error) {
	err = db.Get(&id, "INSERT INTO users (firstname, surname, username, role, group_hash, token, token_expiry, token_max_age, last_login) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9);SELECT id = convert(bigint, SCOPE_IDENTITY());", u.FirstName, u.Surname, u.Username, u.Role, u.GroupHash, u.Token, u.TokenExpiry, u.TokenMaxAge, u.LastLogin)
	if err != nil {
		return id, fmt.Errorf("error adding new user: %w", err)
	}

	return id, nil
}

func (u *User) update(db *sqlx.DB) (err error) {
	_, err = db.Exec("UPDATE users SET firstname = @p1, surname = @p2, role = @p3, group_hash = @p4, token = @p5, token_expiry = @p6, token_max_age = @p7, last_login = @p8 WHERE id = @p9;", u.FirstName, u.Surname, u.Role, u.GroupHash, u.Token, u.TokenExpiry, u.TokenMaxAge, u.LastLogin, u.ID)
	if err != nil {
		return fmt.Errorf("error updating user: %w", err)
	}

	return nil
}

// Validate the payload
func (u *User) Validate() (ve []ValidationError, err error) {
	if u.Username == "" {
		ve = append(ve, ValidationError{
			Parameter: "username",
			Error:     "REQUIRED field",
		})
	}

	if len(ve) > 0 {
		return ve, errors.New("validation failed")
	}

	return ve, nil
}
