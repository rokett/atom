package authentication

import (
	"database/sql"
	"errors"
	"fmt"
	"regexp"

	"github.com/go-ldap/ldap/v3"
	"github.com/jmoiron/sqlx"
)

// IsUserEnabled validates that a user account, as identified by a session token, is enabled in the directory
func IsUserEnabled(token string, db *sqlx.DB, ldapConn *ldap.Conn, baseDN string) (bool, error) {
	var err error
	var username string

	err = db.Get(&username, "SELECT username FROM users WHERE token = @p1", token)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return false, fmt.Errorf("user record does not exist in database: %w", err)
	}
	if err != nil {
		return false, fmt.Errorf("problem retrieving user record: %w", err)
	}

	// Then we run a search for the user we really want and return it's DN
	searchRequest := ldap.NewSearchRequest(
		baseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Person)(userPrincipalName=%s))", username),
		[]string{"userAccountControl"},
		nil,
	)

	sr, err := ldapConn.Search(searchRequest)
	if err != nil {
		return false, fmt.Errorf("error searching for user to authenticate: %w", err)
	}

	// 514 = Disabled account
	// 546 = Disabled, password not required
	// 66050 = Disabled, Password Doesn’t Expire
	// 66082 = Disabled, Password Doesn’t Expire & Not Required
	// 262658 = Disabled, Smartcard Required
	// 262690 = Disabled, Smartcard Required, Password Not Required
	// 328194 = Disabled, Smartcard Required, Password Doesn’t Expire
	// 328226 = Disabled, Smartcard Required, Password Doesn’t Expire & Not Required

	re := regexp.MustCompile(`(?m)^(?:514|546|66050|66082|262658|262690|328194|328226)$`)
	if len(re.FindStringIndex(sr.Entries[0].GetAttributeValue("userAccountControl"))) > 0 {
		return false, nil
	}

	return true, nil
}
