package authentication

import (
	"github.com/go-ldap/ldap/v3"
)

func parseGroups(user *ldap.Entry, baseDN string, conn *ldap.Conn) ([]string, error) {
	// groups contains the DNs of the groups to search for on each iteration.
	// The DNs in this group change on each iteration until there are none left and we have reached the top of the tree.
	var groups []string

	// groupDN will contain the DN of each group, including nested groups, that the user is a member of.
	// groupDN is the variable returned to the calling function.
	var groupDN []string

	groups = append(groups, user.GetAttributeValues("memberOf")...)

	for {
		// Temporary slice to contain DNs of groups which are the parent(s) for the specific group being searched in each loop.
		var nextLevelGroups []string

		for _, g := range groups {
			exit := false

			// Need to ensure we don't create duplicates.
			// If the DN of the group for this iteration matches one already in the groupDN slice
			// we need to skip this loop and move onto the next one.  Without this check we'd end up in an endless loop.
			for _, dn := range groupDN {
				if dn == g {
					exit = true
					break
				}
			}
			if exit {
				continue
			}

			groupDN = append(groupDN, g)

			// We need to see if the group is a member of any other groups (parents).
			parentGroups, err := getGroupMembers(g, baseDN, conn)
			if err != nil {
				return groupDN, err
			}

			// If there are any parent groups we assign them to a temporary slice for processing after the
			// current slice of groups has been checked.
			if len(parentGroups) > 0 {
				nextLevelGroups = append(nextLevelGroups, parentGroups...)
			}
		}

		// If there are no more parent groups to check then we must have reached the top of the tree.
		// If so we can exit the loop and return data to the calling function.
		if len(nextLevelGroups) == 0 {
			break
		}

		// If we're still here then there are still groups to search.
		// Start the loop again with the next set of groups in the tree.
		groups = nextLevelGroups
	}

	return groupDN, nil
}
