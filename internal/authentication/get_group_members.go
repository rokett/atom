package authentication

import (
	"fmt"

	"github.com/go-ldap/ldap/v3"
)

func getGroupMembers(dn string, baseDN string, conn *ldap.Conn) ([]string, error) {
	searchRequest := ldap.NewSearchRequest(
		baseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Group)(distinguishedName=%s))", ldap.EscapeFilter(dn)),
		[]string{"cn", "memberOf"},
		nil,
	)

	sr, err := conn.Search(searchRequest)
	if err != nil {
		return []string{}, fmt.Errorf("error searching for group details %s: %w", dn, err)
	}

	// Some groups return an empty slice; this is normally when it is a built-in group which cannot be a member of any other group.
	// Normal groups, which are not members of any other groups, are fine.
	if len(sr.Entries) > 0 {
		return sr.Entries[0].GetAttributeValues("memberOf"), nil
	}

	return []string{}, nil
}
