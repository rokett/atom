package authentication

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
)

func getGroupsChecksum(groups []string) (string, error) {
	groupString := strings.Join(groups, "#")

	bs, err := json.Marshal(groupString)
	if err != nil {
		return "", fmt.Errorf("unable to marshal groups to calculate checksum: %w", err)
	}

	checksum := sha256.Sum256(bs)

	return hex.EncodeToString(checksum[:]), nil
}
