package authentication

type role struct {
	Group      string
	Role       string
	Preference int
}
