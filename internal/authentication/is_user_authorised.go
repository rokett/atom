package authentication

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"errors"

	"github.com/go-ldap/ldap/v3"
	"github.com/jmoiron/sqlx"
)

// IsUserAuthorised authenticates the given username and password combo to an LDAP directory,
// checks group membership to ensure that the user is allowed access.
func IsUserAuthorised(DCs []string, port int, baseDN string, bindDN string, bindPwd string, creds Creds, db *sqlx.DB) (User, error) {
	conn, err := ConnectLdap(DCs, port)
	if err != nil {
		return User{}, err
	}

	// To bind we need to use a DN so we connect first using the system level DN
	err = conn.Bind(bindDN, bindPwd)
	if err != nil {
		return User{}, fmt.Errorf("error during LDAP bind: %w", err)
	}

	// Sanitise the input to remove wildcard character from username
	creds.Sanitise()

	// Then we run a search for the user we really want and return it's DN
	searchRequest := ldap.NewSearchRequest(
		baseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Person)(userPrincipalName=%s))", creds.AuthUsername),
		[]string{"dn", "memberOf", "givenName", "sn", "userPrincipalName"},
		nil,
	)

	sr, err := conn.Search(searchRequest)
	if err != nil {
		return User{}, fmt.Errorf("error searching for user to authenticate: %w", err)
	}

	if len(sr.Entries) < 1 {
		return User{}, errors.New("user does not exist")
	}

	if len(sr.Entries) > 1 {
		return User{}, errors.New("multiple users returned")
	}

	// 514 = Disabled account
	// 546 = Disabled, password not required
	// 66050 = Disabled, Password Doesn’t Expire
	// 66082 = Disabled, Password Doesn’t Expire & Not Required
	// 262658 = Disabled, Smartcard Required
	// 262690 = Disabled, Smartcard Required, Password Not Required
	// 328194 = Disabled, Smartcard Required, Password Doesn’t Expire
	// 328226 = Disabled, Smartcard Required, Password Doesn’t Expire & Not Required

	re := regexp.MustCompile(`(?m)^(?:514|546|66050|66082|262658|262690|328194|328226)$`)
	if len(re.FindStringIndex(sr.Entries[0].GetAttributeValue("userAccountControl"))) > 0 {
		return User{}, errors.New("user account is disabled")
	}

	// Then we attempt another bind as that user; this is what tells us if they can authenticate or not
	err = conn.Bind(sr.Entries[0].DN, creds.AuthPassword)
	if err != nil {
		return User{}, fmt.Errorf("authentication failed: %w", err)
	}

	// We need to get the user's group memberships, including any nested groups
	groupMembership, err := parseGroups(sr.Entries[0], baseDN, conn)
	if err != nil {
		return User{}, err
	}

	var roles []role
	err = db.Select(&roles, "SELECT [group], role, preference FROM roles")
	if err != nil {
		return User{}, fmt.Errorf("error checking application roles: %w", err)
	}

	groups := make(map[string]string)
	for _, role := range roles {
		p := strconv.Itoa(role.Preference)
		groups[role.Group] = fmt.Sprintf("%s:%s", role.Role, p)
	}

	var userRole string
	var allowedAccess bool
	rolePreference := 9999

	for _, g := range groupMembership {
		// If the user is a member of a group which is also defined in the application group mappings then they are allowed access
		if role, ok := groups[g]; ok {
			r := strings.Split(role, ":")

			preference, err := strconv.Atoi(r[1])
			if err != nil {
				return User{}, fmt.Errorf("unable to parse role preference: %w", err)
			}

			if preference < rolePreference {
				userRole = r[0]
				allowedAccess = true
				rolePreference = preference
			}
		}
	}

	if !allowedAccess {
		return User{}, errors.New("authentication failed; user needs to be a member of an allowed security group")
	}

	// Finally we switch the connection back to the system level DN
	err = conn.Bind(bindDN, bindPwd)
	if err != nil {
		return User{}, fmt.Errorf("error during anonymous LDAP bind: %w", err)
	}

	groupHash, err := getGroupsChecksum(groupMembership)
	if err != nil {
		return User{}, err
	}

	return User{
		FirstName: sr.Entries[0].GetAttributeValue("givenName"),
		Surname:   sr.Entries[0].GetAttributeValue("sn"),
		Username:  sr.Entries[0].GetAttributeValue("userPrincipalName"),
		Role:      userRole,
		GroupHash: groupHash,
	}, nil
}
