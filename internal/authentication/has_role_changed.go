package authentication

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/go-ldap/ldap/v3"
	"github.com/jmoiron/sqlx"
)

func hasRoleChanged(token string, db *sqlx.DB, ldapConn *ldap.Conn, baseDN string) (changed bool, groupMembership []string, groupHash string, err error) {
	var u User

	err = db.Get(&u, "SELECT username, group_hash FROM users WHERE token = @p1", token)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return false, groupMembership, groupHash, fmt.Errorf("user record does not exist in database: %w", err)
	}
	if err != nil {
		return false, groupMembership, groupHash, fmt.Errorf("problem retrieving user record: %w", err)
	}

	// Then we run a search for the user we really want and return it's DN
	searchRequest := ldap.NewSearchRequest(
		baseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectCategory=Person)(userPrincipalName=%s))", u.Username),
		[]string{"memberOf"},
		nil,
	)

	sr, err := ldapConn.Search(searchRequest)
	if err != nil {
		return false, groupMembership, groupHash, fmt.Errorf("error searching for user to authenticate: %w", err)
	}

	groupMembership, err = parseGroups(sr.Entries[0], baseDN, ldapConn)
	if err != nil {
		return false, groupMembership, groupHash, err
	}

	groupHash, err = getGroupsChecksum(groupMembership)
	if err != nil {
		return false, groupMembership, groupHash, err
	}

	if groupHash == u.GroupHash {
		return false, groupMembership, groupHash, nil
	}

	return true, groupMembership, groupHash, nil
}
