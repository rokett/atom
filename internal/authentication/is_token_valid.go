package authentication

import (
	"database/sql"
	"fmt"
	"time"

	"errors"

	"github.com/jmoiron/sqlx"
)

// IsTokenValid checks that the passed token exists in the database, and if it does validates that it has not expired.
func IsTokenValid(token string, db *sqlx.DB) (valid bool, u User, err error) {
	err = db.Get(&u, "SELECT username, token_expiry, role FROM users WHERE token = @p1", token)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return false, u, nil
	}
	if err != nil {
		return false, u, fmt.Errorf("problem checking for session token: %w", err)
	}

	if time.Now().After(u.TokenExpiry) {
		return false, u, errors.New("token has expired")
	}

	return true, u, nil
}
