package model

import "errors"

var ErrNoRecords = errors.New("no matching data found")
