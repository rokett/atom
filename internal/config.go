package model

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

// Config contains the representation of app config stored in the DB
type Config struct {
	Option string `json:"option"`
	Value  string `json:"value"`
}

type ConfigModel struct {
	DB     *sqlx.DB
	Config Config
}

func (c *ConfigModel) IsMaintenanceModeEnabled() (bool, error) {
	var cfg Config

	err := c.DB.Get(&cfg, "SELECT [value] FROM config WHERE [option] = 'maintenance_mode'")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return false, ErrNoRecords
	}
	if err != nil {
		return false, fmt.Errorf("unable to retrieve maintenance mode config: %w", err)
	}

	if cfg.Value == "off" {
		return false, nil
	}

	return true, nil
}

func (c *ConfigModel) ToggleMaintenanceMode() error {
	enabled, err := c.IsMaintenanceModeEnabled()
	if err != nil {
		return err
	}

	q := "UPDATE config SET [value] = 'on' WHERE [option] = 'maintenance_mode'"
	if enabled {
		q = "UPDATE config SET [value] = 'off' WHERE [option] = 'maintenance_mode'"
	}

	_, err = c.DB.Exec(q)
	if err != nil {
		return fmt.Errorf("unable to toggle maintenance mode: %w", err)
	}

	return nil
}
