package randomname

import (
	"fmt"
	"math/rand"
	"time"
)

// Generate a new two word name where the first word is an adjective and the second is a noun, separated by a hyphen.
func Generate() string {
	seed := time.Now().UTC().UnixNano()

	src := rand.NewSource(seed)
	r := rand.New(src)

	adjective := adjectives[r.Intn(len(adjectives))]
	noun := nouns[r.Intn(len(nouns))]

	return fmt.Sprintf("%s-%s", adjective, noun)
}
