package model

import (
	"database/sql"
	"fmt"
	"net/url"
	"time"

	"errors"

	"github.com/jmoiron/sqlx"
)

// EventTarget represents a target for sending job events to
type EventTarget struct {
	ID        int64     `json:"id"`
	Name      string    `json:"name"`
	Target    string    `json:"target"`
	EventType string    `json:"event_type" db:"event_type"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
}

type EventTargetModel struct {
	DB          *sqlx.DB
	EventTarget EventTarget
}

// Validate ensures that the job request to be run is valid
func (et *EventTarget) Validate() (ve []ValidationError, err error) {
	eventTypes := map[string]bool{
		"job.cancelled_timeout":   true,
		"job.complete":            true,
		"job.complete.warning":    true,
		"job.error":               true,
		"job.unauthorised_script": true,
		"job.error_starting":      true,
		"job.expired":             true,
		"*":                       true,
	}

	if et.Name == "" {
		ve = append(ve, ValidationError{
			Parameter: "name",
			Error:     "REQUIRED field",
		})
	}

	if et.Target == "" {
		ve = append(ve, ValidationError{
			Parameter: "target",
			Error:     "REQUIRED field",
		})
	} else {
		u, err := url.Parse(et.Target)
		if err != nil || u.Scheme == "" || u.Host == "" {
			ve = append(ve, ValidationError{
				Parameter: "target",
				Error:     "Invalid URL",
			})
		}
	}

	if et.EventType == "" {
		ve = append(ve, ValidationError{
			Parameter: "event_type",
			Error:     "REQUIRED field",
		})
	} else {
		if !eventTypes[et.EventType] {
			ve = append(ve, ValidationError{
				Parameter: "event_type",
				Error:     fmt.Sprintf("Not an allowed event type; %s", et.EventType),
			})
		}
	}

	if len(ve) > 0 {
		return ve, errors.New("validation failed")
	}

	return ve, nil
}

func (e *EventTargetModel) List() ([]EventTarget, error) {
	var eventTargets []EventTarget

	err := e.DB.Select(&eventTargets, "SELECT id, name, target, event_type FROM eventTargets ORDER BY name")
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return eventTargets, ErrNoRecords
	}
	if err != nil {
		return eventTargets, fmt.Errorf("unable to retrieve event targets: %w", err)
	}

	return eventTargets, nil
}

func (e *EventTargetModel) Get(id int) (EventTarget, error) {
	var eventTarget EventTarget

	err := e.DB.Get(&eventTarget, "SELECT id, name, target, event_type FROM eventTargets WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return eventTarget, ErrNoRecords
	}
	if err != nil {
		return eventTarget, fmt.Errorf("unable to retrieve event target with id %d: %w", id, err)
	}

	return eventTarget, nil
}

// Upsert updates an event target if it already exists, or creates a new one if it doesn't.
func (et *EventTarget) Upsert(db *sqlx.DB) (id int64, err error) {
	err = db.Get(&et.ID, "SELECT id FROM eventTargets WHERE name = @p1 AND target = @p2 AND event_type = @p3", et.Name, et.Target, et.EventType)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return 0, fmt.Errorf("unable to check for event target: %w", err)
	}

	if et.ID == 0 {
		id, err = et.create(db)
		if err != nil {
			return 0, fmt.Errorf("unable to add new event target: %w", err)
		}

		return id, nil
	}

	err = et.update(db)
	if err != nil {
		return 0, fmt.Errorf("unable to update event target: %w", err)
	}

	return et.ID, nil
}

func (et *EventTarget) create(db *sqlx.DB) (id int64, err error) {
	err = db.Get(&id, "INSERT INTO eventTargets (name, target, event_type) VALUES (@p1, @p2, @p3);SELECT id = convert(bigint, SCOPE_IDENTITY());", et.Name, et.Target, et.EventType)
	if err != nil {
		return id, fmt.Errorf("error adding new event target: %w", err)
	}

	return id, nil
}

func (et *EventTarget) update(db *sqlx.DB) (err error) {
	_, err = db.Exec("UPDATE eventTargets SET name = @p1, target = @p2, event_type = @p3 WHERE id = @p4;", et.Name, et.Target, et.EventType, et.ID)
	if err != nil {
		return fmt.Errorf("error updating event target: %w", err)
	}

	return nil
}
