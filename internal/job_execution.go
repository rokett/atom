package model

import (
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"
)

// JobExecution contains the representation of the job as stored in the database
type JobExecution struct {
	ID                            int64     `json:"id"`
	Executor                      string    `json:"executor"`
	Script                        string    `json:"script"`
	Parameters                    string    `json:"parameters"`
	Scope                         string    `json:"scope"`
	ExpireQueuedJobAfterTimestamp time.Time `json:"expire_queued_job_after_timestamp" db:"expire_queued_job_after_timestamp"`
	ExpireQueuedJobAfterDuration  string    `json:"expire_queued_job_after_duration" db:"expire_queued_job_after_duration"`
	Status                        string    `json:"status"`
	SequenceNumber                uint64    `json:"-" db:"sequence_number"`
	Requeued                      int64     `json:"requeued"`
	ExecutionUsername             string    `json:"execution_username" db:"execution_username"`
	ExecutionTimeMs               int64     `json:"execution_time_ms" db:"execution_time_ms"`
	ExecutionTimeFriendly         string    `json:"-"`
	ExecutionRequestedBy          string    `json:"execution_requested_by" db:"execution_requested_by"`
	ClientIP                      string    `json:"client_ip" db:"client_ip"`
	WorkerName                    string    `json:"worker_name" db:"worker_name"`
	WorkerIP                      string    `json:"worker_ip" db:"worker_ip"`
	WorkerServerName              string    `json:"worker_server_name" db:"worker_server_name"`
	WorkerOs                      string    `json:"worker_os" db:"worker_os"`
	ProcessID                     int       `json:"process_id" db:"process_id"`
	TraceID                       string    `json:"trace_id" db:"trace_id"`
	StartedAt                     time.Time `json:"started_at" db:"started_at"`
	FinishedAt                    time.Time `json:"finished_at" db:"finished_at"`
	CreatedAt                     time.Time `json:"created_at" db:"created_at"`
	UpdatedAt                     time.Time `json:"updated_at" db:"updated_at"`
	Log                           string    `json:"log"`
}

type JobExecutionModel struct {
	DB  *sqlx.DB
	Job JobExecution
}

func (j *JobExecutionModel) List(from, to time.Time, fields string, filters map[string]string) ([]JobExecution, error) {
	var jobs []JobExecution

	q := "SELECT"

	if fields != "" {
		q = fmt.Sprintf(`%s %s`, q, fields)
	} else {
		q = fmt.Sprintf(`%s id, executor, script, parameters, scope, expire_queued_job_after_duration, expire_queued_job_after_timestamp, status, sequence_number, execution_time_ms, execution_requested_by, execution_username, process_id, worker_name, worker_ip, worker_server_name, worker_os, client_ip, trace_id, log, started_at, finished_at, created_at, updated_at`, q)
	}

	var params []interface{}

	q = fmt.Sprintf("%s FROM jobExecutions WHERE created_at > @p1 AND created_at < @p2", q)
	params = append(params, from)
	params = append(params, to)

	paramIndex := 3

	for k, v := range filters {
		q = fmt.Sprintf(`%s AND %s LIKE @p%d`, q, k, paramIndex)
		params = append(params, fmt.Sprintf(`%%%s%%`, v))
		paramIndex++
	}

	q = fmt.Sprintf("%s ORDER BY created_at DESC", q)

	err := j.DB.Select(&jobs, q, params...)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return jobs, ErrNoRecords
	}
	if err != nil {
		return jobs, fmt.Errorf("unable to retrieve jobs: %w", err)
	}

	for k, job := range jobs {
		jobs[k] = j.sanitiseSecrets(job)
	}

	return jobs, nil
}

func (j *JobExecutionModel) Get(id int) (JobExecution, error) {
	var job JobExecution

	err := j.DB.Get(&job, "SELECT id, executor, script, parameters, scope, expire_queued_job_after_duration, expire_queued_job_after_timestamp, status, sequence_number, execution_time_ms, execution_requested_by, execution_username, process_id, worker_name, worker_ip, worker_server_name, worker_os, client_ip, trace_id, log, started_at, finished_at, created_at, updated_at FROM jobExecutions WHERE id = @p1;", id)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return job, ErrNoRecords
	}
	if err != nil {
		return job, fmt.Errorf("unable to get job with id %d: %w", id, err)
	}

	t, err := time.ParseDuration(strconv.FormatInt(job.ExecutionTimeMs, 10) + "ms")
	if err != nil {
		return job, fmt.Errorf("unable to humanise job execution time. Job ID %d: %w", id, err)
	}
	job.ExecutionTimeFriendly = t.String()

	job = j.sanitiseSecrets(job)

	return job, nil
}

func (j *JobExecutionModel) sanitiseSecrets(job JobExecution) JobExecution {
	var re = regexp.MustCompile(`(?m)-VaultToken ["'].*?["']`)
	var substitution = "-VaultToken '**********'"

	job.Parameters = re.ReplaceAllString(job.Parameters, substitution)

	return job
}

func (j *JobExecutionModel) SetSequenceNumber(job JobExecution) error {
	_, err := j.DB.Exec("UPDATE jobExecutions SET sequence_number = @p1 WHERE id = @p2", job.SequenceNumber, job.ID)
	if err != nil {
		return err
	}

	return nil
}

func (j *JobExecutionModel) SetStatus(job JobExecution, status string) error {
	allowedStatus := map[string]bool{
		"CANCELLED": true,
	}

	if !allowedStatus[status] {
		return fmt.Errorf("status is not valid: %s", status)
	}

	_, err := j.DB.Exec("UPDATE jobExecutions SET status = @p1 WHERE id = @p2", status, job.ID)
	if err != nil {
		return err
	}

	return nil
}
