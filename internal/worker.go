package model

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

// Worker contains the representation of a worker as stored in the database
type Worker struct {
	ID                 int64     `json:"id"`
	Name               string    `json:"name"`
	IP                 string    `json:"ip" db:"ip"`
	Os                 string    `json:"os" db:"os"`
	ServerName         string    `json:"server_name" db:"server_name"`
	Status             string    `json:"status"`
	HeartbeatTimestamp time.Time `json:"heartbeat_timestamp" db:"heartbeat_timestamp"`
	Groups             string    `json:"groups"`
	Version            string    `json:"version" db:"version"`
	CreatedAt          time.Time `json:"created_at" db:"created_at"`
	UpdatedAt          time.Time `json:"updated_at" db:"updated_at"`
}

type WorkerModel struct {
	DB     *sqlx.DB
	Worker Worker
}

func (w *WorkerModel) List(filters map[string]string) ([]Worker, error) {
	var workers []Worker

	q := "SELECT id, name, os, ip, server_name, status, groups, version, heartbeat_timestamp, created_at, updated_at FROM workers"

	var params []interface{}
	paramIndex := 1

	for k, v := range filters {
		if paramIndex == 1 {
			q = fmt.Sprintf(`%s WHERE %s LIKE @p%d`, q, k, paramIndex)
		} else {
			q = fmt.Sprintf(`%s AND %s LIKE @p%d`, q, k, paramIndex)
		}
		params = append(params, fmt.Sprintf(`%%%s%%`, v))
		paramIndex++
	}

	q = fmt.Sprintf(`%s ORDER BY name`, q)

	err := w.DB.Select(&workers, q, params...)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return workers, ErrNoRecords
	}
	if err != nil {
		return workers, fmt.Errorf("unable to retrieve workers: %w", err)
	}

	return workers, nil
}

func (w *WorkerModel) UpdateHeartbeat(worker string) error {
	_, err := w.DB.Exec("UPDATE workers SET heartbeat_timestamp = @p1 WHERE name = @p2", time.Now().UTC(), worker)
	if err != nil {
		return fmt.Errorf("UPDATE workers SET heartbeat_timestamp = %s WHERE name = %s: %w", time.Now().UTC(), worker, err)
	}

	return nil
}
