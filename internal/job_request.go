package model

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

// JobRequest contains the possible parameters that can be passed in the request body when a job is requested to be executed
type JobRequest struct {
	// REQUIRED parameter(s)
	Script   string `json:"script"`
	Scope    string `json:"scope"`
	Executor string `json:"executor"`

	// OPTIONAL parameters
	Parameters                   string `json:"parameters"`
	ExecutionUsername            string `json:"execution_username"`
	ExpireQueuedJobAfterDuration string `json:"expire_queued_job_after_duration"`
	ClientIP                     string
	TraceID                      string
}

// Validate ensures that the job request to be run is valid
func (j *JobRequest) Validate(allowedScripts []Script) (ve []ValidationError, err error) {
	// REQUIRED parameter validation
	if j.Script == "" {
		ve = append(ve, ValidationError{
			Parameter: "script",
			Error:     "REQUIRED field",
		})
	} else {
		var allowed bool
		for _, v := range allowedScripts {
			if v.Name == j.Script {
				allowed = true
				break
			}
		}

		if !allowed {
			ve = append(ve, ValidationError{
				Parameter: "script",
				Error:     fmt.Sprintf("Not an allowed script; %s", j.Script),
			})
		}
	}

	if j.Scope == "" {
		ve = append(ve, ValidationError{
			Parameter: "scope",
			Error:     "REQUIRED field; must be one of <group>:@any, <group>:@all, @all, @linux, @windows or a comma separated list of servers",
		})
	}

	//TODO Can we validate group names?
	if strings.HasPrefix(j.Scope, "@") && (j.Scope != "@all" && j.Scope != "@linux" && j.Scope != "@windows") {
		ve = append(ve, ValidationError{
			Parameter: "scope",
			Error:     "must be one of <group>:@any, <group>:@all, @all, @linux, @windows or a comma separated list of servers",
		})
	}

	validExecutors := map[string]bool{
		"ansible":    true,
		"powershell": true,
		"pwsh":       true,
		"binary":     true,
	}

	if !validExecutors[strings.ToLower(j.Executor)] {
		ve = append(ve, ValidationError{
			Parameter: "executor",
			Error:     "REQUIRED field; must be one of 'ansible', 'powershell', 'pwsh' or 'binary'",
		})
	}

	if j.ExpireQueuedJobAfterDuration != "" {
		_, err := time.ParseDuration(j.ExpireQueuedJobAfterDuration)
		if err != nil {
			ve = append(ve, ValidationError{
				Parameter: "expire_queued_job_after_duration",
				Error:     fmt.Sprintf("Invalid duration %s; %s", j.ExpireQueuedJobAfterDuration, err.Error()),
			})
		}
	}

	if len(ve) > 0 {
		return ve, errors.New("validation failed")
	}

	return ve, nil
}
