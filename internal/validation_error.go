package model

// ValidationError contains the parameter with the error and a friendly error message
type ValidationError struct {
	Parameter string `json:"parameter"`
	Error     string `json:"error"`
}
