package main

import (
	"regexp"
	"strconv"
	"strings"
)

func (app *application) setAnsibleJobStatus(log string) string {
	// We mark a job as errored if any of the servers have any failed tasks.
	// But only if the number of failed tasks is more than the number of ignored tasks for each server.
	// We'll return on the first time that the number of failed tasks is more than the number of ignored tasks.
	re := regexp.MustCompile(`(?is)PLAY RECAP \**\n(.*)`)
	matches := re.FindStringSubmatch(log)

	if len(matches) > 1 {
		results := strings.Split(matches[1], "\n")

		for _, result := range results {
			if strings.TrimSpace(result) == "" {
				continue
			}

			re = regexp.MustCompile(`(?i)failed=(?P<failed>\d*).*ignored=(?P<ignored>\d*)`)
			matches := app.findNamedMatches(re, result)

			if matches != nil {
				failed, err := strconv.Atoi(matches["failed"])
				if err != nil {
					app.logger.Error("unable to convert string to int",
						"error", err,
					)
					continue
				}

				ignored, err := strconv.Atoi(matches["ignored"])
				if err != nil {
					app.logger.Error("unable to convert string to int",
						"error", err,
					)
					continue
				}

				if failed > ignored {
					return "ERROR"
				}
			}
		}
	}

	if strings.Contains(log, "[WARNING]") {
		return "COMPLETED_WITH_WARNINGS"
	}

	return "COMPLETED"
}
