package main

import (
	"fmt"
)

// We generally only pause subscriptions when entering maintenance mode.
// Pausing means we unsubscribe from the subscriptions.  We unpause elsewhere by subscribing again.
// We want to keep worker to server comms open for management plane purposes, so we only pause job execution subscriptions.
func (app *application) pauseSubscriptions() {
	// Tell goroutines to cancel
	app.cancel()

	// And now wait for goroutines to signal that they are complete
	// If we don't, we unsubscribe from NATS stuff whilst the goroutines are still trying to fetch messages, which causes errors.
	app.goWait.Wait()

	activeSubs := map[string]bool{
		"allowed_scripts:update":           true,
		fmt.Sprintf("%s:ping", app.worker): true,
		"update_maintenance_mode_status":   true,
	}

	// Stop listening for new messages from NATS
	for subject, subscription := range app.subscriptions {
		if activeSubs[subject] {
			continue
		}

		// If a subscription is not active, then we can't unsubscribe from it.
		// It shouldn't be possible to have a subscription which is not active, as this implies we have subbed to it, and then unsubbed, without subbing again.
		// This check covers the potential error condition though.
		if !subscription.IsValid() {
			app.logger.Warn("unable to unsubscribe from inactive subscription",
				"nats_subject", subject,
			)

			continue
		}

		app.logger.Info("unsubscribing to pause worker",
			"nats_subject", subject,
		)

		err := subscription.Unsubscribe()
		if err != nil {
			app.logger.Error("unable to unsubscribe to pause worker",
				"error", err,
				"nats_subject", subject,
			)
		}
	}
}
