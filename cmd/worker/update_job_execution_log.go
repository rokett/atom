package main

import (
	"fmt"

	model "rokett.me/atom/internal"
)

func (app *application) updateJobExecutionLog(job model.JobExecution) error {
	subject := fmt.Sprintf("job_logs.%d", job.ID)

	_, err := app.js.Publish(subject, []byte(job.Log))
	if err != nil {
		return err
	}

	return nil
}
