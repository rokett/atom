package main

import (
	"syscall"

	model "rokett.me/atom/internal"
)

func (app *application) setProcessAttributes(_ *model.JobExecution) *syscall.SysProcAttr {
	return &syscall.SysProcAttr{
		Setpgid: true,
		Pgid:    0,
	}
}
