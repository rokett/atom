package main

import (
	"errors"
	"time"

	"github.com/nats-io/nats.go"
)

func (app *application) checkMaintenanceModeStatus() {
	msg, err := app.nc.Request("check_maintenance_mode_status", nil, 5*time.Second)
	if err != nil && errors.Is(err, nats.ErrNoResponders) {
		app.logger.Info("check maintenance mode; no responders")

		return
	}
	if err != nil && errors.Is(err, nats.ErrTimeout) {
		app.logger.Info("check maintenance mode; no response from server")

		return
	}
	if err != nil {
		app.logger.Error("error checking maintenance mode",
			"error", err,
		)

		return
	}

	if string(msg.Data) == "on" {
		app.logger.Info("maintenance mode enabled")
		app.maintenanceMode = true
		return
	}

	if string(msg.Data) == "off" {
		app.logger.Info("maintenance mode disabled")
		app.maintenanceMode = false
	}
}
