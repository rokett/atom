package main

import (
	"fmt"
	"os"
)

func isDirWritable(dir string) error {
	fi, err := os.Create(fmt.Sprintf("%s/temp.log", dir))
	if err != nil {
		return err
	}
	fi.Close()

	err = os.Remove(fmt.Sprintf("%s/temp.log", dir))
	if err != nil {
		return err
	}

	return nil
}
