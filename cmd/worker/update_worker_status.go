package main

import (
	"encoding/json"
	"time"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

// Workers need to tell the server when they are idle or busy.  Idle workers can accept new jobs, whereas busy workers cannot.
func (app *application) updateWorkerStatus(status string) {
	maxRetries := 10
	var err error

	worker := model.Worker{
		Name:   app.worker,
		Status: status,
	}

	pl, err := json.Marshal(worker)
	if err != nil {
		app.logger.Error(
			"converting worker payload to json",
			"error", err,
		)
		return
	}

	var msg *nats.Msg

	for retry := 1; retry <= maxRetries; retry++ {
		msg, err = app.nc.Request("worker:status_update", pl, 10*time.Second)
		if err != nil {
			app.logger.Error(
				"updating worker status; retrying",
				"error", err,
				"attempt", retry,
			)

			time.Sleep(time.Duration(retry) * (5 * time.Second))
			continue
		}
		break
	}

	if string(msg.Data) == "false" {
		app.logger.Error(
			"failed to update worker status",
			"error", err,
		)
		app.shutdownWorker()
	}
}
