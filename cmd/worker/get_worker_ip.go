package main

import (
	"log/slog"
	"net"
	"strings"
)

func getWorkerIP(logger *slog.Logger) (ip string) {
	conn, err := net.Dial("udp4", "192.168.0.1:80")
	if err != nil {
		logger.Error("unable to dial 192.168.0.1:80")

		return ""
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().String()
	idx := strings.LastIndex(localAddr, ":")

	ip = localAddr[0:idx]

	return ip
}
