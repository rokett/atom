package main

import (
	"regexp"
	"strings"
)

func (app *application) setPowerShellJobStatus(log string) string {
	if strings.Contains(log, "FullyQualifiedErrorId") {
		return "ERROR"
	}

	if strings.HasPrefix(log, "WARNING: The names of some imported commands from the module") {
		// If the first line is a warning about importing a module with unapproved verbs, we want to check if there are any more warnings
		var re = regexp.MustCompile(`(?m)WARNING: `)
		matches := re.FindAllString(log, -1)
		if len(matches) > 1 {
			return "COMPLETED_WITH_WARNINGS"
		}
	}

	if !strings.HasPrefix(log, "WARNING: The names of some imported commands from the module") && strings.Contains(log, "WARNING") {
		return "COMPLETED_WITH_WARNINGS"
	}

	return "COMPLETED"
}
