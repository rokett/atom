package main

import (
	"os"
)

func (app *application) shutdownWorker() {
	app.logger.Info("Exiting worker")

	// Tell goroutines to cancel
	// Depending on when the worker exits, it is possible that we don't have a context yet, hence the check
	if app.cancel != nil {
		app.cancel()
	}

	// And now wait for goroutines to signal that they are complete
	// If we don't, we unsubscribe from NATS stuff whilst the goroutines are still trying to fetch messages, which causes errors.
	app.goWait.Wait()

	// Stop listening for new messages from NATS
	for subject, subscription := range app.subscriptions {
		// If a subscription is not active, then we can't unsubscribe from it.
		// It shouldn't be possible to have a subscription which is not active, as this implies we have subbed to it, and then unsubbed, without subbing again.
		// This check covers the potential error condition though.
		if !subscription.IsValid() {
			app.logger.Warn("skipping unsubscribe from inactive subscription",
				"nats_subject", subject,
			)

			continue
		}

		app.logger.Info("unsubscribing",
			"nats_subject", subject,
		)

		err := subscription.Unsubscribe()
		if err != nil {
			app.logger.Error("unable to unsubscribe",
				"error", err,
				"nats_subject", subject,
			)
		}
	}

	// Wait for the current job to end, and then we can exit.
	app.jobWait.Wait()
	app.deregisterWorker()
	os.Exit(0)
}
