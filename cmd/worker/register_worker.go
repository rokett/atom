package main

import (
	"encoding/json"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

// Workers need to be registered with the server to accept jobs.
// If an error occurs when attempting to register, the worker needs to terminate because it is not in a safe state.
func (app *application) registerWorker() {
	worker := model.Worker{
		Name:       app.worker,
		IP:         app.ip,
		Os:         app.os,
		ServerName: app.hostname,
		Status:     "idle",
		Groups:     strings.Join(app.jobGroups, ","),
		Version:    app.version,
	}

	if app.maintenanceMode {
		worker.Status = "maintenance mode"
	}

	pl, err := json.Marshal(worker)
	if err != nil {
		app.logger.Error(
			"converting worker payload to json",
			"error", err,
		)
		app.shutdownWorker()
	}

	msg, err := app.nc.Request("worker:register", pl, 10*time.Second)
	if err != nil {
		app.logger.Error(
			"register worker",
			"error", err,
		)
		app.shutdownWorker()
	}

	if string(msg.Data) == "false" {
		app.logger.Error("failed to register worker")
		app.shutdownWorker()
	}
}
