package main

import (
	"os"
	"os/signal"
	"syscall"
)

// The close hander watches for an interrupt signal (ctrl+c) and then gracefully stops the programme.
// Gracefully here means letting running job(s) finish.
// func (app *application) setupCloseHandler(jobWait *sync.WaitGroup, subscriptions []*nats.Subscription) {
func (app *application) setupCloseHandler() {
	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-c
		app.shutdownWorker()
	}()
}
