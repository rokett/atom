package main

import (
	"encoding/binary"
	"encoding/json"
	"fmt"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

func (app *application) setupWorkerSubscriptions() {
	// The worker can only run scripts which have been approved by the server.
	// The server publishes a list of approved scripts at startup, and on any update to the list, so we setup a subscription to listen for the published list.
	scriptsSub, err := app.nc.Subscribe("allowed_scripts:update", func(m *nats.Msg) {
		var allowedScripts []model.Script

		err := json.Unmarshal(m.Data, &allowedScripts)
		if err != nil {
			app.logger.Error(
				"decoding allowed scripts",
				"error", err,
			)
			return
		}

		app.logger.Debug(
			"setting allowed scripts",
			"allowed_scripts", allowedScripts,
		)

		app.allowedScripts = allowedScripts
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "allowed_scripts:update",
		)
		app.shutdownWorker()
	}
	app.subscriptions["allowed_scripts:update"] = scriptsSub
	app.logger.Info(
		"subscribed",
		"nats_subject", "allowed_scripts:update",
	)

	mmSub, err := app.nc.Subscribe("update_maintenance_mode_status", func(m *nats.Msg) {
		if string(m.Data) == "on" {
			app.logger.Info("maintenance mode enabled")
			app.maintenanceMode = true
			app.updateWorkerStatus("maintenance mode")
			app.pauseSubscriptions()
			return
		}

		if string(m.Data) == "off" {
			app.logger.Info("maintenance mode disabled")
			app.maintenanceMode = false

			app.updateWorkerStatus("idle")

			// Setup subscription for jobs to run on a specific worker
			app.setupWorkerJobExecuteSubs()
		}
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "update_maintenance_mode_status",
		)
		app.shutdownWorker()
	}
	app.subscriptions["update_maintenance_mode_status"] = mmSub
	app.logger.Info(
		"subscribed",
		"nats_subject", "update_maintenance_mode_status",
	)

	jobCancelSubject := fmt.Sprintf("job.cancel:%s", app.worker)
	cancelSub, err := app.nc.Subscribe(jobCancelSubject, func(m *nats.Msg) {
		app.logger.Info(
			"received request to cancel job",
			"job_id", int64(binary.BigEndian.Uint64(m.Data)),
		)

		app.jobStore.Cancel(int64(binary.BigEndian.Uint64(m.Data)))
		app.nc.Publish(m.Reply, []byte("true"))
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subject", jobCancelSubject,
		)
		app.shutdownWorker()
	}
	app.subscriptions[jobCancelSubject] = cancelSub
	app.logger.Info(
		"subscribed",
		"nats_subject", jobCancelSubject,
	)
}
