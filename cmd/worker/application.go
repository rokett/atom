package main

import (
	"context"
	"log/slog"
	"net/http"
	"sync"
	"time"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

type application struct {
	environment    string
	logger         *slog.Logger
	apiClient      *http.Client
	cancel         context.CancelFunc
	ctx            context.Context
	nc             *nats.Conn
	js             nats.JetStreamContext
	worker         string
	hostname       string
	ip             string
	os             string
	allowedScripts []model.Script

	// To ensure we don't try and unsubscribe from NATS subscriptions before any goroutines have ended, which throws errors, we pass around a waitgroup.
	// Unsubscribing will wait until the goroutines signal that they have ended.
	goWait *sync.WaitGroup

	jobGroups []string

	// jobStore keeps track of running jobs and their cancel functions, meaning we can cancel any job from anywhere in the application
	jobStore *jobStore

	jobTimeout time.Duration

	// To ensure running jobs are allowed to finish when an interrupt signal is sent to stop the programme,
	// we setup a new waitgroup which is incremented before the job starts, and completed when the job finishes.
	// The programme close handler waits for the job waitgroup to be empty.
	jobWait *sync.WaitGroup

	natsHost     string
	natsPort     int
	natsNKeySeed string

	executionLogsPath      string
	psScriptsPath          string
	subscriptions          map[string]*nats.Subscription
	vaultAddr              string
	vaultToken             string
	vaultSecretsEnginePath string
	vaultSecretsPath       string
	defaultUser            string
	version                string
	maintenanceMode        bool
}
