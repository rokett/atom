package main

import (
	"context"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
)

func (app *application) setupWorkerJobExecuteSubs() {
	// In order to ensure we shutdown goroutines, we pass a context around and then cancel it when the application is requested to stop.
	ctx, cancel := context.WithCancel(context.Background())
	app.ctx = ctx
	app.cancel = cancel

	// The 'jobs.execute:<workername>' subject allows for the server to publish a job which should be run on a specific worker.
	// That means anything scoped to @all, <group>:@all, or <hostnames>.
	// The server determines which worker to send the job to.
	workerSubject := fmt.Sprintf("jobs.execute:%s", app.worker)
	workerSub, err := app.js.PullSubscribe(workerSubject, app.worker, nats.InactiveThreshold(6*time.Minute))
	if err != nil {
		app.logger.Error(fmt.Sprintf("unable to subscribe to '%s' queue", workerSubject),
			"error", err,
		)
		app.shutdownWorker()
	}
	app.subscriptions[workerSubject] = workerSub
	app.logger.Info("subscribed",
		"nats_subject", workerSubject,
	)

	app.goWait.Add(1)
	go app.fetchMessages(workerSub)

	// The 'jobs.execute:<group>:@any' subject allows for the server to publish a job that should run on 'any' workers in the defined group
	for _, group := range app.jobGroups {
		anySubject := fmt.Sprintf("jobs.execute:%s:@any", group)

		groupAnySub, err := app.js.PullSubscribe(anySubject, group)
		if err != nil {
			app.logger.Error(fmt.Sprintf("unable to subscribe to '%s' queue", anySubject),
				"error", err,
			)
			app.shutdownWorker()
		}
		app.subscriptions[anySubject] = groupAnySub
		app.logger.Info("subscribed",
			"nats_subject", anySubject,
		)

		app.goWait.Add(1)
		go app.fetchMessages(groupAnySub)
	}
}
