package main

import (
	"fmt"
	"os"
	"runtime"
	"strings"
)

func (app *application) setExecutionLogsPath(executionLogsPathFlg string) {
	switch strings.ToLower(runtime.GOOS) {
	case "windows":
		app.executionLogsPath = "execution_logs"
	case "linux":
		app.executionLogsPath = "/var/log/atom/execution_logs"
	}

	// Allow for user customisable execution logs path
	if executionLogsPathFlg != "" {
		app.executionLogsPath = executionLogsPathFlg
	}

	d, err := os.Stat(app.executionLogsPath)
	if os.IsNotExist(err) {
		app.logger.Error(fmt.Sprintf("%s does not exist", app.executionLogsPath))
		app.shutdownWorker()
	}
	if err != nil {
		app.logger.Error(fmt.Sprintf("there was a problem checking that the %s path exists", app.executionLogsPath),
			"error", err,
		)
		app.shutdownWorker()
	}
	if !d.IsDir() {
		app.logger.Error(fmt.Sprintf("%s is not a directory", app.executionLogsPath))
		app.shutdownWorker()
	}
	err = isDirWritable(app.executionLogsPath)
	if err != nil {
		app.logger.Error(fmt.Sprintf("unable to write to %s", app.executionLogsPath),
			"error", err,
		)
		app.shutdownWorker()
	}
}
