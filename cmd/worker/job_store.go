package main

import (
	"context"
	"sync"
)

type jobStore struct {
	sync.Mutex
	jobs map[int64]context.CancelFunc
}

// Add allows us to add an entry to the store where the key is the job ID and the value is the cancel func.
// We can use the cancel func later just by finding the correct job ID in the store.
func (js *jobStore) Add(id int64, cancelFunc context.CancelFunc) {
	js.Lock()
	defer js.Unlock()
	js.jobs[id] = cancelFunc
}

// Remove cleans up the job store by removing an entry indexed by the job ID
func (js *jobStore) Remove(id int64) {
	js.Lock()
	defer js.Unlock()
	delete(js.jobs, id)
}

// Cancel allows to cancel a job by it's ID
func (js *jobStore) Cancel(id int64) bool {
	js.Lock()
	defer js.Unlock()

	cancelFunc, exists := js.jobs[id]

	if exists {
		cancelFunc()
		delete(js.jobs, id)
		return true
	}

	return false
}

// NewJobStore initialises a fresh job store.  Used at application start.
func NewJobStore() *jobStore {
	return &jobStore{
		jobs: make(map[int64]context.CancelFunc),
	}
}
