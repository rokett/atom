package main

import (
	"fmt"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/nkeys"
)

func (app *application) setupNATSClient() (nc *nats.Conn, js nats.JetStreamContext) {
	totalWait := 10 * time.Minute
	reconnectDelay := 10 * time.Second
	connTimeout := 5 * time.Second

	// Create an NKey pair from the seed
	kp, err := nkeys.FromSeed([]byte(app.natsNKeySeed))
	if err != nil {
		app.logger.Error(
			"failed to create NKey pair from seed",
			"error", err,
		)
		os.Exit(1)
	}

	// Get the public key for the NKey (this is the user/account public key sent to the server)
	pubKey, err := kp.PublicKey()
	if err != nil {
		app.logger.Error(
			"failed to get the public key",
			"error", err,
		)
		os.Exit(1)
	}

	// NKeyOption requires a function for signing the nonce from the server
	sigCB := func(nonce []byte) ([]byte, error) {
		// Sign the nonce using the NKey pair (private key)
		sig, err := kp.Sign(nonce)
		if err != nil {
			app.logger.Error(
				"failed to sign the NKey pair",
				"error", err,
			)
			os.Exit(1)
		}
		return sig, nil
	}

	connOpts := []nats.Option{
		nats.Nkey(pubKey, sigCB),
		nats.ReconnectWait(reconnectDelay),
		nats.MaxReconnects(int(totalWait / reconnectDelay)),
		nats.Timeout(connTimeout),
		nats.DisconnectErrHandler(func(_ *nats.Conn, err error) {
			if err != nil {
				app.logger.Warn(fmt.Sprintf("Disconnected from NATS server: will attempt reconnects for %.0fm", totalWait.Minutes()),
					"error", err,
				)
			}
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			app.logger.Info("Connected to NATS server",
				"server", nc.ConnectedUrl(),
			)
		}),
		// If we have reached the 'MaxReconnects' value then the connection is marked as closed and will never be tried again.
		// Recovery from here requires the worker to be restarted.
		// Because the worker is technically still running, it is time to tell goroutines to cancel which stops the worker from trying to fetch messages.
		nats.ClosedHandler(func(nc *nats.Conn) {
			if nc.LastError() != nil {
				// Tell goroutines to cancel
				app.cancel()

				app.logger.Error("Failed to reconnect to NATS server; stopping attempts",
					"error", nc.LastError(),
				)
			}
		}),
	}

	natsConnString := fmt.Sprintf("nats://%s:%d", app.natsHost, app.natsPort)

	maxAttempts := 10
	var connErr error
	for attempt := 1; attempt <= maxAttempts; attempt++ {
		nc, connErr = nats.Connect(natsConnString, connOpts...)
		if connErr != nil {
			app.logger.Error("unable to connect to NATS server; retrying",
				"error", connErr,
				"nats_server", natsConnString,
			)

			time.Sleep(time.Duration(attempt) * time.Second)
			continue
		}

		break
	}

	if connErr != nil {
		app.logger.Error(
			"unable to connect to NATS server; run out of attempts",
			"error", connErr,
			"nats_server", natsConnString,
			"max_attempts", maxAttempts,
		)
		os.Exit(1)
	}

	js, err = nc.JetStream()
	if err != nil {
		nc.Close()

		app.logger.Error(
			"unable to create JetStream context",
			"error", err,
		)
		os.Exit(1)
	}

	return nc, js
}
