package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	model "rokett.me/atom/internal"
)

func (app *application) processStream(reader io.ReadCloser, streamName string, writer io.Writer, job *model.JobExecution) {
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text() // Get the current line
		line = strings.ToValidUTF8(line, "")
		fmt.Fprintf(writer, "%s\n", line)

		err := app.updateJobExecutionLog(model.JobExecution{
			ID:  job.ID,
			Log: line,
		})
		if err != nil {
			app.logger.Error("error sending log to jetstream",
				"error", err,
			)
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Printf("Error reading %s: %v", streamName, err)
	}
}
