package main

import (
	"fmt"
	"os"
	"strings"
)

func (app *application) testPSScriptsPath() {
	if app.psScriptsPath != "./" {
		if !strings.HasSuffix(app.psScriptsPath, "\\\\") && app.os != "linux" {
			app.psScriptsPath = strings.TrimSuffix(app.psScriptsPath, "\\")
			app.psScriptsPath = fmt.Sprintf("%s\\\\", app.psScriptsPath)
		}

		d, err := os.Stat(app.psScriptsPath)
		if os.IsNotExist(err) {
			app.logger.Error(fmt.Sprintf("%s does not exist", app.psScriptsPath))
			app.shutdownWorker()
		}
		if err != nil {
			app.logger.Error(fmt.Sprintf("there was a problem checking that the %s path exists", app.psScriptsPath),
				"error", err,
			)
			app.shutdownWorker()
		}
		if !d.IsDir() {
			app.logger.Error(fmt.Sprintf("%s is not a directory", app.psScriptsPath))
			app.shutdownWorker()
		}
	}
}
