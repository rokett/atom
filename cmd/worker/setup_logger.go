package main

import (
	"log/slog"
	"os"
	"runtime/debug"
)

func setupLogger(app, version, build, appEnv, workername string) *slog.Logger {
	opts := &slog.HandlerOptions{
		AddSource: true,
	}

	var handler slog.Handler = slog.NewTextHandler(os.Stdout, opts)
	if appEnv != "dev" {
		handler = slog.NewJSONHandler(os.Stdout, opts)
	}

	var goarch string
	var goos string
	buildInfo, ok := debug.ReadBuildInfo()
	if ok {
		for _, v := range buildInfo.Settings {
			if v.Key == "GOARCH" {
				goarch = v.Value
				continue
			}
			if v.Key == "GOOS" {
				goos = v.Value
				continue
			}
		}
	}

	logger := slog.New(handler).With(
		slog.String("app", app),
		slog.String("version", version),
		slog.String("build", build),
		slog.String("worker_name", workername),
		slog.String("environment", appEnv),
		slog.String("go_version", buildInfo.GoVersion),
		slog.String("goarch", goarch),
		slog.String("goos", goos),
	)

	return logger
}
