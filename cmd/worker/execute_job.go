package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) executeJob(job *model.JobExecution) {
	var err error

	jobUpdate := model.JobExecution{
		ID:                   job.ID,
		Executor:             job.Executor,
		Script:               job.Script,
		ExecutionRequestedBy: job.ExecutionRequestedBy,
		ExecutionUsername:    job.ExecutionUsername,
		WorkerName:           app.worker,
		WorkerIP:             app.ip,
		WorkerServerName:     app.hostname,
		WorkerOs:             app.os,
		Status:               "RECEIVED_BY_WORKER",
	}

	err = app.updateJobExecution(jobUpdate)
	if err != nil {
		app.logger.Error("error during request to update job execution; continuing anyway",
			"error", err,
			"requestor", job.ExecutionRequestedBy,
		)
	}

	var allowed bool
	for _, v := range app.allowedScripts {
		if v.Name == job.Script {
			allowed = true
			break
		}
	}

	if !allowed {
		app.logger.Error(
			"unable to run job; script not allowed",
			"script", job.Script,
			"requestor", job.ExecutionRequestedBy,
		)

		jobUpdate.Status = "UNAUTHORISED_SCRIPT"

		err = app.updateJobExecution(jobUpdate)
		if err != nil {
			app.logger.Error("error during request to update job execution",
				"error", err,
				"requestor", job.ExecutionRequestedBy,
			)
		}

		return
	}

	jobUpdate.Status = "ACCEPTED"

	err = app.updateJobExecution(jobUpdate)
	if err != nil {
		app.logger.Error("error during request to update job execution; continuing anyway",
			"error", err,
			"requestor", job.ExecutionRequestedBy,
		)
	}

	// We name the execution log file the same as the script being run
	logfilename := job.Script

	var cmd *exec.Cmd

	//TODO We want to allow a worker configured global timeout, and maybe a per job timeout as well?
	timeoutCtx, cancelTimeout := context.WithTimeout(context.Background(), app.jobTimeout)
	defer cancelTimeout()

	ctx, cancel := context.WithCancel(timeoutCtx)

	// Adding the cancel func to the job store allows us to cancel specific jobs by passing an ID to the app.jobStore.Cancel func
	app.jobStore.Add(job.ID, cancel)

	switch job.Executor {
	case "powershell":
		var args = []string{
			fmt.Sprintf("%s%s %s", app.psScriptsPath, job.Script, job.Parameters),
		}
		cmd = exec.CommandContext(ctx, `powershell.exe`, args...)
	case "pwsh":
		var args = []string{
			"-File",
			fmt.Sprintf("%s%s", app.psScriptsPath, job.Script),
		}
		args = append(args, strings.Split(job.Parameters, " ")...)
		cmd = exec.CommandContext(ctx, `pwsh`, args...)
	case "ansible":
		args := strings.Split(job.Parameters, " ")
		args = append(args, job.Script)
		cmd = exec.CommandContext(ctx, `ansible-playbook`, args...)

		// We need just the playbook name, not the whole path, for creating the execution log
		logfilename = logfilename[strings.LastIndex(logfilename, "/")+1:]
	case "binary":
		args := strings.Split(job.Parameters, " ")
		cmd = exec.CommandContext(ctx, job.Script, args...)

		// We need just the binary name, not the whole path, for creating the execution log
		if app.os == "linux" {
			logfilename = logfilename[strings.LastIndex(logfilename, "/")+1:]
		}

		if app.os == "windows" {
			logfilename = logfilename[strings.LastIndex(logfilename, "\\")+1:]
		}
	}

	// Populate Atom specific environment variables now. These can be used within the script.
	cmd.Env = os.Environ()
	cmd.Env = append(cmd.Env, fmt.Sprintf("ATOM_JOB_ID=%d", job.ID))
	cmd.Env = append(cmd.Env, fmt.Sprintf("ATOM_JOB_REQUESTED_BY=%s", job.ExecutionRequestedBy))
	cmd.Env = append(cmd.Env, fmt.Sprintf("ATOM_JOB_EXECUTION_USERNAME=%s", job.ExecutionUsername))

	// In the event of an interrupt signal being sent to the main programme (ctrl+c) a job should be allowed to finish gracefully.
	// To do this we need to run it in a separate programme group.
	// Linux and Windows handle this differently.
	cmd.SysProcAttr = app.setProcessAttributes(job)

	logfile, err := os.Create(fmt.Sprintf("%s/%s_%s.log", app.executionLogsPath, logfilename, time.Now().UTC().Format("2006-01-02T150405Z0700")))
	if err != nil {
		app.logger.Error("unable to create execution log file",
			"error", err,
			"log", fmt.Sprintf("%s_%s.log", job.Script, time.Now().UTC().Format("2006-01-02T150405Z0700")),
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)

		cancel()

		return
	}
	defer logfile.Close()

	var outputBuffer bytes.Buffer

	// Get stdout and stderr pipes
	stdoutPipe, err := cmd.StdoutPipe()
	if err != nil {
		app.logger.Error(
			"failed to get stdout pipe",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
		os.Exit(1)
	}

	stderrPipe, err := cmd.StderrPipe()
	if err != nil {
		app.logger.Error(
			"failed to get stderr pipe",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
		os.Exit(1)
	}

	multiWriter := io.MultiWriter(&outputBuffer, logfile)
	// For development purposes, we want to be able to see the job output directly in the terminal
	if app.environment == "dev" {
		multiWriter = io.MultiWriter(os.Stdout, &outputBuffer, logfile)
	}

	jobUpdate.Status = "STARTING"
	jobUpdate.StartedAt = time.Now().UTC()

	err = app.updateJobExecution(jobUpdate)
	if err != nil {
		app.logger.Error("error during request to update job execution; continuing anyway",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
	}

	// Start a timer so that we can log script execution time
	start := time.Now()

	// Start the command (but don't wait for it to finish)
	err = cmd.Start()
	if err != nil {
		app.logger.Error("unable to start command",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)

		jobUpdate.Log = strings.ToValidUTF8(outputBuffer.String(), "")
		jobUpdate.Log = fmt.Sprintf("%s\n\n%s", jobUpdate.Log, err.Error())

		jobUpdate.Status = "ERROR_STARTING"
		if (job.Executor == "powershell" || job.Executor == "pwsh") && (strings.Contains(jobUpdate.Log, "Throw") && strings.Contains(jobUpdate.Log, "RuntimeException")) {
			jobUpdate.Status = "ERROR"
		}

		err = app.updateJobExecution(jobUpdate)
		if err != nil {
			app.logger.Error("error during request to update job execution",
				"error", err,
				"command", cmd.String(),
				"requestor", job.ExecutionRequestedBy,
			)
		}

		app.jobStore.Remove(job.ID)

		return
	}

	app.logger.Info("executing job",
		"job_id", job.ID,
		"process_id", cmd.Process.Pid,
		"command", cmd.String(),
		"requestor", job.ExecutionRequestedBy,
	)

	jobUpdate.Status = "RUNNING"
	jobUpdate.ProcessID = cmd.Process.Pid

	err = app.updateJobExecution(jobUpdate)
	if err != nil {
		app.logger.Error(
			"request to update job execution; continuing anyway",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
	}

	// Capture stdout line by line
	go app.processStream(stdoutPipe, "STDOUT", multiWriter, job)
	go app.processStream(stderrPipe, "STDERR", multiWriter, job)

	err = cmd.Wait()
	if ctx.Err() == context.DeadlineExceeded {
		app.logger.Info(
			"cancelled job due to timeout",
			"job_id", job.ID,
			"job_timeout", app.jobTimeout, //TODO We want to allow a worker configured global timeout, and maybe a per job timeout as well?
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
		jobUpdate.Status = "CANCELLED_TIMEOUT"
	} else if ctx.Err() == context.Canceled {
		app.logger.Info(
			"cancelled job due to request from operator",
			"job_id", job.ID,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
		jobUpdate.Status = "CANCELLED_MANUAL"
	} else if err != nil {
		app.logger.Error(
			"unable to wait for command to finish",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)

		jobUpdate.Log = strings.ToValidUTF8(outputBuffer.String(), "")
		jobUpdate.Log = fmt.Sprintf("%s\n\n%s", jobUpdate.Log, err.Error())

		jobUpdate.Status = "ERROR_STARTING"
		if (job.Executor == "powershell" || job.Executor == "pwsh") && (strings.Contains(jobUpdate.Log, "Throw") && strings.Contains(jobUpdate.Log, "RuntimeException")) {
			jobUpdate.Status = "ERROR"
		}

		err = app.updateJobExecution(jobUpdate)
		if err != nil {
			app.logger.Error("error during request to update job execution",
				"error", err,
				"command", cmd.String(),
				"requestor", job.ExecutionRequestedBy,
			)
		}

		app.jobStore.Remove(job.ID)

		return
	}

	app.jobStore.Remove(job.ID)

	jobUpdate.ExecutionTimeMs = time.Since(start).Milliseconds()

	jobUpdate.Log = strings.ToValidUTF8(outputBuffer.String(), "")
	jobUpdate.FinishedAt = time.Now().UTC()

	if jobUpdate.Status != "CANCELLED_TIMEOUT" && jobUpdate.Status != "CANCELLED_MANUAL" {
		switch job.Executor {
		case "powershell", "pwsh":
			jobUpdate.Status = app.setPowerShellJobStatus(jobUpdate.Log)
		case "ansible":
			jobUpdate.Status = app.setAnsibleJobStatus(jobUpdate.Log)
		case "binary":
			jobUpdate.Status = app.setBinaryJobStatus(jobUpdate.Log)
		}
	}

	app.logger.Info("completed job",
		"job_id", job.ID,
		"process_id", cmd.Process.Pid,
		"command", cmd.String(),
		"requestor", job.ExecutionRequestedBy,
	)

	err = app.updateJobExecution(jobUpdate)
	if err != nil {
		app.logger.Error("error during request to update job execution",
			"error", err,
			"command", cmd.String(),
			"requestor", job.ExecutionRequestedBy,
		)
	}
}
