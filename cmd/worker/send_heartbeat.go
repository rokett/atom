package main

import "time"

func (app *application) sendHeartbeat() {
	var err error

	for {
		err = app.nc.Publish("worker:heartbeat", []byte(app.worker))
		if err != nil {
			app.logger.Error(
				"error publishing heartbeat",
				"error", err,
			)
		}

		time.Sleep(1 * time.Minute)
	}
}
