package main

import (
	"strings"
)

func (app *application) setBinaryJobStatus(log string) string {
	if strings.Contains(log, "error") {
		return "ERROR"
	}

	if strings.Contains(log, "warning") {
		return "COMPLETED_WITH_WARNINGS"
	}

	return "COMPLETED"
}
