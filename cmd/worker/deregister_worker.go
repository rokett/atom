package main

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

// When a worker is shutting down we ideally need to nicely deregister it so that the server does not try to send any further job execution requests.
// If an error occurs we continue with the rest of the shutdown process anyway, as the server will realise the worker no longer exists because that process is eventually consistent.
func (app *application) deregisterWorker() {
	maxRetries := 10
	var err error

	// Workers are uniquely identified by their name and IP
	worker := model.Worker{
		Name:   app.worker,
		IP:     app.ip,
		Groups: strings.Join(app.jobGroups, ","),
	}

	pl, err := json.Marshal(worker)
	if err != nil {
		app.logger.Error(
			"converting worker payload to json",
			"error", err,
		)
		return
	}

	var msg *nats.Msg

	for retry := 1; retry <= maxRetries; retry++ {
		msg, err = app.nc.Request("worker:deregister", pl, 10*time.Second)
		if err != nil {
			app.logger.Error(
				"deregistering worker; retrying",
				"error", err,
				"attempt", retry,
			)

			time.Sleep(time.Duration(retry) * (5 * time.Second))
			continue
		}
		break
	}

	if string(msg.Data) == "false" {
		app.logger.Error(
			"failed to deregister worker",
			"error", err,
		)
	}
}
