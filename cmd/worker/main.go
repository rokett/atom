package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
	"rokett.me/atom/internal/randomname"
)

var (
	app     = "Atom"
	version string
	build   string

	versionFlg                = flag.Bool("version", false, "Display application version")
	envFlg                    = flag.String("application.environment", "dev", "Environment the application is running in")
	natsHost                  = flag.String("nats.host", "127.0.0.1", "Host running the NATS server")
	natsPort                  = flag.Int("nats.port", 4222, "Port that NATS is listening on")
	natsNKeySeedFlg           = flag.String("nats.nkey_seed", "", "The NKey seed for the Atom user")
	jobGroupsFlg              = flag.String("job.groups", "", "Comma separated list of job groups that worker will listen for")
	jobTimeoutFlg             = flag.String("job.timeout", "1h", "Set a default timeout after which a job should be forcibly cancelled; for example 1h for 1 hour.  Valid units are 's', 'm' or 'h'.")
	vaultAddrFlg              = flag.String("vault.address", "http://127.0.0.1:8200", "Address of Vault API in the form 'http://127.0.0.1:8200'")
	vaultTokenFlg             = flag.String("vault.token", "dev-only-token", "Token for read-only access to Vault")
	vaultSecretsEnginePathFlg = flag.String("vault.secrets_engine_path", "secrets", "Mount path of the secrets engine where Atom secrets are stored")
	vaultSecretsPathFlg       = flag.String("vault.secrets_path", "Atom", "Path to secrets stored for use by Atom")
	helpFlg                   = flag.Bool("help", false, "Display application help")
	executionLogsPathFlg      = flag.String("execution.logs_path", "", "path to folder where execution logs should be stored")
	psScriptsPathFlg          = flag.String("ps.scripts_path", "./", "path to folder where PS scripts are stored")
	defaultUserFlg            = flag.String("default_user", "", "Default user to run jobs as, if not whatever the worker is running as")
)

func main() {
	flag.Parse()

	if *versionFlg {
		fmt.Printf("%s v%s build %s\n", app, version, build)
		os.Exit(0)
	}

	if *helpFlg {
		flag.PrintDefaults()
		os.Exit(0)
	}

	workername := randomname.Generate()

	logger := setupLogger(app, version, build, *envFlg, workername)

	hostname, err := os.Hostname()
	if err != nil {
		logger.Error(
			"error getting worker hostname",
			"error", err,
		)
		os.Exit(1)
	}

	// Check for defined job groups.
	// If we don't check for an empty string here, and only split the string into a slice if we have content, we end up with a slice with a length of 1, which means the worker subscribes to dummy queues.
	var jg []string
	if *jobGroupsFlg != "" {
		jg = strings.Split(*jobGroupsFlg, ",")
	}

	jobTimeout, err := time.ParseDuration(*jobTimeoutFlg)
	if err != nil {
		logger.Error(
			"parsing job.timeout duration",
			"error", err,
		)
		os.Exit(1)
	}

	app := &application{
		environment: *envFlg,
		logger:      logger,
		apiClient: &http.Client{
			Timeout: time.Second * 10,
		},
		worker:                 workername,
		hostname:               hostname,
		ip:                     getWorkerIP(logger),
		os:                     strings.ToLower(runtime.GOOS),
		allowedScripts:         []model.Script{},
		goWait:                 &sync.WaitGroup{},
		jobGroups:              jg,
		jobStore:               NewJobStore(),
		jobTimeout:             jobTimeout,
		jobWait:                &sync.WaitGroup{},
		natsHost:               *natsHost,
		natsPort:               *natsPort,
		natsNKeySeed:           *natsNKeySeedFlg,
		psScriptsPath:          *psScriptsPathFlg,
		subscriptions:          make(map[string]*nats.Subscription),
		vaultAddr:              *vaultAddrFlg,
		vaultToken:             *vaultTokenFlg,
		vaultSecretsEnginePath: *vaultSecretsEnginePathFlg,
		vaultSecretsPath:       *vaultSecretsPathFlg,
		defaultUser:            *defaultUserFlg,
		version:                version,
		maintenanceMode:        false,
	}

	nc, js := app.setupNATSClient()
	defer nc.Close()

	app.nc = nc
	app.js = js

	app.testPSScriptsPath()
	app.setExecutionLogsPath(*executionLogsPathFlg)
	app.setupWorkerSubscriptions()
	app.checkMaintenanceModeStatus()

	// To cater for the server being started before the worker, for example a worker restarting, we request a list of allowed scripts from the server.
	// The request/reply pattern blocks until the timeout is reached or a reply is received.
	// If we can't retrieve the list of allowed scripts we exit.
	msg, err := app.nc.Request("allowed_scripts:request", []byte("allowed_scripts"), 10*time.Second)
	if err != nil {
		app.logger.Error(
			"error with request to return allowed scripts",
			"error", err,
		)
		app.shutdownWorker()
	}
	err = json.Unmarshal(msg.Data, &app.allowedScripts)
	if err != nil {
		app.logger.Error(
			"decoding allowed scripts",
			"error", err,
		)
		app.shutdownWorker()
	}

	app.registerWorker()

	// The worker sends a heartbeat to the server every 1m
	go app.sendHeartbeat()

	if !app.maintenanceMode {
		// Setup job execution subscriptions
		app.setupWorkerJobExecuteSubs()
	}

	app.setupCloseHandler()

	app.logger.Debug("Worker running; waiting for jobs")

	// The waitgroup is used to block forever.  It will wait until the waitgroup equals 0, which it will never do.
	// The programme will therefore only end when an interrupt signal is sent.
	appWg := sync.WaitGroup{}
	appWg.Add(1)
	appWg.Wait()
}
