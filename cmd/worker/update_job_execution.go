package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

func (app *application) updateJobExecution(job model.JobExecution) error {
	maxRetries := 10
	var err error
	var msg *nats.Msg

	pl, err := json.Marshal(job)
	if err != nil {
		app.logger.Error(
			"converting job payload to json",
			"error", err,
		)
		return fmt.Errorf("failed to jsonify job execution payload: %w", err)
	}

	// Debugging code to determine if we have payloads > 900KB in size, to account for metadata.
	// Max size of a message in NATS is 1MB,.
	if len(pl) > 900*1024 {
		app.logger.Error("job payload size is too large")
	}

	for retry := 1; retry <= maxRetries; retry++ {
		msg, err = app.nc.Request("job:execution_update", pl, 10*time.Second)
		if err != nil {
			app.logger.Error("updating job execution status; retrying",
				"job_id", job.ID,
				"error", err,
				"attempt", retry,
			)

			time.Sleep(time.Duration(retry) * (5 * time.Second))
			continue
		}
		break
	}

	if string(msg.Data) == "false" {
		return fmt.Errorf("failed to update job execution status: %w", err)
	}

	return nil
}
