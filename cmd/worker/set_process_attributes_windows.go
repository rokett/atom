package main

import (
	"context"
	"strings"
	"syscall"
	"unsafe"

	vault "github.com/hashicorp/vault/api"
	"golang.org/x/sys/windows"
	model "rokett.me/atom/internal"
)

var (
	advapi32         = syscall.NewLazyDLL("advapi32.dll")
	logonProc        = advapi32.NewProc("LogonUserW")
	secureZeroMemory = advapi32.NewProc("SecureZeroMemory")
)

func (app *application) setProcessAttributes(job *model.JobExecution) *syscall.SysProcAttr {
	// The job may have been requested to be run under a specific user context.
	// This is normally done where the local system context is not correct; the job needs access to file shares for example.
	// We grab the password from Vault, which must have been stored there previously with the key being the username.
	// The details are then passed to the logonUser function, which returns a token to be used by Windows to change the user.
	// If the job does not require a specific user account, but a default account has been specified, we'll use that instead.
	// Otherwise the job will run under the context of the account running the worker process.
	if job.ExecutionUsername == "" && app.defaultUser != "" {
		job.ExecutionUsername = app.defaultUser
	}
	if job.ExecutionUsername != "" {
		config := vault.DefaultConfig()

		config.Address = app.vaultAddr

		client, err := vault.NewClient(config)
		if err != nil {
			app.logger.Error("unable to initialise Vault client",
				"error", err,
			)

			return nil
		}

		client.SetToken(app.vaultToken)

		secret, err := client.KVv2(app.vaultSecretsEnginePath).Get(context.Background(), app.vaultSecretsPath)
		if err != nil {
			app.logger.Error("unable to read secret",
				"error", err,
			)

			return nil
		}

		executionPwd, ok := secret.Data[job.ExecutionUsername].(string)
		if !ok {
			app.logger.Error("value type assertion failed")

			return nil
		}

		token, err := logonUser(job.ExecutionUsername, executionPwd)
		if err != nil {
			app.logger.Error("authenticating job execution user",
				"username", job.ExecutionUsername,
				"error", err,
			)

			return nil
		}

		return &syscall.SysProcAttr{
			CreationFlags: syscall.CREATE_NEW_PROCESS_GROUP,
			Token:         syscall.Token(token),
		}
	}

	return &syscall.SysProcAttr{
		CreationFlags: syscall.CREATE_NEW_PROCESS_GROUP,
	}
}

func logonUser(user, pass string) (syscall.Handle, error) {
	const (
		LOGON32_LOGON_NETWORK     = uintptr(3)
		LOGON32_PROVIDER_DEFAULT  = uintptr(0)
		LOGON32_LOGON_INTERACTIVE = uintptr(2)
	)

	var token syscall.Handle

	pd := []uint16{uint16('.'), 0}

	var err error
	if strings.Contains(user, "@") {
		pd, err = windows.UTF16FromString("")
		if err != nil {
			return token, err
		}
	}

	var pu, pp []uint16
	pu, err = windows.UTF16FromString(user)
	if err != nil {
		return token, err
	}
	pp, err = windows.UTF16FromString(pass)
	if err != nil {
		return token, err
	}

	rc, _, ec := syscall.SyscallN(logonProc.Addr(),
		uintptr(unsafe.Pointer(&pu[0])),
		uintptr(unsafe.Pointer(&pd[0])),
		uintptr(unsafe.Pointer(&pp[0])),
		LOGON32_LOGON_NETWORK,
		LOGON32_PROVIDER_DEFAULT,
		uintptr(unsafe.Pointer(&token)),
	)

	/* https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/aa366877(v=vs.85)
	r1, _, e1 := syscall.SyscallN(secureZeroMemory.Addr(),
		uintptr(unsafe.Pointer(&pp[0])),
		size of pp
	)*/

	if rc == 0 {
		return token, error(ec)
	}
	return token, nil
}
