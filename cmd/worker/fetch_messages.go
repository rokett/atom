package main

import (
	"encoding/json"
	"time"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

func (app *application) fetchMessages(sub *nats.Subscription) {
	defer app.goWait.Done()

	for {
		select {
		case <-app.ctx.Done():
			return
		default:
			// If the worker has lost connection to NATS it will try to reconnect until the 'MaxReconnects' value is reached.
			// We don't want to try and fetch messages whilst disconnected, so the worker will sleep for a bit before trying again.
			if app.nc.IsReconnecting() {
				time.Sleep(30 * time.Second)
				continue
			}

			msgs, err := sub.Fetch(1)
			// If there are no messages waiting then the timeout will be reached and an error thrown.
			// That's not really an error though so we'll ignore it.
			if err != nil && err == nats.ErrTimeout {
				continue
			}
			if err != nil && err != nats.ErrTimeout {
				app.logger.Error("unable to fetch messages",
					"error", err,
					"nats_subject", sub.Subject,
				)
				continue
			}

			for _, msg := range msgs {
				var job model.JobExecution

				err := json.Unmarshal(msg.Data, &job)
				if err != nil {
					app.logger.Error("error decoding JSON encoded job execution request",
						"error", err,
						"nats_subject", sub.Subject,
					)

					continue
				}

				if time.Now().UTC().After(job.ExpireQueuedJobAfterTimestamp) {
					job.WorkerName = app.worker
					job.WorkerIP = app.ip
					job.WorkerServerName = app.hostname
					job.WorkerOs = app.os
					job.Status = "EXPIRED"

					err = app.updateJobExecution(job)
					if err != nil {
						app.logger.Error("error during request to update job execution; continuing anyway",
							"error", err,
						)
					}

					msg.Term()
					continue
				}

				err = msg.AckSync()
				if err != nil {
					app.logger.Error("unable to ack message",
						"error", err,
						"job_id", job.ID,
					)
				}

				app.jobWait.Add(1)
				app.updateWorkerStatus("busy")
				app.executeJob(&job)
				app.updateWorkerStatus("idle")
				app.jobWait.Done()
			}
		}
	}
}
