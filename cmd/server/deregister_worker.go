package main

import (
	"fmt"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) deregisterWorker(worker model.Worker) error {
	maxRetries := 10
	var dbNotThere bool
	var err error

	for retry := 1; retry <= maxRetries; retry++ {
		_, err = app.db.NamedExec("DELETE FROM workers WHERE name = :name and ip = :ip", worker)
		if err != nil && (strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host") || strings.Contains(err.Error(), "No connection could be made because the target machine actively refused it")) {
			dbNotThere = true
			time.Sleep(time.Duration(retry) * (10 * time.Second))
			continue
		}
		if err != nil {
			return fmt.Errorf("DELETE FROM workers WHERE name = '%s' and ip = '%s': %w", worker.Name, worker.IP, err)
		}

		dbNotThere = false
		break
	}

	if dbNotThere {
		return fmt.Errorf("DELETE FROM workers WHERE name = '%s' and ip = '%s': %w", worker.Name, worker.IP, err)
	}

	go app.setWorkerMetrics()

	return nil
}
