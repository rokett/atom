package main

import model "rokett.me/atom/internal"

// Retrieve a list of approved scripts from the database, and then publish them to any listening workers
func (app *application) initialiseAllowedScripts() (allowedScripts []model.Script) {
	allowedScripts = app.getAllowedScripts()

	app.publishAllowedScripts(allowedScripts)

	return allowedScripts
}
