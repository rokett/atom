package main

import (
	"net/http"

	model "rokett.me/atom/internal"
)

func (app *application) returnAllScheduledJobsHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		jobs := []model.JobSchedule{}

		q := "SELECT id, executor, script, parameters, scope, execution_username, schedule, expire_queued_job_after_duration, paused, created_at, updated_at FROM jobs"

		err := app.db.Select(&jobs, q)
		if err != nil {
			app.logger.Error("error retrieving scheduled jobs",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving scheduled jobs"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		APIResponse.Result = jobs
		APIResponse.Send(http.StatusOK, w)
	})
}
