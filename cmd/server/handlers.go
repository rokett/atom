package main

import (
	"net/http"
	"strings"
)

func (app *application) showApiDocs(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "api.page.html", &templateData{})
}

func (app *application) showAdminPage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	app.render(w, r, "admin.page.html", &templateData{
		ActivePage:      "admin",
		IsAuthenticated: true,
		UserRole:        role,
		AppBuild:        app.config.Server.Build,
		AppVersion:      app.config.Server.Version,
	})
}

func (app *application) toggleMaintenanceMode(w http.ResponseWriter, r *http.Request) {
	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	err := app.savedConfig.ToggleMaintenanceMode()
	if err != nil {
		app.logger.Error("error toggling maintenance mode",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
		)

		if strings.HasPrefix(r.URL.String(), "/api") {
			a := Response{
				TraceID: traceID,
				Message: "unable to toggle maintenance mode",
				Error:   err.Error(),
			}
			a.Send(http.StatusInternalServerError, w)
			return
		}
	}

	app.publishMaintenanceModeStatus()

	if strings.HasPrefix(r.URL.String(), "/api") {
		Response{}.Send(http.StatusAccepted, w)
		return
	}

	http.Redirect(w, r, "/admin", http.StatusSeeOther)
}
