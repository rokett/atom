package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) sendJobStatusEvent(job model.JobExecution) {
	eventType := map[string]string{
		"COMPLETED":               "job.complete",
		"COMPLETED_WITH_WARNINGS": "job.complete.warnings",
		"ERROR":                   "job.error",
		"UNAUTHORISED_SCRIPT":     `job.unauthorised_script`,
		"ERROR_STARTING":          `job.error_starting`,
		"EXPIRED":                 `job.expired`,
		"CANCELLED_TIMEOUT":       `job.cancelled_timeout`,
	}

	var eventTargets []model.EventTarget

	err := app.db.Select(&eventTargets, "SELECT target FROM eventTargets WHERE event_type = '*' OR event_type = @p1", eventType[job.Status])
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return
	}
	if err != nil {
		app.logger.Error("unable to retrieve event targets",
			"error", err,
			"event_type", eventType[job.Status],
		)

		return
	}

	payload, err := json.Marshal(job)
	if err != nil {
		app.logger.Error("unable to convert event to JSON",
			"error", err,
		)
	}

	client := &http.Client{
		Timeout: time.Second * 20,
	}

	for _, target := range eventTargets {
		resp, err := client.Post(target.Target, "application/json", bytes.NewBuffer(payload))
		if err != nil {
			app.logger.Error("problem sending event payload to target",
				"error", err,
				"event_target", target.Target,
				"payload", string(payload),
			)
		}
		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			app.logger.Error("unable to read response body when sending job event",
				"error", err,
				"event_target", target.Target,
			)
		}

		var statusCode = regexp.MustCompile(`^2\d\d$`)

		switch {
		case statusCode.MatchString(strconv.Itoa(resp.StatusCode)):
			return
		default:
			app.logger.Error("target had a problem receiving event payload",
				"error", string(body),
			)
		}
	}
}
