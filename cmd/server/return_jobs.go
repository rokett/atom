package main

import (
	"net/http"
	"time"
)

func (app *application) returnJobs() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error

		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		from := time.Now().UTC().AddDate(0, 0, -1)
		if r.URL.Query().Get("from") != "" {
			from, err = time.Parse(time.RFC3339, r.URL.Query().Get("from"))
			if err != nil {
				app.logger.Error("'from' is not a valid date/time",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusBadRequest)

				APIResponse.Error = err.Error()
				APIResponse.Message = "'from' is not a valid date/time"
				APIResponse.Send(http.StatusBadRequest, w)
				return
			}
		}

		to := time.Now().UTC()
		if r.URL.Query().Get("to") != "" {
			to, err = time.Parse(time.RFC3339, r.URL.Query().Get("to"))
			if err != nil {
				app.logger.Error("'to' is not a valid date/time",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusBadRequest)

				APIResponse.Error = err.Error()
				APIResponse.Message = "'to' is not a valid date/time"
				APIResponse.Send(http.StatusBadRequest, w)
				return
			}
		}

		jobs, err := app.jobs.List(from, to, r.URL.Query().Get("fields"), map[string]string{})
		if err != nil {
			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving jobs"
			APIResponse.Send(http.StatusBadRequest, w)

			return
		}

		APIResponse.Result = jobs
		APIResponse.Send(http.StatusOK, w)
	})
}
