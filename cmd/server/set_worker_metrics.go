package main

import (
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	model "rokett.me/atom/internal"
)

func (app *application) setWorkerMetrics() {
	workers := []model.Worker{}

	err := app.db.Select(&workers, "SELECT groups FROM workers")
	if err != nil {
		app.logger.Error("error retrieving workers to set initial metrics",
			"error", err,
		)

		return
	}

	groups := make(map[string]int)

	for _, worker := range workers {
		for _, g := range strings.Split(worker.Groups, ",") {
			if g == "" {
				continue
			}

			groups[g] = groups[g] + 1
		}
	}

	workersTotal.Reset()

	for k, v := range groups {
		workersTotal.With(
			prometheus.Labels{
				"group": k,
			},
		).Set(float64(v))
	}
}
