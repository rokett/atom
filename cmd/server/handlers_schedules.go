package main

import (
	"errors"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	model "rokett.me/atom/internal"
)

func (app *application) showSchedulesPage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	filters := make(map[string]string)
	if r.URL.Query().Get("filter") != "" {
		// The schedule field has spaces so we want to ensure we pull it out of the filter string and deal with it separately.
		// If we don't, we will have a problem as we split the string on spaces for each key/value pair, and it will split on the spaces within the schedule value.
		re := regexp.MustCompile(`(?m)(schedule:".*")`)

		filter := re.ReplaceAllString(r.URL.Query().Get("filter"), "")

		parts := strings.Split(filter, " ")

		for _, part := range parts {
			if part == "" {
				continue
			}
			if !strings.Contains(part, ":") {
				app.serverError(w, errors.New("invalid filter defined"))
				return
			}

			t := strings.SplitN(part, ":", 2)
			filters[t[0]] = t[1]
		}

		// We can now deal with the schedule field, assuming it exists.
		// We'll clean up the quotes as we don't need those, and then carry on as normal to pass the key/value pair into the filter.
		scheduleFilter := re.FindString(r.URL.Query().Get("filter"))
		if scheduleFilter != "" {
			scheduleFilter = strings.Replace(scheduleFilter, `"`, "", -1)
			t := strings.SplitN(scheduleFilter, ":", 2)
			filters[t[0]] = t[1]
		}
	}

	jobSchedules, err := app.jobSchedules.List(filters)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "schedules.page.html", &templateData{
		ActivePage:      "schedules",
		Flash:           app.session.PopString(r, "flash"),
		FilterString:    r.URL.Query().Get("filter"),
		IsAuthenticated: true,
		JobSchedules:    jobSchedules,
		UserRole:        role,
	})
}

func (app *application) showScheduleUpdatePage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	jobSchedule, err := app.jobSchedules.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.Itoa(id))
	form.Set("script", jobSchedule.Script)
	form.Set("parameters", jobSchedule.Parameters)
	form.Set("executor", jobSchedule.Executor)
	form.Set("execution_username", jobSchedule.ExecutionUsername)
	form.Set("scope", jobSchedule.Scope)
	form.Set("expire_queued_job_after_duration", jobSchedule.ExpireQueuedJobAfterDuration)
	form.Set("schedule", jobSchedule.Schedule)
	form.Set("paused", strconv.FormatBool(jobSchedule.Paused))

	// Set expiry time to application level config definition if it is blank in the job schedule definition
	if jobSchedule.ExpireQueuedJobAfterDuration == "" {
		form.Set("expire_queued_job_after_duration", app.config.Server.ExpireQueuedJobAfter)
	}

	app.render(w, r, "schedule_detail.page.html", &templateData{
		Action:          "view",
		ActivePage:      "schedule_detail",
		FormData:        form,
		IsAuthenticated: true,
		UserRole:        role,
	})
}

func (app *application) showAddSchedulePage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	form := url.Values{}
	form.Set("script", r.URL.Query().Get("script"))
	form.Set("parameters", r.URL.Query().Get("parameters"))
	form.Set("executor", r.URL.Query().Get("executor"))
	form.Set("execution_username", r.URL.Query().Get("execution_username"))
	form.Set("scope", r.URL.Query().Get("scope"))
	form.Set("expire_queued_job_after_duration", r.URL.Query().Get("expire_queued_job_after_duration"))

	// Set expiry time to application level config definition if it is blank in the job schedule definition
	if r.URL.Query().Get("expire_queued_job_after_duration") == "" {
		form.Set("expire_queued_job_after_duration", app.config.Server.ExpireQueuedJobAfter)
	}

	app.render(w, r, "schedule_detail.page.html", &templateData{
		Action:          "add",
		ActivePage:      "schedule_detail",
		FormData:        form,
		IsAuthenticated: true,
		UserRole:        role,
	})
}

func (app *application) executeScheduledJob(w http.ResponseWriter, r *http.Request) {
	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	jobSchedule, err := app.getOneScheduledJob(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	// We can ignore the error here when parsing the duration as we have already tested that this works when validating the job request
	expireAfterDur, _ := time.ParseDuration(jobSchedule.ExpireQueuedJobAfterDuration)

	job := model.JobExecution{
		Executor:                      jobSchedule.Executor,
		Script:                        jobSchedule.Script,
		Parameters:                    jobSchedule.Parameters,
		Scope:                         jobSchedule.Scope,
		ExpireQueuedJobAfterTimestamp: time.Now().Add(expireAfterDur),
		ExpireQueuedJobAfterDuration:  jobSchedule.ExpireQueuedJobAfterDuration,
		ExecutionUsername:             jobSchedule.ExecutionUsername,
		Status:                        "queued",
		Log:                           "",
		ExecutionRequestedBy:          r.Context().Value(usernameCtxKey).(string),
		ClientIP:                      clientIP,
		TraceID:                       traceID,
	}

	_, executionErrors, err := app.executeJob(job)
	if err != nil {
		if len(executionErrors) == 0 {
			executionErrors = append(executionErrors, err.Error())
		}

		for _, v := range executionErrors {
			app.logger.Error("unable to execute requested job",
				"job_id", job.ID,
				"error", v,
				"executor", job.Executor,
				"script", job.Script,
				"job_parameters", job.Parameters,
				"scope", job.Scope,
				"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
				"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
			)
		}

		// Need to ensure we can pass the scheduled job details back to the page
		form := url.Values{}
		form.Set("id", chi.URLParam(r, "id"))
		form.Set("script", jobSchedule.Script)
		form.Set("parameters", jobSchedule.Parameters)
		form.Set("executor", jobSchedule.Executor)
		form.Set("execution_username", jobSchedule.ExecutionUsername)
		form.Set("scope", jobSchedule.Scope)
		form.Set("expire_queued_job_after_duration", jobSchedule.ExpireQueuedJobAfterDuration)
		form.Set("schedule", jobSchedule.Schedule)
		form.Set("paused", strconv.FormatBool(jobSchedule.Paused))

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "view",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        form,
			IsAuthenticated: true,
			UserRole:        r.Context().Value(roleCtxKey).(string),
		})

		return
	}

	http.Redirect(w, r, "/jobs", http.StatusSeeOther)
}

func (app *application) updateScheduledJob(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	if r.PostForm.Get("action") == "delete" {
		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "delete",
			ActivePage:      "schedule_detail",
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	job := model.JobSchedule{
		Script:   r.PostForm.Get("script"),
		Executor: r.PostForm.Get("executor"),
		Scope:    r.PostForm.Get("scope"),
		Schedule: r.PostForm.Get("schedule"),
	}

	id, err := strconv.ParseInt(r.PostForm.Get("id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}
	job.ID = id

	if r.PostForm.Get("parameters") != "" {
		job.Parameters = r.PostForm.Get("parameters")
	}

	if r.PostForm.Get("execution_username") != "" {
		job.ExecutionUsername = r.PostForm.Get("execution_username")
	}

	if r.PostForm.Get("expire_queued_job_after_duration") != "" {
		job.ExpireQueuedJobAfterDuration = r.PostForm.Get("expire_queued_job_after_duration")
	}

	if r.PostForm.Get("action") == "pause" {
		job.Paused = true
	}
	if r.PostForm.Get("action") == "unpause" {
		job.Paused = false
	}

	// Validating the scheduled job expects a list of allowed scripts.
	// We pull the list of scripts from the DB, add each script to a map, and pass that to the validation function.
	scripts, err := app.scripts.List(nil)
	if err != nil {
		app.serverError(w, err)
		return
	}

	// We need to carry out some validation that the job passed by the user is actually valid.
	// We can't validate the parameters, but we can validate that required fields are included and that
	// valid values have been passed for those fields which expect them.
	ve, err := job.Validate(scripts)
	if err != nil {
		validationErrors := make(map[string]string)
		for _, v := range ve {
			validationErrors[v.Parameter] = v.Error
		}

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "view",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			FormErrors:      validationErrors,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	err = app.jobSchedules.Update(job)
	if err != nil {
		app.logger.Error("error updating scheduled job",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
			"job_request", job,
			"job_id", job.ID,
		)

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "view",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	if job.Paused {
		app.removeScheduledJobFromCron(job.ID)
	} else {
		app.removeScheduledJobFromCron(job.ID)
		err := app.addScheduledJobToCron(job.ID)
		if err != nil {
			app.logger.Error("unable to update scheduled job",
				"job_id", job.ID,
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"job_request", job,
			)

			app.render(w, r, "schedule_detail.page.html", &templateData{
				Action:          "view",
				ActivePage:      "schedule_detail",
				ErrorMessage:    err.Error(),
				FormData:        r.PostForm,
				IsAuthenticated: true,
				UserRole:        role,
			})

			return
		}
	}

	http.Redirect(w, r, "/schedules", http.StatusSeeOther)
}

func (app *application) addScheduledJob(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	job := model.JobSchedule{
		Script:   r.PostForm.Get("script"),
		Executor: r.PostForm.Get("executor"),
		Scope:    r.PostForm.Get("scope"),
		Schedule: r.PostForm.Get("schedule"),
	}

	if r.PostForm.Get("parameters") != "" {
		job.Parameters = r.PostForm.Get("parameters")
	}

	if r.PostForm.Get("execution_username") != "" {
		job.ExecutionUsername = r.PostForm.Get("execution_username")
	}

	job.ExpireQueuedJobAfterDuration = app.config.Server.ExpireQueuedJobAfter
	if r.PostForm.Get("expire_queued_job_after_duration") != "" {
		job.ExpireQueuedJobAfterDuration = r.PostForm.Get("expire_queued_job_after_duration")
	}

	// Validating the scheduled job expects a list of allowed scripts.
	// We pull the list of scripts from the DB, add each script to a map, and pass that to the validation function.
	scripts, err := app.scripts.List(nil)
	if err != nil {
		app.serverError(w, err)
		return
	}

	// We need to carry out some validation that the job passed by the user is actually valid.
	// We can't validate the parameters, but we can validate that required fields are included and that
	// valid values have been passed for those fields which expect them.
	ve, err := job.Validate(scripts)
	if err != nil {
		validationErrors := make(map[string]string)
		for _, v := range ve {
			validationErrors[v.Parameter] = v.Error
		}

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "add",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			FormErrors:      validationErrors,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	err = app.db.Get(&job.ID, "INSERT INTO jobs (executor, script, parameters, scope, expire_queued_job_after_duration, execution_username, schedule) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7);SELECT id = convert(bigint, SCOPE_IDENTITY());", job.Executor, job.Script, job.Parameters, job.Scope, job.ExpireQueuedJobAfterDuration, job.ExecutionUsername, job.Schedule)
	if err != nil {
		app.logger.Error("error adding new scheduled job",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
			"job_request", job,
		)

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "add",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	err = app.addScheduledJobToCron(job.ID)
	if err != nil {
		app.logger.Error("unable to update scheduled job",
			"job_id", job.ID,
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
			"job_request", job,
		)

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "add",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	http.Redirect(w, r, "/schedules", http.StatusSeeOther)
}

func (app *application) deleteSchedule(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	id, err := strconv.ParseInt(r.PostForm.Get("id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	_, err = app.db.Exec("DELETE FROM jobs WHERE id = @p1;", id)
	if err != nil {
		app.logger.Error("error removing scheduled job",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
			"job_id", id,
		)

		app.render(w, r, "schedule_detail.page.html", &templateData{
			Action:          "view",
			ActivePage:      "schedule_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	app.removeScheduledJobFromCron(id)

	http.Redirect(w, r, "/schedules", http.StatusSeeOther)
}
