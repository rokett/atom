package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	httpRequestsTotal = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests, partitioned by path, method and status",
		},
		[]string{"path", "method", "status"},
	)

	httpRequestDurationSeconds = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "http_request_duration_seconds",
			Help: "Duration of HTTP requests, partitioned by path and method",
		},
		[]string{"path", "method"},
	)

	httpErrorsTotal = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_errors_total",
			Help: "Total number of HTTP errors, partitioned by status",
		},
		[]string{"status"},
	)

	jobRunsTotal = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name:      "job_runs_total",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Total number of job runs, by type, script and status.",
		},
		[]string{"type", "job", "status"},
	)

	jobsQueued = promauto.NewGauge(
		prometheus.GaugeOpts{
			Name:      "jobs_queued",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Total number of jobs currently queued.",
		},
	)

	jobsRunning = promauto.NewGauge(
		prometheus.GaugeOpts{
			Name:      "jobs_running",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Total number of job currently running.",
		},
	)

	jobDurationSeconds = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:      "job_duration_seconds",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Duration of jobs, partitioned by the script name",
			Buckets:   []float64{1, 5, 10, 30, 60, 120, 240, 480},
		},
		[]string{"job"},
	)

	workersTotal = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name:      "workers_total",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Total number of workers, by group",
		},
		[]string{"group"},
	)

	workerStatus = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name:      "worker_status",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Is worker idle (0) or busy (1) or in maintenance mode (2)",
		},
		[]string{"worker_name"},
	)

	maintenanceMode = promauto.NewGauge(
		prometheus.GaugeOpts{
			Name:      "maintenance_mode",
			Namespace: "atom",
			Subsystem: "server",
			Help:      "Is maintenance mode enabled (1) or disabled (0)",
		},
	)
)
