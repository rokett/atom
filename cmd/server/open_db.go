package main

import (
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/microsoft/go-mssqldb"
)

func openDB(dbHost string, dbPort int, dbName string, username string, password string, logger *slog.Logger) (db *sqlx.DB) {
	var dbError error

	maxAttempts := 10

	dsn := fmt.Sprintf("server=%s;port=%d;database=%s;encrypt=disable;connection timeout=30", dbHost, dbPort, dbName)

	if username != "" && password != "" {
		dsn = fmt.Sprintf("%s;user id=%s;password=%s", dsn, username, password)
	}

	for attempts := 1; attempts <= maxAttempts; attempts++ {
		db, dbError = sqlx.Connect("sqlserver", dsn)
		if dbError != nil {
			logger.Info(
				"unable to connect to database; retrying.",
				"error", dbError,
			)

			time.Sleep(time.Duration(attempts) * time.Second)
			continue
		}

		break
	}

	if dbError != nil {
		logger.Error(
			"run out of attempts to connect to database",
			"error", dbError,
			"max_attempts", maxAttempts,
		)
		os.Exit(1)
	}

	return db
}
