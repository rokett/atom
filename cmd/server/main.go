package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	model "rokett.me/atom/internal"

	"github.com/golangcollege/sessions"
	_ "github.com/microsoft/go-mssqldb"
	"github.com/nats-io/nats.go"
)

type contextKeyType string

const clientIPCtxKey contextKeyType = "client_ip"
const traceIDCtxKey contextKeyType = "trace_id"
const roleCtxKey contextKeyType = "role"
const usernameCtxKey contextKeyType = "username"

var (
	appName = "Atom"
	version string
	build   string

	versionFlg                = flag.Bool("version", false, "Display application version")
	envFlg                    = flag.String("application.environment", "dev", "Environment the application is running in")
	natsHostFlg               = flag.String("nats.host", "127.0.0.1", "IP address/FQDN of NATS server to connect to")
	natsServerPortFlg         = flag.Int("nats.server_port", 4222, "Port that NATS server is listening for clients on")
	natsStreamReplicasFlg     = flag.Int("nats.stream_replicas", 1, "Number of replicas for NATS streams; either 1, 3 or 5")
	natsNKeySeedFlg           = flag.String("nats.nkey_seed", "", "The NKey seed for the Atom user")
	apiPortFlg                = flag.Int("api.port", 9999, "Port to listen for API requests on")
	corsAllowedOriginsFlg     = flag.String("cors.allowed_origins", "", "Allowed origins for CORS purposes")
	corsAllowedHeadersFlg     = flag.String("cors.allowed_headers", "*", "Allowed headers for CORS purposes")
	dbHostFlg                 = flag.String("db.host", "127.0.0.1", "Name of database host, including instance name if needed")
	dbPortFlg                 = flag.Int("db.port", 1433, "Port to connect to database server on")
	dbName                    = flag.String("db.name", "atom", "Name of the database to use")
	dbUsernameFlg             = flag.String("db.username", "", "Username of account with read/write access to database")
	dbPasswordFlg             = flag.String("db.password", "", "Password for database user")
	authHosts                 = flag.String("auth.hosts", "", "Comma separated string of LDAP hosts for authentication purposes")
	authBaseDN                = flag.String("auth.base_dn", "", "Base LDAP DN to search for user to be authenticated")
	authBindDN                = flag.String("auth.bind_dn", "", "Distinguished name of user account to use to search the LDAP directory")
	authBindPW                = flag.String("auth.bind_pw", "", "Password for the user used to search the LDAP directory")
	authPort                  = flag.Int("auth.port", 389, "Defaults to 389, or 636 if SSL is used")
	authUseSSL                = flag.Bool("auth.use_ssl", false, "Use a secure connection")
	authStartTLS              = flag.Bool("auth.start_tls", false, "Init secure connection using StartTLS")
	authSSLSkipVerify         = flag.Bool("auth.ssl_skip_verify", false, "Skip verification of certificate when using a secure connection")
	disableStaleWorkerCleanup = flag.Bool("disable_stale_worker_cleanup", false, "Disable the scheduled job to remove stale workers")
	session_secret            = flag.String("session_secret", "", "Secret key for session cookies; 32 bytes long")
	helpFlg                   = flag.Bool("help", false, "Display application help")
	expireQueuedJobAfterFlg   = flag.String("expire_queued_job_after", "720h", "Set a default length of time after which a job should not be processed; for example 1h for 1 hour.  Valid units are 's', 'm' or 'h'.")
)

func main() {
	flag.Parse()

	if *versionFlg {
		fmt.Printf("%s v%s build %s\n", appName, version, build)
		os.Exit(0)
	}

	if *helpFlg || *dbUsernameFlg == "" || *dbPasswordFlg == "" || *authHosts == "" || *authBaseDN == "" || *authBindDN == "" || *authBindPW == "" {
		flag.PrintDefaults()
		os.Exit(0)
	}

	logger := setupLogger(appName, version, build, *envFlg)

	_, err := time.ParseDuration(*expireQueuedJobAfterFlg)
	if err != nil {
		logger.Error(
			"parsing 'expire_queued_job_after' flag",
			"error", err,
			"flag", *expireQueuedJobAfterFlg,
		)
		os.Exit(1)
	}

	config, err := parseConfig(
		*natsHostFlg,
		*natsServerPortFlg,
		*natsStreamReplicasFlg,
		*natsNKeySeedFlg,
		*apiPortFlg,
		*corsAllowedOriginsFlg,
		*corsAllowedHeadersFlg,
		*dbHostFlg,
		*dbPortFlg,
		*dbName,
		*dbUsernameFlg,
		*dbPasswordFlg,
		*authHosts,
		*authBaseDN,
		*authBindDN,
		*authBindPW,
		*authPort,
		*authUseSSL,
		*authStartTLS,
		*authSSLSkipVerify,
		*disableStaleWorkerCleanup,
		*expireQueuedJobAfterFlg,
		version,
		build,
	)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	db := openDB(config.Database.Host, config.Database.Port, config.Database.Name, config.Database.Username, config.Database.Password, logger)
	defer db.Close()

	runMigrations(logger, db)

	// Servers send messages to workers, and multiple servers communicate with each other, via NATS.
	// NATS should be running as an external service.
	// Let's setup the client so that messages can be published and received.
	nc, js := setupNATSClient(config.Server.NatsConnString, config.Server.NatsStreamReplicas, config.Server.NatsNKeySeed, logger)
	defer nc.Close()

	templateCache, err := newTemplateCache()
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	session := sessions.New([]byte(*session_secret))
	session.Lifetime = 12 * time.Hour

	app := &application{
		config: config,
		db:     db,
		eventTargets: &model.EventTargetModel{
			DB: db,
		},
		jobs: &model.JobExecutionModel{
			DB: db,
		},
		jobSchedules: &model.JobScheduleModel{
			DB: db,
		},
		js:              js,
		logger:          logger,
		maintenanceMode: false,
		nc:              nc,
		savedConfig: &model.ConfigModel{
			DB: db,
		},
		session: session,
		scripts: &model.ScriptModel{
			DB: db,
		},
		templateCache: templateCache,
		workers: &model.WorkerModel{
			DB: db,
		},
	}

	// Some jobs are scheduled, and we store the schedules in the database.
	// We need to retrieve the scheduled jobs and set the application up to run them at the desired time.
	c := app.setupCron()
	app.cron = c

	app.setWorkerMetrics()

	// Only approved scripts are allowed to be run.  At startup we retrieve the list of allowed scripts from the DB, publish them to listening workers, and store them in the server config struct.
	// When a worker starts up it asks the server for an updated list.
	// The allowed scripts can be updated on the fly via the API, at which point the config struct is updated and the up-to-date list published to listening workers.
	app.config.Server.AllowedScripts = app.initialiseAllowedScripts()

	enabled, err := app.savedConfig.IsMaintenanceModeEnabled()
	if err != nil {
		app.logger.Error(
			"unable to determine if maintenance mode is enabled",
			"error", err,
		)
	}
	if enabled {
		app.maintenanceMode = true
		maintenanceMode.Set(1)
	} else {
		app.maintenanceMode = false
		maintenanceMode.Set(0)
	}

	// Now that most of the server is running we can setup NATS subscriptions to receive messages from workers.
	subs := app.setupSubscriptions()

	// Time to setup the API to allow users to connect
	apiServer := app.runAPIServer()
	defer apiServer.Close()

	app.setupCloseHandler(subs, apiServer)

	// The waitgroup is used to block forever.  It will wait until the waitgroup equals 0, which it will never do.
	// The programme will therefore only end when an interrupt signal is sent.
	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Wait()
}

// The close hander watches for an interrupt signal (ctrl+c) and then gracefully stops the programme.
func (app *application) setupCloseHandler(subscriptions []*nats.Subscription, apiServer *http.Server) {
	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-c
		app.logger.Info("Exiting server")

		apiServer.Close()

		// Stop listening for new messages from NATS
		for _, sub := range subscriptions {
			err := sub.Unsubscribe()
			if err != nil {
				app.logger.Error(
					"unable to unsubscribe",
					"error", err,
					"nats_subject", sub.Subject,
				)

				continue
			}

			app.logger.Info(
				"unsubscribed",
				"nats_subject", sub.Subject,
			)
		}

		app.nc.Close()

		app.db.Close()

		os.Exit(0)
	}()
}
