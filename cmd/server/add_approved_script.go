package main

import (
	"encoding/json"
	"net/http"
	"strings"
)

func (app *application) addApprovedScript() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		var scripts []string
		err := json.NewDecoder(r.Body).Decode(&scripts)
		if err != nil {
			app.logger.Error("error decoding JSON request",
				"client_ip", clientIP,
				"trace_id", traceID,
				"error", err,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error decoding JSON request"
			APIResponse.Send(http.StatusBadRequest, w)

			return
		}

		if len(scripts) > 1 {
			app.logger.Error("too many scripts in request to add a new one to the approved list",
				"client_ip", clientIP,
				"trace_id", traceID,
				"script_name", strings.Join(scripts, ","),
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = "too many scripts in request to add a new one to the approved list"
			APIResponse.Message = "too many scripts in request to add a new one to the approved list"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		uniqueScript, _, err := app.isUniqueScript(scripts[0])
		if err != nil {
			app.logger.Error("error checking that script is unique",
				"client_ip", clientIP,
				"trace_id", traceID,
				"script_name", scripts[0],
				"error", err,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error checking that script is unique"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		if !uniqueScript {
			APIResponse.Send(http.StatusNoContent, w)
			return
		}

		_, err = app.db.Exec("INSERT INTO scripts (script) VALUES (@p1);", scripts[0])
		if err != nil {
			app.logger.Error("error adding new approved scripts",
				"client_ip", clientIP,
				"trace_id", traceID,
				"script_name", scripts[0],
				"error", err,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error adding new approved scripts"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		APIResponse.Send(http.StatusCreated, w)

		app.updateApprovedScriptsConfig()
	})
}
