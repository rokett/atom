package main

func (app *application) ackJobLogMessages(id int64) {
	// Now we need to delete the buffered log entries from NATS because the log has been written to the DB
	msgs, err := app.fetchJobLogFromNATS(id)
	if err != nil {
		app.logger.Error("fetching job log from NATS",
			"error", err,
		)
	}

	for _, msg := range msgs {
		err = msg.Ack()
		if err != nil {
			app.logger.Error("acking message",
				"error", err,
			)
		}
	}
}
