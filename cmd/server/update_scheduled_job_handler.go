package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	model "rokett.me/atom/internal"

	"github.com/go-chi/chi/v5"
)

func (app *application) updateScheduledJobHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			app.logger.Error("error converting id from string to int64",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "unable to parse id of scheduled job to be deleted"

			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		var job model.JobSchedule

		err = json.NewDecoder(r.Body).Decode(&job)
		if err != nil {
			app.logger.Error("error decoding JSON request to create a new scheduled job",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error decoding JSON request to create a new scheduled job"
			APIResponse.Send(http.StatusBadRequest, w)

			return
		}

		job.ID = id

		if job.ExpireQueuedJobAfterDuration == "" {
			job.ExpireQueuedJobAfterDuration = app.config.Server.ExpireQueuedJobAfter
		}

		// We need to carry out some validation that the job passed by the user is actually valid.
		// We can't validate the parameters, but we can validate that required fields are included and that
		// valid values have been passed for those fields which expect them.
		ve, err := job.Validate(app.config.Server.AllowedScripts)
		if err != nil {
			json, err := json.Marshal(ve)
			if err != nil {
				app.logger.Error("unable to encode errors from validation process",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusInternalServerError)

				APIResponse.Message = "unable to encode errors from validation process"
				APIResponse.Error = err.Error()
				APIResponse.Send(http.StatusInternalServerError, w)

				return
			}

			app.logger.Error("error(s) when validating request to schedule job",
				"validation_errors", string(json),
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(json)

			return
		}

		err = app.jobSchedules.Update(job)
		if err != nil {
			app.logger.Error("error updating scheduled job",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"job_request", job,
				"job_id", job.ID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error updating scheduled job"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		if job.Paused {
			app.removeScheduledJobFromCron(id)
		} else {
			app.removeScheduledJobFromCron(id)
			err := app.addScheduledJobToCron(id)
			if err != nil {
				app.logger.Error("unable to update scheduled job",
					"job_id", id,
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
					"job_request", job,
				)

				logErrorMetrics(http.StatusInternalServerError)

				APIResponse.Error = err.Error()
				APIResponse.Message = "unable to update scheduled job"
				APIResponse.Send(http.StatusInternalServerError, w)
				return
			}
		}

		APIResponse.Result = job
		APIResponse.Send(http.StatusOK, w)
	})
}
