package main

import (
	"net/http"

	model "rokett.me/atom/internal"
)

func (app *application) returnAllEventTargetsHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		var eventTargets []model.EventTarget

		err := app.db.Select(&eventTargets, "SELECT id, name, target, event_type, created_at, updated_at FROM eventTargets")
		if err != nil {
			app.logger.Error("error retrieving event targets",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving event targets"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		APIResponse.Result = eventTargets
		APIResponse.Send(http.StatusOK, w)
	})
}
