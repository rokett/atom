package main

import "net/http"

func authOk() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var APIResponse Response

		APIResponse.Result = r.Context().Value(roleCtxKey).(string)

		APIResponse.Send(http.StatusOK, w)
	})
}
