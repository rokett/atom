package main

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

func (app *application) returnOneJob() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			app.logger.Error("error converting id from string to int64",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error converting id from string to int64"

			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		job, err := app.jobs.Get(int(id))
		if err != nil {
			app.logger.Error("error retrieving job",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"job_id", id,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving job"

			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		APIResponse.Result = job
		APIResponse.Send(http.StatusOK, w)
	})
}
