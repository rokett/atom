package main

import (
	"io/fs"
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"rokett.me/atom"

	"github.com/go-chi/cors"
)

func (app *application) setupRouter() http.Handler {
	r := chi.NewRouter()

	if len(app.config.Server.CorsAllowedOrigins) > 0 {
		r.Use(cors.Handler(cors.Options{
			AllowedOrigins: app.config.Server.CorsAllowedOrigins,
			AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
			AllowedHeaders: app.config.Server.CorsAllowedHeaders,
		}))
	}

	r.Method(http.MethodGet, "/metrics", promhttp.Handler())

	// status route is used for healthchecking and is excluded from logging
	r.Get("/status", func(w http.ResponseWriter, r *http.Request) {
		APIResponse := Response{
			Message: "ok",
		}

		APIResponse.Send(http.StatusOK, w)
	})

	r.Group(func(r chi.Router) {
		r.Use(logMetrics)
		r.Use(getClientIP)
		r.Use(setTraceID)
		r.Use(cacheControl)
		r.Use(app.session.Enable)
		r.Use(middleware.Compress(5))

		r.Post("/api/authenticate", app.authenticateUser())

		r.Get("/login", app.showLoginPage)
		r.Post("/login", app.loginUser)

		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			http.Redirect(w, r, "/jobs", http.StatusSeeOther)
		})
	})

	r.Group(func(r chi.Router) {
		r.Use(logMetrics)
		r.Use(getClientIP)
		r.Use(setTraceID)
		r.Use(cacheControl)
		r.Use(app.validateAuth("user,admin"))
		r.Use(middleware.Compress(5))

		r.Get("/api/authentication/validate", authOk())
		r.Get("/api/jobs", app.returnJobs())
		r.Get("/api/jobs/schedule", app.returnAllScheduledJobsHandler())
		r.Get("/api/jobs/{id}", app.returnOneJob())
		r.Post("/api/jobs/execute", app.executeJobHandler())
		r.Post("/api/jobs/execute/{id}", app.executeJobAgainHandler())
		r.Get("/api/workers", app.returnAllWorkers())
		r.Get("/api/config/approved_scripts", app.returnApprovedScripts())
	})

	r.Group(func(r chi.Router) {
		r.Use(logMetrics)
		r.Use(getClientIP)
		r.Use(setTraceID)
		r.Use(cacheControl)
		r.Use(app.validateAuth("admin"))
		r.Use(middleware.Compress(5))

		r.Post("/api/jobs/schedule", app.addScheduledJobHandler())
		r.Put("/api/jobs/schedule/{id}", app.updateScheduledJobHandler())
		r.Delete("/api/jobs/schedule/{id}", app.removeScheduledJobHandler())
		r.Post("/api/config/approved_scripts", app.addApprovedScript())
		r.Put("/api/config/approved_scripts", app.updateApprovedScripts())
		r.Post("/api/config/application_users", app.createApplicationUser())
		r.Post("/api/config/event_targets", app.addEventTargetHandler())
		r.Get("/api/config/event_targets", app.returnAllEventTargetsHandler())
		r.Delete("/api/config/event_targets/{id}", app.removeEventTargetHandler())
		r.Post("/api/config/maintenance_mode", app.toggleMaintenanceMode)
	})

	r.Group(func(r chi.Router) {
		r.Use(logMetrics)
		r.Use(getClientIP)
		r.Use(setTraceID)
		r.Use(cacheControl)
		r.Use(app.validateInteractiveAuth("user,admin"))
		r.Use(app.session.Enable)
		r.Use(middleware.Compress(5))

		r.Get("/api", app.showApiDocs)

		r.Get("/jobs", app.showJobsPage)
		r.Get("/jobs/{id}", app.showJobDetailPage)
		r.Post("/jobs/{id}", app.showJobDetailPageAction)
		r.Post("/jobs/{id}/execute", app.executeJobById)
		r.Post("/jobs/{id}/cancel", app.cancelJobById)

		r.Get("/schedules", app.showSchedulesPage)
		r.Get("/schedules/{id}", app.showScheduleUpdatePage)
		r.Post("/schedules/{id}/delete", app.deleteSchedule)
		r.Get("/schedules/add", app.showAddSchedulePage)
		r.Post("/schedules/{id}", app.updateScheduledJob)
		r.Post("/schedules/{id}/execute", app.executeScheduledJob)
		r.Post("/schedules/add", app.addScheduledJob)

		r.Get("/scripts", app.showScriptsPage)
		r.Get("/scripts/add", app.showAddScriptPage)
		r.Post("/scripts/add", app.addScript)
		r.Get("/scripts/{id}/execute", app.showScriptExecutePage)
		r.Post("/scripts/{id}/execute", app.executeScript)
		r.Post("/scripts/search", app.searchScripts)

		r.Get("/workers", app.showWorkersPage)
	})

	r.Group(func(r chi.Router) {
		r.Use(logMetrics)
		r.Use(getClientIP)
		r.Use(setTraceID)
		r.Use(cacheControl)
		r.Use(app.validateInteractiveAuth("admin"))
		r.Use(app.session.Enable)
		r.Use(middleware.Compress(5))

		r.Get("/admin", app.showAdminPage)

		r.Get("/admin/event_targets", app.showEventTargets)
		r.Get("/admin/event_target/{id}", app.showEventTargetUpdatePage)
		r.Post("/admin/event_target/{id}/delete", app.deleteEventTarget)
		r.Get("/admin/event_target/add", app.showAddEventTargetPage)
		r.Post("/admin/event_target/{id}", app.updateEventTarget)
		r.Post("/admin/event_target/add", app.addEventTarget)

		r.Post("/admin/maintenance_mode", app.toggleMaintenanceMode)
	})

	var static, err = fs.Sub(atom.UI, "ui/dist/static")
	if err != nil {
		app.logger.Error("returning embedded assets",
			"error", err,
		)
	}

	r.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.FS(static))))

	return r
}
