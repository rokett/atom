package main

import (
	"html/template"
	"log/slog"

	"github.com/golangcollege/sessions"
	"github.com/jmoiron/sqlx"
	"github.com/nats-io/nats.go"
	"github.com/robfig/cron/v3"
	model "rokett.me/atom/internal"
)

type application struct {
	config          config
	cron            *cron.Cron
	db              *sqlx.DB
	eventTargets    *model.EventTargetModel
	jobs            *model.JobExecutionModel
	jobSchedules    *model.JobScheduleModel
	js              nats.JetStreamContext
	logger          *slog.Logger
	maintenanceMode bool
	nc              *nats.Conn
	savedConfig     *model.ConfigModel
	scripts         *model.ScriptModel
	session         *sessions.Session
	templateCache   map[string]*template.Template
	workers         *model.WorkerModel
}
