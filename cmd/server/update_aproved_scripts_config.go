package main

func (app *application) updateApprovedScriptsConfig() {
	app.config.Server.AllowedScripts = app.getAllowedScripts()

	app.publishAllowedScripts(app.config.Server.AllowedScripts)
}
