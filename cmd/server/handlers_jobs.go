package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	model "rokett.me/atom/internal"
)

func (app *application) showJobsPage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	from := time.Now().UTC().AddDate(0, 0, -1)
	to := time.Now().UTC()
	jobsDatePickerString := "Last 24 hours"

	filters := make(map[string]string)
	if r.URL.Query().Get("filter") != "" {
		parts := strings.Split(r.URL.Query().Get("filter"), " ")

		for _, part := range parts {
			if !strings.Contains(part, ":") {
				app.serverError(w, errors.New("invalid filter defined"))
				return
			}

			t := strings.SplitN(part, ":", 2)
			filters[t[0]] = t[1]
		}

		// The UI names some fields differently than the database, so we need to ensure we set the filter keys correctly or the SQL query will bomb
		if _, ok := filters["job"]; ok {
			filters["script"] = filters["job"]
			delete(filters, "job")
		}
		if _, ok := filters["worker"]; ok {
			filters["worker_name"] = filters["worker"]
			delete(filters, "worker")
		}

		switch filters["relative"] {
		case "last_2_days":
			year, month, day := time.Now().UTC().AddDate(0, 0, -2).Date()
			from = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

			year, month, day = time.Now().UTC().Date()
			to = time.Date(year, month, day, 23, 59, 59, 0, time.UTC)

			jobsDatePickerString = "Last 2 days"
		case "last_7_days":
			year, month, day := time.Now().UTC().AddDate(0, 0, -7).Date()
			from = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

			year, month, day = time.Now().UTC().Date()
			to = time.Date(year, month, day, 23, 59, 59, 0, time.UTC)

			jobsDatePickerString = "Last 7 days"
		case "yesterday":
			year, month, day := time.Now().UTC().AddDate(0, 0, -1).Date()
			from = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
			to = time.Date(year, month, day, 23, 59, 59, 0, time.UTC)

			jobsDatePickerString = "Yesterday"
		case "day_before_yesterday":
			year, month, day := time.Now().UTC().AddDate(0, 0, -2).Date()
			from = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
			to = time.Date(year, month, day, 23, 59, 59, 0, time.UTC)

			jobsDatePickerString = "Day before yesterday"
		case "today":
			year, month, day := time.Now().UTC().Date()
			from = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
			to = time.Date(year, month, day, 23, 59, 59, 0, time.UTC)

			jobsDatePickerString = "Today"
		case "this_week":
			year, week := time.Now().UTC().ISOWeek()
			from, to = app.weekRange(year, week)

			jobsDatePickerString = "This week"
		}

		delete(filters, "relative")

		fromFilter := false
		if val, ok := filters["from"]; ok {
			delete(filters, "from")

			if val != "" {
				fromFilter = true

				layouts := []string{
					"2006-01-02",
					"2006-01-02T15:04",
				}

				var parseSuccess bool
				var err error
				for _, layout := range layouts {
					from, err = time.Parse(layout, val)
					if err != nil {
						continue
					}

					parseSuccess = true
					break
				}

				if !parseSuccess {
					app.serverError(w, err)
					return
				}
			}
		}

		toFilter := false
		if val, ok := filters["to"]; ok {
			delete(filters, "to")

			if val != "" {
				toFilter = true

				layouts := []string{
					"2006-01-02",
					"2006-01-02T15:04",
				}

				var parseSuccess bool
				var err error
				for _, layout := range layouts {
					to, err = time.Parse(layout, val)
					if err != nil {
						continue
					}

					parseSuccess = true
					break
				}

				if !parseSuccess {
					app.serverError(w, err)
					return
				}
			}
		}

		if fromFilter && toFilter {
			jobsDatePickerString = fmt.Sprintf("%s to %s", from.Format("02 January 2006 @ 15:04"), to.Format("02 January 2006 @ 15:04"))
		}
	}

	jobs, err := app.jobs.List(from, to, r.URL.Query().Get("fields"), filters)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "jobs.page.html", &templateData{
		ActivePage:           "jobs",
		Flash:                app.session.PopString(r, "flash"),
		FilterString:         r.URL.Query().Get("filter"),
		IsAuthenticated:      true,
		Jobs:                 jobs,
		JobsDatepickerString: jobsDatePickerString,
		UserRole:             role,
	})
}

func (app *application) showJobDetailPage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	job, err := app.jobs.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	// If the job has completed the log as stored in the DB is used.
	// If it is still running, the log is pulled from NATS where it is buffered.
	completionStatus := map[string]bool{
		"EXPIRED":                 true,
		"ERROR":                   true,
		"ERROR_STARTING":          true,
		"COMPLETED":               true,
		"COMPLETED_WITH_WARNINGS": true,
		"UNAUTHORISED_SCRIPT":     true,
		"CANCELLED":               true,
	}

	if !completionStatus[job.Status] {
		msgs, err := app.fetchJobLogFromNATS(int64(id))
		if err != nil {
			app.serverError(w, err)
		}

		// We need to create the log now from the stored messages, after which we can unsubscribe from the subject.
		log := []string{}
		for _, msg := range msgs {
			log = append(log, string(msg.Data))
		}

		job.Log = strings.Join(log, "\n")
	}

	script, err := app.scripts.GetByName(job.Script)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "job_detail.page.html", &templateData{
		Action:          "view",
		ActivePage:      "job_detail",
		IsAuthenticated: true,
		Job:             job,
		Script:          script,
		UserRole:        role,
	})
}

func (app *application) showJobDetailPageAction(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	job, err := app.jobs.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	// If the job has completed the log as stored in the DB is used.
	// If it is still running, the log is pulled from NATS where it is buffered.
	completionStatus := map[string]bool{
		"EXPIRED":                 true,
		"ERROR":                   true,
		"ERROR_STARTING":          true,
		"COMPLETED":               true,
		"COMPLETED_WITH_WARNINGS": true,
		"UNAUTHORISED_SCRIPT":     true,
		"CANCELLED":               true,
	}

	if !completionStatus[job.Status] {
		msgs, err := app.fetchJobLogFromNATS(int64(id))
		if err != nil {
			app.serverError(w, err)
		}

		// We need to create the log now from the stored messages, after which we can unsubscribe from the subject.
		log := []string{}
		for _, msg := range msgs {
			log = append(log, string(msg.Data))
		}

		job.Log = strings.Join(log, "\n")
	}

	script, err := app.scripts.GetByName(job.Script)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "job_detail.page.html", &templateData{
		Action:          "delete",
		ActivePage:      "job_detail",
		IsAuthenticated: true,
		Job:             job,
		Script:          script,
		UserRole:        role,
	})
}

func (app *application) cancelJobById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	job, err := app.jobs.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	switch job.Status {
	case "queued":
		err = app.js.DeleteMsg("JOBS", job.SequenceNumber)
		if err != nil {
			app.serverError(w, err)
			return
		}

		app.session.Put(r, "flash", "Cancelled job")
	case "ACCEPTED", "STARTING", "RUNNING", "RECEIVED_BY_WORKER":
		subject := fmt.Sprintf("job.cancel:%s", job.WorkerName)

		// Need to convert the job ID to a byte array for passing in the request published to NATS
		jobID := make([]byte, 8)
		binary.BigEndian.PutUint64(jobID, uint64(job.ID))

		msg, err := app.nc.Request(subject, jobID, 10*time.Second)
		if err != nil {
			app.logger.Error(
				"publishing request to cancel job",
				"error", err,
				"job_id", job.ID,
			)
			return
		}

		switch string(msg.Data) {
		case "true":
			err = app.jobs.SetStatus(job, "CANCELLED")
			if err != nil {
				app.serverError(w, err)
			}

			app.session.Put(r, "flash", "Cancelled job")
		case "false":
			app.logger.Error("cancelling job failed")
			app.session.Put(r, "flash", "Problem cancelling job")
		}
	}

	http.Redirect(w, r, "/jobs", http.StatusSeeOther)
}

func (app *application) executeJobById(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	job, err := app.jobs.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	job.Status = "queued"
	job.Log = ""
	job.ExecutionRequestedBy = r.Context().Value(usernameCtxKey).(string)
	job.ClientIP = clientIP
	job.TraceID = traceID

	// We need to recalculate the job expiry timestamp based on the original expiry duration
	expireAfterDur, _ := time.ParseDuration(job.ExpireQueuedJobAfterDuration)
	job.ExpireQueuedJobAfterTimestamp = time.Now().Add(expireAfterDur)

	_, executionErrors, err := app.executeJob(job)
	if err != nil {
		if len(executionErrors) == 0 {
			executionErrors = append(executionErrors, err.Error())
		}

		for _, v := range executionErrors {
			app.logger.Error("unable to execute requested job",
				"job_id", job.ID,
				"error", v,
				"executor", job.Executor,
				"script", job.Script,
				"job_parameters", job.Parameters,
				"scope", job.Scope,
				"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
				"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
			)
		}

		app.render(w, r, "job_detail.page.html", &templateData{
			ActivePage:      "job_detail",
			ErrorMessage:    err.Error(),
			IsAuthenticated: true,
			Job:             job,
			UserRole:        role,
		})

		return
	}

	http.Redirect(w, r, "/jobs", http.StatusSeeOther)
}

func (app *application) executeJob(job model.JobExecution) ([]model.JobExecution, []string, error) {
	var jobExecutions []model.JobExecution

	scopeGroupAny := regexp.MustCompile(`(?i)^.*:@any$`)
	scopeAll := regexp.MustCompile(`(?i)^@all$`)
	scopeGroupAll := regexp.MustCompile(`(?i)^(.*):@all$`)

	switch {
	case scopeGroupAny.MatchString(job.Scope):
		job, err := app.queueJob(job)
		if err != nil {
			return jobExecutions, []string{}, fmt.Errorf("queuing job: %w", err)
		}

		err = app.publishJob(job)
		if err != nil {
			return jobExecutions, []string{}, fmt.Errorf("executing job: %w", err)
		}

		jobExecutions = append(jobExecutions, job)
	case scopeAll.MatchString(job.Scope):
		workers := []model.Worker{}

		err := app.db.Select(&workers, "SELECT name FROM workers")
		if err != nil {
			return jobExecutions, []string{}, fmt.Errorf("retrieving workers: %w", err)
		}

		if len(workers) == 0 {
			return jobExecutions, []string{}, fmt.Errorf("no matching workers available")
		}

		var executionErrors []string

		for _, worker := range workers {
			job.WorkerName = worker.Name

			job, err = app.queueJob(job)
			if err != nil {
				executionErrors = append(executionErrors, err.Error())
				continue
			}

			err = app.publishJob(job)
			if err != nil {
				app.logger.Error("unable to execute requested job",
					"job_id", job.ID,
					"error", err,
					"executor", job.Executor,
					"script", job.Script,
					"job_parameters", job.Parameters,
					"scope", job.Scope,
					"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
					"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
				)

				executionErrors = append(executionErrors, err.Error())
				continue
			}

			jobExecutions = append(jobExecutions, job)
		}

		if len(executionErrors) > 0 {
			return jobExecutions, executionErrors, errors.New("execution errors")
		}
	case scopeGroupAll.MatchString(job.Scope):
		workers := []model.Worker{}

		matches := scopeGroupAll.FindStringSubmatch(job.Scope)

		param := fmt.Sprintf(`%%%s%%`, matches[1])
		err := app.db.Select(&workers, "SELECT name FROM workers WHERE groups LIKE @p1", param)
		if err != nil {
			return jobExecutions, []string{}, fmt.Errorf("retrieving workers: %w", err)
		}

		if len(workers) == 0 {
			return jobExecutions, []string{}, fmt.Errorf("no workers available")
		}

		var executionErrors []string

		for _, worker := range workers {
			job.WorkerName = worker.Name

			job, err = app.queueJob(job)
			if err != nil {
				executionErrors = append(executionErrors, err.Error())
				continue
			}

			err = app.publishJob(job)
			if err != nil {
				app.logger.Error("unable to execute requested job",
					"job_id", job.ID,
					"error", err,
					"executor", job.Executor,
					"script", job.Script,
					"job_parameters", job.Parameters,
					"scope", job.Scope,
					"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
					"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
				)

				executionErrors = append(executionErrors, err.Error())
				continue
			}

			jobExecutions = append(jobExecutions, job)
		}

		if len(executionErrors) > 0 {
			return jobExecutions, executionErrors, errors.New("execution errors")
		}
	default:
		var executionErrors []string

		hostnames := strings.Split(job.Scope, ",")
		for _, hostname := range hostnames {
			hostname = strings.TrimSpace(hostname)

			workers := []model.Worker{}

			//TODO - ENHANCEMENT - Figure out how to find idle workers first, and then any worker if there isn't an idle one
			err := app.db.Select(&workers, "SELECT name FROM workers WHERE server_name = @p1", hostname)
			if err != nil {
				return jobExecutions, []string{}, fmt.Errorf("retrieving workers by hostname: %w", err)
			}

			if len(workers) == 0 {
				return jobExecutions, []string{}, fmt.Errorf("no matching workers available")
			}

			seed := time.Now().UTC().UnixNano()

			src := rand.NewSource(seed)
			r := rand.New(src)

			worker := workers[r.Intn(len(workers))]
			job.WorkerName = worker.Name

			job, err = app.queueJob(job)
			if err != nil {
				executionErrors = append(executionErrors, err.Error())
				continue
			}

			err = app.publishJob(job)
			if err != nil {
				app.logger.Error("unable to execute requested job",
					"job_id", job.ID,
					"error", err,
					"executor", job.Executor,
					"script", job.Script,
					"job_parameters", job.Parameters,
					"scope", job.Scope,
					"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
					"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
				)

				executionErrors = append(executionErrors, err.Error())
				continue
			}

			jobExecutions = append(jobExecutions, job)
		}

		if len(executionErrors) > 0 {
			return jobExecutions, executionErrors, errors.New("execution errors")
		}
	}

	return jobExecutions, []string{}, nil
}
