package main

import (
	"fmt"
	"time"
)

// Remove workers which have not sent a heartbeat in the last 6 minutes
func (app *application) cleanupStaleWorkers() {
	olderThan := time.Now().UTC().Add(-time.Minute * 6)

	_, err := app.db.Exec("DELETE FROM workers WHERE heartbeat_timestamp < @p1", olderThan)
	if err != nil {
		app.logger.Error("error deleting stale workers",
			"query", fmt.Sprintf("DELETE FROM workers WHERE heartbeat_timestamp = '%s'", olderThan),
			"error", err,
		)
	}

	go app.setWorkerMetrics()
}
