package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"log/slog"
	"os"
	"sort"
	"strings"

	"rokett.me/atom"

	"github.com/jmoiron/sqlx"
	mssql "github.com/microsoft/go-mssqldb"
)

type migration struct {
	Filename string
	Hash     string
}

func runMigrations(logger *slog.Logger, db *sqlx.DB) {
	// This may be the first run of the application so we need to check if the migrations table exists or not.
	// If it doesn't, then the first thing to do is to create it.
	_, err := db.Exec("IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'migrations') CREATE TABLE migrations (id INT PRIMARY KEY IDENTITY NOT NULL, filename NVARCHAR(500) NOT NULL, hash NVARCHAR(32) NOT NULL, CONSTRAINT UniqueMigration UNIQUE(filename, hash));")
	if err != nil {
		logger.Error(
			"error creating migrations table",
			"error", err,
		)
		os.Exit(1)
	}

	// We compare the list of migration filenames and hashes from the file system, against those recorded in the migrations table.
	// If the file exists in the migrations table, but the hashes do not match, then there is a problem that we need to manually look at.
	migs := []migration{}

	err = db.Select(&migs, "SELECT filename, hash FROM migrations")
	if err != nil {
		logger.Error(
			"unable to retrieve migrations from database",
			"error", err,
		)
		os.Exit(1)
	}

	// Making a map of the migrations allows us to check whether a filename (key) exists much easier than iterating over the slice each time.
	migrations := make(map[string]string)

	for _, m := range migs {
		migrations[m.Filename] = m.Hash
	}

	migrationFiles, err := atom.Migrations.ReadDir("migrations")
	if err != nil {
		logger.Error(
			"unable to read migration files",
			"error", err,
		)
		os.Exit(1)
	}

	var files []string

	for _, f := range migrationFiles {
		files = append(files, f.Name())
	}

	sort.Strings(files)

	// All migrations are completed within a transaction.
	// If there is a problem we can rollback the transaction to ensure the whole thing is atomic.
	tx, err := db.Beginx()
	if err != nil {
		logger.Error(
			"unable to begin transaction",
			"error", err,
		)
		os.Exit(1)
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			logger.Error(
				"error running migrations",
				"error", err,
			)

			os.Exit(1)
		}
	}()

	for _, file := range files {
		path := fmt.Sprintf("migrations/%s", file)
		b, err := atom.Migrations.ReadFile(path)
		if err != nil {
			logger.Error(
				"error retrieving migration file contents",
				"error", err,
				"migration", file,
			)

			return
		}

		// Generate a hash of the file contents so that we can compare it against an existing migration if the file has already been run before
		hash := md5.New()

		// Strip line breaks in order to ensure that, whether Atom is running on Windows or Linux, migration hashes are the same if the OS changes.
		t := strings.ReplaceAll(string(b), "\r\n", "")
		t = strings.ReplaceAll(t, "\n", "")

		hash.Write([]byte(t))

		// Get the 16 byte hash
		hashInBytes := hash.Sum(nil)

		filehash := hex.EncodeToString(hashInBytes)

		// If the file exists as a migration that has already been run, we need to check the hash.
		// If the file hash differs from that stored in the migration table, we should rollback the transaction and immediately exit so that it can be looked at.
		if mighash, ok := migrations[file]; ok {
			if mighash != filehash {
				logger.Error(
					"hash of already run migration does not match",
					"error", err,
					"migration", file,
				)

				return
			}

			// We can assume that the hash is the same, in which case there is no need to run the migration again so let's not bother
			continue
		}

		// Migrations are idempotent so running them again won't do anything.
		logger.Info(
			"running migration",
			"migration", file,
		)

		_, err = db.Exec(string(b))
		if err != nil {
			logger.Error(
				"error running migration",
				"error", err,
				"migration", string(b),
			)

			return
		}

		_, err = db.Exec("IF NOT EXISTS (SELECT 1 FROM migrations WHERE filename = @p1 AND hash = @p2) INSERT INTO migrations (filename, hash) VALUES (@p1, @p2);", file, filehash)
		if err != nil {
			if err, ok := err.(mssql.Error); ok {
				// Error code 2627 occurs when trying to insert a record which would be a duplicate.
				// In this case it means that the migration record already exists; that's ok for us so we can just ignore the error.
				if err.SQLErrorNumber() == 2627 {
					continue
				}
			}

			logger.Error(
				"error updating migration table",
				"error", err,
			)

			return
		}
	}
}
