package main

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	model "rokett.me/atom/internal"
)

func (app *application) showScriptsPage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	filters := make(map[string]string)
	if r.URL.Query().Get("filter") != "" {
		parts := strings.Split(r.URL.Query().Get("filter"), " ")

		for _, part := range parts {
			if !strings.Contains(part, ":") {
				app.serverError(w, errors.New("invalid filter defined"))
				return
			}

			t := strings.SplitN(part, ":", 2)
			filters[t[0]] = t[1]
		}
	}

	scripts, err := app.scripts.List(filters)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "scripts.page.html", &templateData{
		ActivePage:      "scripts",
		Flash:           app.session.PopString(r, "flash"),
		FilterString:    r.URL.Query().Get("filter"),
		IsAuthenticated: true,
		Scripts:         scripts,
		UserRole:        role,
	})
}

func (app *application) showAddScriptPage(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "scripts_detail.page.html", &templateData{
		ActivePage:      "scripts_detail",
		IsAuthenticated: true,
	})
}

func (app *application) searchScripts(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	scripts, err := app.scripts.SearchByName(r.PostForm.Get("script"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "scripts.partial.html", &templateData{
		Scripts: scripts,
	})
}

func (app *application) addScript(w http.ResponseWriter, r *http.Request) {
	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	validationErrors := make(map[string]string)

	if strings.TrimSpace(r.PostForm.Get("name")) == "" {
		validationErrors["name"] = "Cannot be blank"
	}

	uniqueScript, _, err := app.isUniqueScript(r.PostForm.Get("name"))
	if err != nil {
		app.logger.Error("error checking that script is unique",
			"client_ip", clientIP,
			"trace_id", traceID,
			"script_name", r.PostForm.Get("name"),
			"error", err,
		)

		app.render(w, r, "scripts_detail.page.html", &templateData{
			ActivePage:      "scripts_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
		})

		return
	}

	if !uniqueScript {
		validationErrors["name"] = "script already exists"
	}

	if len(validationErrors) > 0 {
		app.render(w, r, "scripts_detail.page.html", &templateData{
			ActivePage:      "scripts_detail",
			ErrorMessage:    "validation failed",
			FormData:        r.PostForm,
			FormErrors:      validationErrors,
			IsAuthenticated: true,
		})
		return
	}

	script := model.Script{
		Name: r.PostForm.Get("name"),
	}

	err = app.scripts.Insert(script)
	if err != nil {
		app.render(w, r, "scripts_detail.page.html", &templateData{
			ActivePage:      "scripts_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
		})
		return
	}

	app.updateApprovedScriptsConfig()

	app.session.Put(r, "flash", "Added approved script: "+r.PostForm.Get("name"))

	http.Redirect(w, r, "/scripts", http.StatusSeeOther)
}

func (app *application) showScriptExecutePage(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	script, err := app.scripts.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("parameters", r.URL.Query().Get("parameters"))
	form.Set("executor", r.URL.Query().Get("executor"))
	form.Set("execution_username", r.URL.Query().Get("execution_username"))
	form.Set("scope", r.URL.Query().Get("scope"))
	form.Set("expire_queued_job_after_duration", r.URL.Query().Get("expire_queued_job_after_duration"))

	// Set expiry time to application level config definition if it is blank in the job schedule definition
	if r.URL.Query().Get("expire_queued_job_after_duration") == "" {
		form.Set("expire_queued_job_after_duration", app.config.Server.ExpireQueuedJobAfter)
	}

	app.render(w, r, "script_execute.page.html", &templateData{
		ActivePage:      "scripts_execute",
		FormData:        form,
		IsAuthenticated: true,
		Script:          script,
	})
}

func (app *application) executeScript(w http.ResponseWriter, r *http.Request) {
	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	jobRequest := model.JobRequest{
		Script:                       r.PostForm.Get("name"),
		Scope:                        r.PostForm.Get("scope"),
		Executor:                     r.PostForm.Get("executor"),
		ExpireQueuedJobAfterDuration: r.PostForm.Get("expire_queued_job_after_duration"),
	}

	if r.PostForm.Get("parameters") != "" {
		jobRequest.Parameters = r.PostForm.Get("parameters")
	}

	if r.PostForm.Get("execution_username") != "" {
		jobRequest.ExecutionUsername = r.PostForm.Get("execution_username")
	}

	script := model.Script{
		Name: jobRequest.Script,
	}

	// Validating the job request expects a list of allowed scripts.
	// We pull the list of scripts from the DB, add each script to a map, and pass that to the validation function.
	scripts, err := app.scripts.List(nil)
	if err != nil {
		app.serverError(w, err)
		return
	}

	// We need to carry out some validation that the job passed by the user is actually valid.
	// We can't validate the parameters, but we can validate that required fields are included and that
	// valid values have been passed for those fields which expect them.
	ve, err := jobRequest.Validate(scripts)
	if err != nil {
		validationErrors := make(map[string]string)
		for _, v := range ve {
			validationErrors[v.Parameter] = v.Error
		}

		app.render(w, r, "script_execute.page.html", &templateData{
			ActivePage:      "scripts_execute",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			FormErrors:      validationErrors,
			IsAuthenticated: true,
			Script:          script,
		})

		return
	}

	// We can ignore the error here when parsing the duration as we have already tested that this works when validating the job request
	expireAfterDur, _ := time.ParseDuration(jobRequest.ExpireQueuedJobAfterDuration)

	job := model.JobExecution{
		Executor:                      jobRequest.Executor,
		Script:                        jobRequest.Script,
		Parameters:                    jobRequest.Parameters,
		Scope:                         strings.ToLower(jobRequest.Scope),
		ExecutionUsername:             jobRequest.ExecutionUsername,
		ExpireQueuedJobAfterTimestamp: time.Now().Add(expireAfterDur),
		ExpireQueuedJobAfterDuration:  jobRequest.ExpireQueuedJobAfterDuration,
		Status:                        "queued",
		Log:                           "",
		ExecutionRequestedBy:          r.Context().Value(usernameCtxKey).(string),
		ClientIP:                      clientIP,
		TraceID:                       traceID,
	}

	_, executionErrors, err := app.executeJob(job)
	if err != nil {
		if len(executionErrors) == 0 {
			executionErrors = append(executionErrors, err.Error())
		}

		for _, v := range executionErrors {
			app.logger.Error("unable to execute requested job",
				"job_id", job.ID,
				"error", v,
				"executor", job.Executor,
				"script", job.Script,
				"job_parameters", job.Parameters,
				"scope", job.Scope,
				"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
				"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
			)
		}

		app.render(w, r, "script_execute.page.html", &templateData{
			ActivePage:      "scripts_execute",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			Script:          script,
		})

		return
	}

	http.Redirect(w, r, "/jobs", http.StatusSeeOther)
}
