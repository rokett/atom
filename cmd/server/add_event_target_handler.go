package main

import (
	"encoding/json"
	"net/http"

	model "rokett.me/atom/internal"
)

func (app *application) addEventTargetHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		var eventTarget model.EventTarget

		err := json.NewDecoder(r.Body).Decode(&eventTarget)
		if err != nil {
			app.logger.Error("error decoding JSON request to add a new event target",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error decoding JSON request to add a new event target"
			APIResponse.Send(http.StatusBadRequest, w)

			return
		}

		ve, err := eventTarget.Validate()
		if err != nil {
			json, err := json.Marshal(ve)
			if err != nil {
				app.logger.Error("unable to encode errors from validation process",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusInternalServerError)

				APIResponse.Message = "unable to encode errors from validation process"
				APIResponse.Error = err.Error()
				APIResponse.Send(http.StatusInternalServerError, w)

				return
			}

			app.logger.Error("error(s) when validating request to add event target",
				"validation_errors", string(json),
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(json)

			return
		}

		eventTarget.ID, err = eventTarget.Upsert(app.db)
		if err != nil {
			app.logger.Error("error upserting event target",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			APIResponse := Response{
				Message: "unable to upsert event target",
				Error:   err.Error(),
			}

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		APIResponse.Result = eventTarget
		APIResponse.Send(http.StatusCreated, w)
	})
}
