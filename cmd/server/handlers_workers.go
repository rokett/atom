package main

import (
	"errors"
	"net/http"
	"strings"
)

func (app *application) showWorkersPage(w http.ResponseWriter, r *http.Request) {
	filters := make(map[string]string)
	if r.URL.Query().Get("filter") != "" {
		parts := strings.Split(r.URL.Query().Get("filter"), " ")

		for _, part := range parts {
			if !strings.Contains(part, ":") {
				app.serverError(w, errors.New("invalid filter defined"))
				return
			}

			t := strings.SplitN(part, ":", 2)
			filters[t[0]] = t[1]
		}

		// The UI names some fields differently than the database, so we need to ensure we set the filter keys correctly or the SQL query will bomb
		if _, ok := filters["worker"]; ok {
			filters["name"] = filters["worker"]
			delete(filters, "worker")
		}
		if _, ok := filters["server"]; ok {
			filters["server_name"] = filters["server"]
			delete(filters, "server")
		}
	}

	workers, err := app.workers.List(filters)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "workers.page.html", &templateData{
		ActivePage:      "workers",
		FilterString:    r.URL.Query().Get("filter"),
		IsAuthenticated: true,
		Workers:         workers,
	})
}
