package main

import (
	"encoding/json"
	"net/http"

	model "rokett.me/atom/internal"
)

// Response represents the API response content
type Response struct {
	Message       string               `json:"message,omitempty"`
	Error         string               `json:"error,omitempty"`
	TraceID       string               `json:"trace_id,omitempty"`
	Result        interface{}          `json:"data,omitempty"`
	JobExecutions []model.JobExecution `json:"job_executions,omitempty"`
	JobID         int64                `json:"job_id,omitempty"`
	Errors        []string             `json:"errors,omitempty"`
}

// Send API response back to client
func (r Response) Send(httpStatus int, w http.ResponseWriter) {
	json, err := json.Marshal(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(httpStatus)
	w.Write(json)
}
