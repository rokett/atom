package main

import (
	"net/http"
	"time"

	"rokett.me/atom/internal/authentication"
	"rokett.me/atom/internal/uuid"
)

func (app *application) showLoginPage(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "login.page.html", &templateData{
		Flash:           app.session.PopString(r, "flash"),
		IsAuthenticated: false,
	})
}

func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {
	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	if r.PostForm.Get("username") == "" || r.PostForm.Get("password") == "" {
		app.render(w, r, "login.page.html", &templateData{
			ActivePage:      "login",
			ErrorMessage:    "You must provide a username and password",
			FormData:        r.PostForm,
			IsAuthenticated: false,
		})

		return
	}

	creds := authentication.Creds{
		AuthUsername: r.PostForm.Get("username"),
		AuthPassword: r.PostForm.Get("password"),
	}

	u, err := authentication.IsUserAuthorised(app.config.Auth.Hosts, app.config.Auth.Port, app.config.Auth.BaseDN, app.config.Auth.BindDN, app.config.Auth.BindPW, creds, app.db)
	if err != nil {
		app.logger.Error("authentication error",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
			"username", creds.AuthUsername,
		)

		app.render(w, r, "login.page.html", &templateData{
			ActivePage:      "login",
			ErrorMessage:    "Authentication error",
			FormData:        r.PostForm,
			IsAuthenticated: false,
		})

		return
	}

	token, err := uuid.NewV4()
	if err != nil {
		app.logger.Error("unable to generate token for user",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
		)

		app.render(w, r, "login.page.html", &templateData{
			ActivePage:      "login",
			ErrorMessage:    "Unable to generate token",
			FormData:        r.PostForm,
			IsAuthenticated: false,
		})

		return
	}
	u.Token = token.String()
	u.TokenExpiry = time.Now().Add(2592000 * time.Second)
	exp := time.Until(u.TokenExpiry)
	u.TokenMaxAge = int(exp.Seconds())

	u.LastLogin = time.Now()

	u.ID, err = u.Upsert(app.db)
	if err != nil {
		app.logger.Error("error upserting user",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
		)

		app.render(w, r, "login.page.html", &templateData{
			ActivePage:      "login",
			ErrorMessage:    "Unable to update user token",
			FormData:        r.PostForm,
			IsAuthenticated: false,
		})

		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:   "session_token",
		Value:  u.Token,
		MaxAge: u.TokenMaxAge,
		Path:   "/",
	})

	http.Redirect(w, r, "/jobs", http.StatusSeeOther)
}
