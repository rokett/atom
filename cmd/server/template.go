package main

import (
	"html/template"
	"io/fs"
	"net/url"
	"path/filepath"
	"regexp"
	"time"

	"rokett.me/atom"
	model "rokett.me/atom/internal"
)

type templateData struct {
	Action                 string
	ActivePage             string
	AppBuild               string
	AppVersion             string
	ErrorMessage           string
	EventTarget            model.EventTarget
	EventTargets           []model.EventTarget
	Flash                  string
	FilterString           string
	FormData               url.Values
	FormErrors             map[string]string
	IsAuthenticated        bool
	Job                    model.JobExecution
	Jobs                   []model.JobExecution
	JobsDatepickerString   string
	JobSchedules           []model.JobSchedule
	MaintenanceModeEnabled bool
	Script                 model.Script
	Scripts                []model.Script
	UserRole               string
	Workers                []model.Worker
}

func newTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := fs.Glob(atom.UI, "ui/dist/html/*.page.html")
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.New(name).Funcs(functions).ParseFS(atom.UI, page)
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseFS(atom.UI, "ui/dist/html/*.layout.html")
		if err != nil {
			return nil, err
		}

		/*ts, err = ts.ParseFS(hive.UI, "ui/dist/html/*.partial.html")
		if err != nil {
			return nil, err
		}*/

		cache[name] = ts
	}

	partials, err := fs.Glob(atom.UI, "ui/dist/html/*.partial.html")
	if err != nil {
		return nil, err
	}

	for _, partial := range partials {
		name := filepath.Base(partial)

		ts, err := template.New(name).Funcs(functions).ParseFS(atom.UI, partial)
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}

	return cache, nil
}

func friendlydate(t time.Time) string {
	return t.Format("02 Jan 2006 @ 15:04:05")
}

func friendlyJobStatus(s string) string {
	status := map[string]string{
		"UNAUTHORISED_SCRIPT":     "Unauthorised Script",
		"queued":                  "Job Queued",
		"dequeuing":               "Job Queued",
		"ACCEPTED":                "Job Accepted By Worker",
		"STARTING":                "Job Starting",
		"ERROR_STARTING":          "Error Starting Job",
		"EXPIRED":                 "Queued job expired",
		"CANCELLED":               "Queued job cancelled",
		"CANCELLED_TIMEOUT":       "Job timed out",
		"CANCELLED_MANUAL":        "Job cancelled",
		"RUNNING":                 "Job Running",
		"ERROR":                   "Error",
		"COMPLETED":               "Completed",
		"COMPLETED_WITH_WARNINGS": "Completed With Warnings",
		"Failed - No workers":     "Failed - No Workers",
		"RECEIVED_BY_WORKER":      "Received by worker",
	}

	return status[s]
}

func isJobFinished(s string) bool {
	status := map[string]bool{
		"ERROR":                   true,
		"COMPLETED":               true,
		"COMPLETED_WITH_WARNINGS": true,
		"Failed - No workers":     true,
		"EXPIRED":                 true,
		"CANCELLED":               true,
		"CANCELLED_TIMEOUT":       true,
		"CANCELLED_MANUAL":        true,
	}

	return status[s]
}

func isJobCancellable(s string) bool {
	status := map[string]bool{
		"queued":             true,
		"ACCEPTED":           true,
		"STARTING":           true,
		"RUNNING":            true,
		"RECEIVED_BY_WORKER": true,
	}

	return status[s]
}

func formatJobLog(log, executor string) template.HTML {
	// For display purposes, we add colour to the log output for different types of messages.
	// Error text is coloured red for example.

	if executor == "powershell" || executor == "pwsh" {
		var re = regexp.MustCompile(`(?m)(WARNING:.*)`)
		var substitution = "<span class=\"text-orange-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(VERBOSE:.*)`)
		substitution = "<span class=\"text-amber-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(DEBUG:.*)`)
		substitution = "<span class=\"text-emerald-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)((.*\r\n){2})(\+)`)
		substitution = "<span class=\"text-red-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(\+.*)`)
		substitution = "<span class=\"text-red-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)
	}

	if executor == "ansible" {
		var re = regexp.MustCompile(`(?m)(skipping: .*)`)
		var substitution = "<span class=\"text-blue-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(skipped=\d+)`)
		substitution = "<span class=\"text-emerald-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(ok: .*)`)
		substitution = "<span class=\"text-emerald-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(ok=\d+)`)
		substitution = "<span class=\"text-emerald-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(included: .*)`)
		substitution = "<span class=\"text-blue-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(fatal: .*)`)
		substitution = "<span class=\"text-red-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(failed=\d+)`)
		substitution = "<span class=\"text-red-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(changed: .*)`)
		substitution = "<span class=\"text-amber-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(changed=\d+)`)
		substitution = "<span class=\"text-amber-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(\[DEPRECATION WARNING\].*)`)
		substitution = "<span class=\"text-violet-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)

		re = regexp.MustCompile(`(?m)(\[WARNING\].*)`)
		substitution = "<span class=\"text-orange-500\">$1</span>"
		log = re.ReplaceAllString(log, substitution)
	}

	return template.HTML(log)
}

var functions = template.FuncMap{
	"formatJobLog":      formatJobLog,
	"friendlydate":      friendlydate,
	"friendlyJobStatus": friendlyJobStatus,
	"isJobFinished":     isJobFinished,
	"isJobCancellable":  isJobCancellable,
}
