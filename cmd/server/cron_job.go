package main

import (
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

type cronJob struct {
	ID                           int64
	Executor                     string
	Script                       string
	Parameters                   string
	Scope                        string
	ExpireQueuedJobAfterDuration string
	Status                       string
	Log                          string
	ExecutionRequestedBy         string
	ExecutionUsername            string
	App                          *application
}

func (job cronJob) Run() {
	// if job does not exist in DB, publish deletion and exit
	exists, err := job.App.doesScheduledJobExist(job.ID)
	if err != nil {
		job.App.logger.Error("unable to determine if scheduled job exists or not",
			"error", err,
			"job_id", job.ID,
		)

		return
	}
	if !exists {
		job.App.removeScheduledJobFromCron(job.ID)
		return
	}

	// We can ignore the error here when parsing the duration as we have already tested that this works when validating the job request
	expireAfterDur, _ := time.ParseDuration(job.ExpireQueuedJobAfterDuration)

	je := model.JobExecution{
		Executor:                      job.Executor,
		Script:                        job.Script,
		Parameters:                    job.Parameters,
		Scope:                         strings.ToLower(job.Scope),
		ExpireQueuedJobAfterTimestamp: time.Now().Add(expireAfterDur),
		ExpireQueuedJobAfterDuration:  job.ExpireQueuedJobAfterDuration,
		ExecutionUsername:             job.ExecutionUsername,
		Status:                        "queued",
		Log:                           job.Log,
		ExecutionRequestedBy:          job.ExecutionRequestedBy,
	}

	_, executionErrors, err := job.App.executeJob(je)
	if err != nil {
		if len(executionErrors) == 0 {
			executionErrors = append(executionErrors, err.Error())
		}

		for _, v := range executionErrors {
			job.App.logger.Error("unable to execute requested job",
				"job_id", job.ID,
				"error", v,
				"executor", je.Executor,
				"script", je.Script,
				"job_parameters", je.Parameters,
				"scope", je.Scope,
				"expire_queued_job_after_timetamp", je.ExpireQueuedJobAfterTimestamp,
				"expire_queued_job_after_duration", je.ExpireQueuedJobAfterDuration,
			)
		}
	}
}
