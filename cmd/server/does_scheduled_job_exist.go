package main

import (
	"fmt"
)

func (app *application) doesScheduledJobExist(id int64) (bool, error) {
	var count int64

	err := app.db.Get(&count, "SELECT COUNT(id) FROM jobs WHERE id = @p1", id)
	if err != nil {
		return true, fmt.Errorf("error checking if scheduled job exists: %w", err)
	}

	if count > 0 {
		return true, nil
	}

	return false, nil
}
