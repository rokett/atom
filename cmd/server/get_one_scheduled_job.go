package main

import (
	"fmt"

	model "rokett.me/atom/internal"
)

func (app *application) getOneScheduledJob(id int64) (job model.JobSchedule, err error) {
	q := "SELECT id, executor, script, parameters, scope, expire_queued_job_after_duration, execution_username, schedule, created_at, updated_at FROM jobs WHERE id = @p1"

	err = app.db.Get(&job, q, id)
	if err != nil {
		return job, fmt.Errorf("error retrieving scheduled job: %w", err)
	}

	return job, nil
}
