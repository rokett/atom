package main

import (
	"net/http"

	model "rokett.me/atom/internal"
)

func (app *application) returnAllWorkers() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		workers := []model.Worker{}

		err := app.db.Select(&workers, "SELECT id, name, os, ip, server_name, status, groups, version, heartbeat_timestamp, created_at, updated_at FROM workers ORDER BY name")
		if err != nil {
			app.logger.Error("error retrieving workers",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving workers"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		APIResponse.Result = workers
		APIResponse.Send(http.StatusOK, w)
	})
}
