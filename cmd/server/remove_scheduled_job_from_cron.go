package main

import (
	"github.com/robfig/cron/v3"
)

func (app *application) removeScheduledJobFromCron(id int64) {
	app.cron.Remove(cron.EntryID(app.config.Server.CronEntries[id]))
	delete(app.config.Server.CronEntries, id)
}
