package main

import (
	"bytes"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func logErrorMetrics(status int) {
	// We don't really care about specific status codes, just the type
	code := strconv.Itoa(status)
	switch code[0:1] {
	case "2":
		code = "2xx"
	case "3":
		code = "3xx"
	case "4":
		code = "4xx"
	case "5":
		code = "5xx"
	}

	httpErrorsTotal.WithLabelValues(code).Inc()
}

func (app *application) addDefaultData(td *templateData, _ *http.Request) *templateData {
	if td == nil {
		td = &templateData{}
	}

	td.MaintenanceModeEnabled = app.maintenanceMode

	return td
}

func (app *application) serverError(w http.ResponseWriter, err error) {
	app.logger.Error(err.Error())

	logErrorMetrics(http.StatusInternalServerError)

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func (app *application) clientError(w http.ResponseWriter, status int) {
	logErrorMetrics(status)

	http.Error(w, http.StatusText(status), status)
}

func (app *application) render(w http.ResponseWriter, r *http.Request, name string, td *templateData) {
	ts, ok := app.templateCache[name]
	if !ok {
		app.serverError(w, fmt.Errorf("the %s template does not exist", name))
		return
	}

	buf := new(bytes.Buffer)

	err := ts.Execute(buf, app.addDefaultData(td, r))
	if err != nil {
		app.serverError(w, err)
		return
	}

	w.Header().Set("Content-Type", http.DetectContentType(buf.Bytes()))

	_, err = buf.WriteTo(w)
	if err != nil {
		app.serverError(w, err)
		return
	}
}

func (app *application) weekStart(year, week int) time.Time {
	// Start from the middle of the year:
	t := time.Date(year, 7, 1, 0, 0, 0, 0, time.UTC)

	// Roll back to Monday:
	if wd := t.Weekday(); wd == time.Sunday {
		t = t.AddDate(0, 0, -6)
	} else {
		t = t.AddDate(0, 0, -int(wd)+1)
	}

	// Difference in weeks:
	_, w := t.ISOWeek()
	t = t.AddDate(0, 0, (week-w)*7)

	return t
}

func (app *application) weekRange(year, week int) (time.Time, time.Time) {
	start := app.weekStart(year, week)
	year, month, day := start.AddDate(0, 0, 6).Date()
	end := time.Date(year, month, day, 23, 59, 59, 0, time.UTC)

	return start, end
}
