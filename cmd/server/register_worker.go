package main

import (
	"fmt"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) registerWorker(worker model.Worker) error {
	maxRetries := 10
	var dbNotThere bool
	var err error

	for retry := 1; retry <= maxRetries; retry++ {
		_, err = app.db.NamedExec("INSERT INTO workers (name, ip, os, server_name, status, groups, version) VALUES (:name, :ip, :os, :server_name, :status, :groups, :version)", worker)
		if err != nil && (strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host") || strings.Contains(err.Error(), "No connection could be made because the target machine actively refused it")) {
			dbNotThere = true
			time.Sleep(time.Duration(retry) * (10 * time.Second))
			continue
		}
		if err != nil {
			return fmt.Errorf("INSERT INTO workers (name, ip, os, server_name, status, groups, version) VALUES (%s, %s, %s, %s, %s, %s, %s): %w", worker.Name, worker.IP, worker.Os, worker.ServerName, worker.Status, worker.Groups, worker.Version, err)
		}

		dbNotThere = false
		break
	}

	if dbNotThere {
		return fmt.Errorf("INSERT INTO workers (name, ip, os, server_name, status, groups, version) VALUES (%s, %s, %s, %s, %s, %s, %s): %w", worker.Name, worker.IP, worker.Os, worker.ServerName, worker.Status, worker.Groups, worker.Version, err)
	}

	go app.setWorkerMetrics()

	return nil
}
