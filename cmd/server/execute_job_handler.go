package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) executeJobHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		//start := time.Now()

		body, err := io.ReadAll(r.Body)
		if err != nil {
			app.logger.Error("unable to read HTTP request body",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Message = "unable to read HTTP request body"
			APIResponse.Error = err.Error()
			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		jobRequest := model.JobRequest{}
		err = json.Unmarshal(body, &jobRequest)
		if err != nil {
			app.logger.Error("unable to decode job request",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"job_request", body,
			)

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Message = "unable to decode job request"
			APIResponse.Error = err.Error()
			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		app.logger.Debug("Validate job",
			"client_ip", clientIP,
			"trace_id", traceID,
			"job_request", body,
		)

		// We need to carry out some validation that the job passed by the user is actually valid.
		// We can't validate the parameters, but we can validate that required fields are included and that
		// valid values have been passed for those fields which expect them.
		ve, err := jobRequest.Validate(app.config.Server.AllowedScripts)
		if err != nil {
			json, err := json.Marshal(ve)
			if err != nil {
				app.logger.Error("unable to encode errors from validation process",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusInternalServerError)

				APIResponse.Message = "unable to encode errors from validation process"
				APIResponse.Error = err.Error()
				APIResponse.Send(http.StatusInternalServerError, w)

				return
			}

			app.logger.Error("error(s) when validating incoming query",
				"validation_errors", string(json),
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(json)

			return
		}

		// We need to check if there is an expiry time for the job.  We start with the application level default.
		// If there is a job specific expiry time we will use that instead.
		// We can ignore the error here when parsing the duration as we have already tested that this works when validating the job request
		// or, in the case of the default, when starting the server.
		if jobRequest.ExpireQueuedJobAfterDuration == "" {
			jobRequest.ExpireQueuedJobAfterDuration = app.config.Server.ExpireQueuedJobAfter
		}
		expireAfterDur, _ := time.ParseDuration(jobRequest.ExpireQueuedJobAfterDuration)

		job := model.JobExecution{
			Executor:                      jobRequest.Executor,
			Script:                        jobRequest.Script,
			Parameters:                    jobRequest.Parameters,
			Scope:                         strings.ToLower(jobRequest.Scope),
			ExpireQueuedJobAfterTimestamp: time.Now().Add(expireAfterDur),
			ExpireQueuedJobAfterDuration:  jobRequest.ExpireQueuedJobAfterDuration,
			ExecutionUsername:             jobRequest.ExecutionUsername,
			Status:                        "queued",
			Log:                           "",
			ExecutionRequestedBy:          r.Context().Value(usernameCtxKey).(string),
			ClientIP:                      clientIP,
			TraceID:                       traceID,
		}

		jobExecutions, executionErrors, err := app.executeJob(job)
		if err != nil {
			if len(executionErrors) == 0 {
				executionErrors = append(executionErrors, err.Error())
			}

			for _, v := range executionErrors {
				app.logger.Error("unable to execute requested job",
					"job_id", job.ID,
					"error", v,
					"executor", job.Executor,
					"script", job.Script,
					"job_parameters", job.Parameters,
					"scope", job.Scope,
					"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
					"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
				)
			}

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Message = "error executing job"
			APIResponse.Errors = executionErrors
			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		//duration := time.Since(start)
		//requestDuration.WithLabelValues(strconv.Itoa(http.StatusAccepted)).Observe(duration.Seconds())
		APIResponse.JobExecutions = jobExecutions
		APIResponse.Send(http.StatusAccepted, w)
	})
}
