package main

import (
	"fmt"
	"strings"

	model "rokett.me/atom/internal"

	"github.com/prometheus/client_golang/prometheus"
)

func (app *application) logJobCompletion(job model.JobExecution) {
	app.logger.Info("completed job execution",
		"job_id", job.ID,
		"executor", job.Executor,
		"script", fmt.Sprintf("%s %s", job.Script, job.Parameters),
		"scope", job.Scope,
		"status", job.Status,
		"started_at", job.StartedAt,
		"finished_at", job.FinishedAt,
		"execution_time_ms", job.ExecutionTimeMs,
		"execution_requested_by", job.ExecutionRequestedBy,
		"worker_name", job.WorkerName,
		"worker_ip", job.WorkerIP,
		"worker_server_name", job.WorkerServerName,
		"worker_os", job.WorkerOs,
	)

	// Increment total job runs metric
	jobType := "ad-hoc"
	if job.ExecutionRequestedBy == "scheduled" {
		jobType = "scheduled"
	}

	jobRunsTotal.With(
		prometheus.Labels{
			"type":   jobType,
			"job":    job.Script,
			"status": strings.ToLower(job.Status),
		},
	).Inc()

	executionTimeSeconds := job.ExecutionTimeMs / 1000
	jobDurationSeconds.With(
		prometheus.Labels{
			"job": job.Script,
		},
	).Observe(float64(executionTimeSeconds))
}
