package main

import (
	"database/sql"
	"errors"
	"os"

	model "rokett.me/atom/internal"

	"github.com/robfig/cron/v3"
)

func (app *application) setupCron() (c *cron.Cron) {
	var jobs []model.JobSchedule

	q := "SELECT id FROM jobs WHERE paused = 0"

	err := app.db.Select(&jobs, q)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		app.logger.Error(
			"error getting scheduled jobs",
			"query", q,
			"error", err,
		)
		os.Exit(1)
	}

	c = cron.New()
	app.cron = c

	for _, job := range jobs {
		err = app.addScheduledJobToCron(job.ID)
		if err != nil {
			app.logger.Error(err.Error())
		}
	}

	if !app.config.DisableStaleWorkerCleanup {
		c.AddFunc("@every 1m", func() {
			app.cleanupStaleWorkers()
		})
	}

	c.Start()

	return c
}
