package main

import (
	"fmt"
)

func (app *application) addScheduledJobToCron(id int64) error {
	job, err := app.getOneScheduledJob(id)
	if err != nil {
		return err
	}

	entryID, err := app.cron.AddJob(job.Schedule, cronJob{
		ID:                           job.ID,
		Executor:                     job.Executor,
		Script:                       job.Script,
		Parameters:                   job.Parameters,
		Scope:                        job.Scope,
		ExpireQueuedJobAfterDuration: job.ExpireQueuedJobAfterDuration,
		Status:                       "queued",
		Log:                          "",
		ExecutionRequestedBy:         "scheduled",
		ExecutionUsername:            job.ExecutionUsername,
		App:                          app,
	})
	if err != nil {
		return fmt.Errorf("unable to add job to cron: %w", err)
	}

	app.config.Server.CronEntries[id] = entryID

	return nil
}
