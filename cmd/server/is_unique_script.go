package main

import (
	"database/sql"
	"errors"
	"fmt"
)

func (app *application) isUniqueScript(script string) (bool, int64, error) {
	var id int64

	err := app.db.Get(&id, "SELECT id FROM scripts WHERE script = @p1;", script)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return true, 0, nil
	}
	if err != nil {
		query := fmt.Sprintf("SELECT id FROM scripts WHERE script = %s;", script)

		return false, 0, fmt.Errorf("problem with query %s: %w", query, err)
	}

	return false, id, nil
}
