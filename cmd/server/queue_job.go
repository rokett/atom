package main

import (
	"fmt"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) queueJob(job model.JobExecution) (model.JobExecution, error) {
	app.logger.Debug("queue job for execution",
		"executor", job.Executor,
		"script", job.Script,
		"job_parameters", job.Parameters,
		"scope", job.Scope,
		"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
		"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
	)

	job.StartedAt = time.Time{}
	job.FinishedAt = time.Time{}

	// To account for instances when the DB may not be available, we give it a few retries with an exponential backoff time.
	// If it continues to fail then we can end with a suitable error message.
	maxRetries := 10
	var dbNotThere bool
	var err error

	for retry := 1; retry <= maxRetries; retry++ {
		err = app.db.Get(&job.ID, "INSERT INTO jobExecutions (executor, script, parameters, scope, status, execution_time_ms, execution_requested_by, execution_username, worker_name, worker_ip, process_id, started_at, finished_at, client_ip, trace_id, log, expire_queued_job_after_duration, expire_queued_job_after_timestamp) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18);SELECT id = convert(bigint, SCOPE_IDENTITY());", job.Executor, job.Script, job.Parameters, job.Scope, job.Status, job.ExecutionTimeMs, job.ExecutionRequestedBy, job.ExecutionUsername, job.WorkerName, job.WorkerIP, job.ProcessID, job.StartedAt, job.FinishedAt, job.ClientIP, job.TraceID, job.Log, job.ExpireQueuedJobAfterDuration, job.ExpireQueuedJobAfterTimestamp)
		if err != nil && (strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host") || strings.Contains(err.Error(), "No connection could be made because the target machine actively refused it")) {
			dbNotThere = true
			time.Sleep(time.Duration(retry) * (10 * time.Second))
			continue
		}
		if err != nil {
			return job, fmt.Errorf("error queueing job; INSERT INTO jobExecutions (executor, script, parameters, scope, status, execution_time_ms, execution_requested_by, execution_username, worker_name, worker_ip, process_id, started_at, finished_at, client_ip, trace_id, log, expire_queued_job_after_duration, expire_queued_job_after_timestamp) VALUES(%s, %s, %s, %s, %s, %d, %s, %s, %s, %s, %d, %s, %s, %s, %s, %s, %s, %s);SELECT id = convert(bigint, SCOPE_IDENTITY()): %w", job.Executor, job.Script, job.Parameters, job.Scope, job.Status, job.ExecutionTimeMs, job.ExecutionRequestedBy, job.ExecutionUsername, job.WorkerName, job.WorkerIP, job.ProcessID, job.StartedAt, job.FinishedAt, job.ClientIP, job.TraceID, job.Log, job.ExpireQueuedJobAfterDuration, job.ExpireQueuedJobAfterTimestamp, err)
		}

		dbNotThere = false
		break
	}

	if dbNotThere {
		return job, fmt.Errorf("unable to connect to database after %d retries: %w", maxRetries, err)
	}

	jobsQueued.Inc()

	return job, nil
}
