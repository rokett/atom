package main

import (
	"encoding/json"
	"net/http"
	"time"

	"rokett.me/atom/internal/authentication"
	"rokett.me/atom/internal/uuid"
)

func (app *application) authenticateUser() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		var creds authentication.Creds

		err := json.NewDecoder(r.Body).Decode(&creds)
		if err != nil {
			app.logger.Error("unable to decode JSON authentication request",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Message = "unable to decode JSON authentication request"
			APIResponse.Error = err.Error()
			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		ve := creds.Validate()

		if len(ve) > 0 {
			json, err := json.Marshal(ve)
			if err != nil {
				app.logger.Error("unable to encode errors from validation process",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusInternalServerError)

				APIResponse.Message = "unable to encode errors from validation process"
				APIResponse.Error = err.Error()
				APIResponse.Send(http.StatusInternalServerError, w)

				return
			}

			logErrorMetrics(http.StatusBadRequest)

			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(json)

			return
		}

		u, err := authentication.IsUserAuthorised(app.config.Auth.Hosts, app.config.Auth.Port, app.config.Auth.BaseDN, app.config.Auth.BindDN, app.config.Auth.BindPW, creds, app.db)
		if err != nil {
			app.logger.Error("authentication error",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"username", creds.AuthUsername,
			)

			logErrorMetrics(http.StatusUnauthorized)

			APIResponse.Message = "authentication error"
			APIResponse.Error = err.Error()
			APIResponse.Send(http.StatusUnauthorized, w)

			return
		}

		token, err := uuid.NewV4()
		if err != nil {
			app.logger.Error("unable to generate token for user",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			APIResponse := Response{
				Message: "unable to generate token for user",
				Error:   err.Error(),
			}

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}
		u.Token = token.String()
		u.TokenExpiry = time.Now().Add(2592000 * time.Second)
		exp := time.Until(u.TokenExpiry)
		u.TokenMaxAge = int(exp.Seconds())

		u.LastLogin = time.Now()

		u.ID, err = u.Upsert(app.db)
		if err != nil {
			app.logger.Error("error upserting user",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			APIResponse := Response{
				Message: "unable to upsert user",
				Error:   err.Error(),
			}

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:   "session_token",
			Value:  u.Token,
			MaxAge: u.TokenMaxAge,
			Path:   "/",
		})

		APIResponse.Result = u
		APIResponse.Send(http.StatusOK, w)
	})
}
