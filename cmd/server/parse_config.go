package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/robfig/cron/v3"
	model "rokett.me/atom/internal"
)

type config struct {
	Server                    server
	Database                  database
	Auth                      auth
	DisableStaleWorkerCleanup bool
}

type server struct {
	NatsHost             string
	NatsServerPort       int
	NatsConnString       string
	NatsStreamReplicas   int
	NatsNKeySeed         string
	APIPort              int
	CorsAllowedOrigins   []string
	CorsAllowedHeaders   []string
	Debug                bool
	AllowedScripts       []model.Script
	CronEntries          map[int64]cron.EntryID
	ExpireQueuedJobAfter string
	Version              string
	Build                string
}

type database struct {
	Host     string
	Port     int
	Name     string
	Username string
	Password string
}

type auth struct {
	Hosts         []string
	BaseDN        string
	BindDN        string
	BindPW        string
	Port          int
	UseSSL        bool
	StartTLS      bool
	SSLSkipVerify bool
	GroupMappings []struct {
		GroupDN string
		Role    string
	}
}

func parseConfig(natsHost string, natsServerPort int, natsStreamReplicas int, natsNKeySeed string, apiPort int, corsAllowedOrigins string, corsAllowedHeaders string, dbHost string, dbPort int, dbName string, dbUsername string, dbPassword string, authHosts string, authBaseDN string, authBindDN string, authBindPW string, authPort int, authUseSSL bool, authStartTLS bool, authSSLSkipVerify bool, disableStaleWorkerCleanup bool, expireQueuedJobAfter, version, build string) (config, error) {
	if natsStreamReplicas != 1 && natsStreamReplicas != 3 && natsStreamReplicas != 5 {
		return config{}, errors.New("invalid 'nats_stream_replicas' flag; must be 1, 3 or 5")
	}

	if natsNKeySeed == "" {
		return config{}, errors.New("you must specify a NATS NKey seed for the Atom user")
	}

	var allowedOrigins []string
	tmp := strings.Split(corsAllowedOrigins, ",")
	for _, v := range tmp {
		if strings.TrimSpace(v) == "" {
			continue
		}

		allowedOrigins = append(allowedOrigins, strings.TrimSpace(v))
	}

	var allowedHeaders []string
	tmp = strings.Split(corsAllowedHeaders, ",")
	for _, v := range tmp {
		allowedHeaders = append(allowedHeaders, strings.TrimSpace(v))
	}

	var hosts []string
	tmp = strings.Split(authHosts, ",")
	for _, v := range tmp {
		hosts = append(hosts, strings.TrimSpace(v))
	}

	var debug bool
	if os.Getenv("atom_debug") != "" {
		debug = true
	}

	return config{
		DisableStaleWorkerCleanup: disableStaleWorkerCleanup,
		Server: server{
			NatsHost:             natsHost,
			NatsServerPort:       natsServerPort,
			NatsConnString:       fmt.Sprintf("nats://%s:%d", natsHost, natsServerPort),
			NatsStreamReplicas:   natsStreamReplicas,
			NatsNKeySeed:         natsNKeySeed,
			APIPort:              apiPort,
			CorsAllowedOrigins:   allowedOrigins,
			CorsAllowedHeaders:   allowedHeaders,
			Debug:                debug,
			CronEntries:          make(map[int64]cron.EntryID),
			ExpireQueuedJobAfter: expireQueuedJobAfter,
			Version:              version,
			Build:                build,
		},
		Database: database{
			Host:     dbHost,
			Port:     dbPort,
			Name:     dbName,
			Username: dbUsername,
			Password: dbPassword,
		},
		Auth: auth{
			Hosts:         hosts,
			BaseDN:        authBaseDN,
			BindDN:        authBindDN,
			BindPW:        authBindPW,
			Port:          authPort,
			UseSSL:        authUseSSL,
			StartTLS:      authStartTLS,
			SSLSkipVerify: authSSLSkipVerify,
		},
	}, nil
}
