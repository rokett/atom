package main

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

func (app *application) removeEventTargetHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			app.logger.Error("error converting id from string to int64",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Message = "unable to parse id of event target to be removed"
			APIResponse.Error = err.Error()

			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		_, err = app.db.Exec("DELETE FROM eventTargets WHERE id = @p1;", id)
		if err != nil {
			app.logger.Error("error removing event target",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"event_target_id", id,
			)

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Message = "error removing event target"
			APIResponse.Error = err.Error()
			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		APIResponse.Send(http.StatusNoContent, w)
	})
}
