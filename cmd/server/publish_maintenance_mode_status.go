package main

func (app *application) publishMaintenanceModeStatus() {
	enabled, err := app.savedConfig.IsMaintenanceModeEnabled()
	if err != nil {
		app.logger.Error("unable to check if maintenance mode is enabled for publish",
			"error", err,
		)
		return
	}

	var status string
	if enabled {
		status = "on"
		app.maintenanceMode = true
		maintenanceMode.Set(1)
	} else {
		status = "off"
		app.maintenanceMode = false
		maintenanceMode.Set(0)
	}

	err = app.nc.Publish("update_maintenance_mode_status", []byte(status))
	if err != nil {
		app.logger.Error("unable to publish maintenance mode status update",
			"error", err,
		)
	}
}
