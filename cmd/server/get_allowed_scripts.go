package main

import (
	"database/sql"
	"errors"
	"os"

	model "rokett.me/atom/internal"
)

func (app *application) getAllowedScripts() []model.Script {
	scripts, err := app.scripts.List(nil)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		app.logger.Error(
			"getting allowed scripts",
			"error", err,
		)
		os.Exit(1)
	}

	return scripts
}
