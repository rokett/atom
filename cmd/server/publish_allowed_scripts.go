package main

import (
	"encoding/json"
	"os"

	model "rokett.me/atom/internal"
)

// Publish list of approved scripts to workers
// Workers will also request a list of approved scripts when they start up in case they start up after the server (which is pretty likely)
func (app *application) publishAllowedScripts(allowedScripts []model.Script) {
	pl, err := json.Marshal(allowedScripts)
	if err != nil {
		app.logger.Error(
			"converting allowed scripts to json",
			"error", err,
		)
		os.Exit(1)
	}

	err = app.nc.Publish("allowed_scripts:update", pl)
	if err != nil {
		app.logger.Error(
			"publishing allowed scripts",
			"error", err,
		)
		os.Exit(1)
	}
}
