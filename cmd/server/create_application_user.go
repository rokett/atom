package main

import (
	"encoding/json"
	"net/http"
	"time"

	"rokett.me/atom/internal/authentication"
	"rokett.me/atom/internal/uuid"
)

func (app *application) createApplicationUser() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		var applicationUser authentication.User

		err := json.NewDecoder(r.Body).Decode(&applicationUser)
		if err != nil {
			app.logger.Error("error decoding JSON request to create a new application user",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error decoding JSON request to create a new application user"
			APIResponse.Send(http.StatusBadRequest, w)

			return
		}

		ve, err := applicationUser.Validate()
		if err != nil {
			json, err := json.Marshal(ve)
			if err != nil {
				app.logger.Error("unable to encode errors from validation process",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				logErrorMetrics(http.StatusInternalServerError)

				APIResponse.Message = "unable to encode errors from validation process"
				APIResponse.Error = err.Error()
				APIResponse.Send(http.StatusInternalServerError, w)

				return
			}

			app.logger.Error("error(s) when validating request to create application user",
				"validation_errors", string(json),
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(json)

			return
		}

		applicationUser.Role = "user"

		token, err := uuid.NewV4()
		if err != nil {
			app.logger.Error("unable to decode JSON authentication requestunable to generate token for user",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse := Response{
				Message: "unable to generate token for user",
				Error:   err.Error(),
			}

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}
		applicationUser.Token = token.String()
		applicationUser.TokenExpiry = time.Now().Add(876000 * time.Hour) // 100 years
		exp := time.Until(applicationUser.TokenExpiry)
		applicationUser.TokenMaxAge = int(exp.Seconds())

		applicationUser.ID, err = applicationUser.Upsert(app.db)
		if err != nil {
			app.logger.Error("error upserting user",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse := Response{
				Message: "unable to upsert user",
				Error:   err.Error(),
			}

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		APIResponse.Result = applicationUser
		APIResponse.Send(http.StatusCreated, w)
	})
}
