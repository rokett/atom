package main

import (
	"net/http"
)

func (app *application) returnApprovedScripts() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		scripts, err := app.scripts.List(nil)
		if err != nil {
			app.logger.Error(
				"retrieving approved scripts",
				"client_ip", clientIP,
				"trace_id", traceID,
				"error", err,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving approved scripts"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		APIResponse.Result = scripts
		APIResponse.Send(http.StatusOK, w)
	})
}
