package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	model "rokett.me/atom/internal"
)

func (app *application) updateWorkerStatus(worker model.Worker) error {
	maxRetries := 10
	var dbNotThere bool
	var err error

	var status float64
	switch strings.ToLower(worker.Status) {
	case "idle":
		status = 0
	case "busy":
		status = 1
	case "maintenance mode":
		status = 2
	}

	workerStatus.With(
		prometheus.Labels{
			"worker_name": worker.Name,
		},
	).Set(status)

	for retry := 1; retry <= maxRetries; retry++ {
		_, err = app.db.NamedExec("UPDATE workers SET status = :status WHERE name = :name", worker)
		if err != nil && (strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host") || strings.Contains(err.Error(), "No connection could be made because the target machine actively refused it")) {
			dbNotThere = true
			time.Sleep(time.Duration(retry) * (10 * time.Second))
			continue
		}
		if err != nil {
			return fmt.Errorf("UPDATE workers SET status = %s WHERE name = %s: %w", worker.Status, worker.Name, err)
		}

		dbNotThere = false
		break
	}

	if dbNotThere {
		return fmt.Errorf("UPDATE workers SET status = %s WHERE name = %s: %w", worker.Status, worker.Name, err)
	}

	return nil
}
