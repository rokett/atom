package main

import (
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"time"
)

func (app *application) runAPIServer() *http.Server {
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", app.config.Server.APIPort),
		ErrorLog:     slog.NewLogLogger(app.logger.Handler(), slog.LevelError),
		Handler:      app.setupRouter(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 60 * time.Second,
	}

	go func() {
		app.logger.Info("running server",
			"port", app.config.Server.APIPort,
		)
		err := srv.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			app.logger.Error(
				"unable to start API server",
				"error", err,
			)
			os.Exit(1)
		}
	}()

	return srv
}
