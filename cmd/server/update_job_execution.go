package main

import (
	"fmt"
	"strings"
	"time"

	model "rokett.me/atom/internal"
)

func (app *application) updateJobExecution(job model.JobExecution) error {
	maxRetries := 10
	var dbNotThere bool
	var err error

	for retry := 1; retry <= maxRetries; retry++ {
		_, err = app.db.NamedExec("UPDATE jobExecutions SET status = :status, log = :log, worker_name = :worker_name, worker_ip = :worker_ip, worker_server_name = :worker_server_name, worker_os = :worker_os, process_id = :process_id, execution_time_ms = :execution_time_ms, started_at = :started_at, finished_at = :finished_at WHERE id = :id", job)
		if err != nil && (strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host") || strings.Contains(err.Error(), "No connection could be made because the target machine actively refused it")) {
			dbNotThere = true
			time.Sleep(time.Duration(retry) * (10 * time.Second))
			continue
		}
		if err != nil {
			return fmt.Errorf("UPDATE jobExecutions SET status = '%s', log = '%s', worker_name = '%s', worker_ip = '%s', worker_server_name = '%s', worker_os = '%s', process_id = %d, execution_time_ms = %d, started_at = '%s', finished_at = '%s' WHERE id = %d: %w", job.Status, job.Log, job.WorkerName, job.WorkerIP, job.WorkerServerName, job.WorkerOs, job.ProcessID, job.ExecutionTimeMs, job.StartedAt, job.FinishedAt, job.ID, err)
		}

		dbNotThere = false
		break
	}

	if dbNotThere {
		return fmt.Errorf("UPDATE jobExecutions SET status = '%s', log = '%s', worker_name = '%s', worker_ip = '%s', worker_server_name = '%s', worker_os = '%s', process_id = %d, execution_time_ms = %d, started_at = '%s', finished_at = '%s' WHERE id = %d: %w", job.Status, job.Log, job.WorkerName, job.WorkerIP, job.WorkerServerName, job.WorkerOs, job.ProcessID, job.ExecutionTimeMs, job.StartedAt, job.FinishedAt, job.ID, err)
	}

	// If the job has ended we log its completion and send an event to any configured targets
	completionStatus := map[string]bool{
		"EXPIRED":                 true,
		"ERROR":                   true,
		"ERROR_STARTING":          true,
		"COMPLETED":               true,
		"COMPLETED_WITH_WARNINGS": true,
		"UNAUTHORISED_SCRIPT":     true,
		"CANCELLED_TIMEOUT":       true,
		"CANCELLED_MANUAL":        true,
	}

	if completionStatus[job.Status] {
		go app.logJobCompletion(job)
		go app.sendJobStatusEvent(job)
		go app.ackJobLogMessages(job.ID)
	}

	return nil
}
