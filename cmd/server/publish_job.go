package main

import (
	"encoding/json"
	"fmt"
	"regexp"

	model "rokett.me/atom/internal"
)

func (app *application) publishJob(job model.JobExecution) (err error) {
	var subject string

	scopeGroupAny := regexp.MustCompile(`(?i)^.*:@any$`)

	// <group>:@any needs to be published to specific subjects.
	// @all, <group>:@all, and <hostnames> all get published to specific workers so we can group them together.
	switch {
	case scopeGroupAny.MatchString(job.Scope):
		subject = fmt.Sprintf("jobs.execute:%s", job.Scope)
	default:
		subject = fmt.Sprintf("jobs.execute:%s", job.WorkerName)
	}

	app.logger.Debug("request job execution",
		"job_id", job.ID,
		"executor", job.Executor,
		"script", job.Script,
		"username", job.ExecutionUsername,
		"job_parameters", job.Parameters,
		"scope", job.Scope,
		"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
		"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
		"subject", subject,
	)

	j, err := json.Marshal(job)
	if err != nil {
		return fmt.Errorf("marshaling job to JSON: %w", err)
	}

	ack, err := app.js.Publish(subject, j)
	if err != nil {
		return fmt.Errorf("error publishing job to '%s' subject: %w", subject, err)
	}

	job.SequenceNumber = ack.Sequence
	err = app.jobs.SetSequenceNumber(job)
	if err != nil {
		return fmt.Errorf("setting job sequence number (%d): :%w", ack.Sequence, err)
	}

	return nil
}
