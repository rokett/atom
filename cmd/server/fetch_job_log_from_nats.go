package main

import (
	"fmt"
	"strconv"

	"github.com/nats-io/nats.go"
)

func (app *application) fetchJobLogFromNATS(id int64) ([]*nats.Msg, error) {
	consumerName := strconv.FormatInt(id, 10)
	subject := fmt.Sprintf("job_logs.%d", id)

	_, err := app.js.AddConsumer("JOB_LOGS", &nats.ConsumerConfig{
		AckPolicy:     nats.AckExplicitPolicy,
		Durable:       consumerName,
		FilterSubject: subject,
	})
	if err != nil {
		return nil, fmt.Errorf("adding consumer for %s: %w", subject, err)
	}
	defer app.js.DeleteConsumer("JOB_LOGS", consumerName)

	// If there are no pending messages there is no reason to try and retrieve any
	// Although retrieving 0 messages is ok, as the timeout will be hit and we can live with that, it results in loading the page taking ~ 5 seconds
	// So instead we get the consumer info and subsequently check if there are 0 messages, in which case we can return instantly.
	consumerInfo, err := app.js.ConsumerInfo("JOB_LOGS", consumerName)
	if err != nil {
		return nil, fmt.Errorf("get consumer (%s) info for %s stream: %w", consumerName, "JOB_LOGS", err)
	}

	if consumerInfo.NumPending == 0 {
		return nil, nil
	}

	sub, err := app.js.PullSubscribe(subject, consumerName, nats.BindStream("JOB_LOGS"))
	if err != nil {
		return nil, fmt.Errorf("setup pull subscription for %s: %w", subject, err)
	}
	defer sub.Unsubscribe()

	// We fetch a very high number of messages in an attempt to ensure we pull the whole log.
	// We don't have any way of knowing how many there are, and we need to pass a number of msgs in to the Fetch function.
	msgs, err := sub.Fetch(10000)
	// If there are no messages waiting then the timeout will be reached and an error thrown.
	// That's not really an error though so we'll ignore it.
	if err != nil && err != nats.ErrTimeout {
		return nil, fmt.Errorf("fetching messages for %s: %w", subject, err)
	}

	return msgs, nil
}
