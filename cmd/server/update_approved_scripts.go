package main

import (
	"encoding/json"
	"net/http"
)

func (app *application) updateApprovedScripts() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		var scripts []string
		err := json.NewDecoder(r.Body).Decode(&scripts)
		if err != nil {
			app.logger.Error("error decoding JSON request",
				"client_ip", clientIP,
				"trace_id", traceID,
				"error", err,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error decoding JSON request"
			APIResponse.Send(http.StatusBadRequest, w)

			return
		}

		// We need to retrieve the existing script records.
		// This allows us to check whether a script already exists, in which case we update the record, or needs to be created.
		// We can also track which existing records have been updated, meaning that records which have not been updated can be deleted.
		existingScripts, err := app.scripts.List(nil)
		if err != nil {
			app.logger.Error(
				"retrieving approved scripts",
				"client_ip", clientIP,
				"trace_id", traceID,
				"error", err,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving approved scripts"
			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		// Making a map of the script IDs allows us to check whether a script already exists much easier than iterating over the slice each time.
		scriptIDs := make(map[string]int64)

		for _, s := range existingScripts {
			scriptIDs[s.Name] = s.ID
		}

		for _, s := range scripts {
			// If the script already exists we remove it from the map.
			// Otherwise we create a new record.
			if _, ok := scriptIDs[s]; ok {
				delete(scriptIDs, s)
			} else {
				_, err := app.db.Exec("INSERT INTO scripts (script) VALUES (@p1);", s)
				if err != nil {
					app.logger.Error("error adding new approved scripts",
						"client_ip", clientIP,
						"trace_id", traceID,
						"script_name", s,
						"error", err,
					)

					logErrorMetrics(http.StatusBadRequest)

					APIResponse.Error = err.Error()
					APIResponse.Message = "error adding new approved scripts"
					APIResponse.Send(http.StatusBadRequest, w)
					return
				}
			}
		}

		for _, id := range scriptIDs {
			_, err := app.db.Exec("DELETE FROM scripts WHERE id = @p1;", id)
			if err != nil {
				app.logger.Error("error deleting unapproved scripts",
					"client_ip", clientIP,
					"trace_id", traceID,
					"script_id", id,
					"error", err,
				)

				logErrorMetrics(http.StatusBadRequest)

				APIResponse.Error = err.Error()
				APIResponse.Message = "error deleting unapproved scripts"
				APIResponse.Send(http.StatusBadRequest, w)
				return
			}
		}

		APIResponse.Send(http.StatusOK, w)

		app.updateApprovedScriptsConfig()
	})
}
