package main

import (
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	model "rokett.me/atom/internal"
)

func (app *application) executeJobAgainHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
		traceID := r.Context().Value(traceIDCtxKey).(string)

		APIResponse := Response{
			TraceID: traceID,
		}

		// The clientIP is included in every log entry and in some metrics for later analysis
		clientIP := r.Context().Value(clientIPCtxKey).(string)

		id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			app.logger.Error("error converting id from string to int64",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Message = "unable to parse id of job to be executed"
			APIResponse.Error = err.Error()

			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		job := model.JobExecution{
			Status:               "queued",
			Log:                  "",
			ExecutionRequestedBy: r.Context().Value(usernameCtxKey).(string),
			ClientIP:             clientIP,
			TraceID:              traceID,
		}

		err = app.db.Get(&job, "SELECT executor, script, parameters, scope, execution_username, expire_queued_job_after_duration FROM jobExecutions WHERE id = @p1", id)
		if err != nil {
			app.logger.Error("error retrieving job",
				"error", err,
				"client_ip", clientIP,
				"trace_id", traceID,
				"job_id", id,
			)

			logErrorMetrics(http.StatusBadRequest)

			APIResponse.Error = err.Error()
			APIResponse.Message = "error retrieving job"

			APIResponse.Send(http.StatusBadRequest, w)
			return
		}

		// We need to recalculate the job expiry timestamp based on the original expiry duration
		expireAfterDur, _ := time.ParseDuration(job.ExpireQueuedJobAfterDuration)
		job.ExpireQueuedJobAfterTimestamp = time.Now().Add(expireAfterDur)

		jobExecutions, executionErrors, err := app.executeJob(job)
		if err != nil {
			if len(executionErrors) == 0 {
				executionErrors = append(executionErrors, err.Error())
			}

			for _, v := range executionErrors {
				app.logger.Error("unable to execute requested job",
					"job_id", job.ID,
					"error", v,
					"executor", job.Executor,
					"script", job.Script,
					"job_parameters", job.Parameters,
					"scope", job.Scope,
					"expire_queued_job_after_timetamp", job.ExpireQueuedJobAfterTimestamp,
					"expire_queued_job_after_duration", job.ExpireQueuedJobAfterDuration,
				)
			}

			logErrorMetrics(http.StatusInternalServerError)

			APIResponse.Message = "error executing job"
			APIResponse.Errors = executionErrors
			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		//duration := time.Since(start)
		//requestDuration.WithLabelValues(strconv.Itoa(http.StatusAccepted)).Observe(duration.Seconds())
		APIResponse.JobExecutions = jobExecutions
		APIResponse.Send(http.StatusAccepted, w)
	})
}
