package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"

	"github.com/felixge/httpsnoop"
	"rokett.me/atom/internal/authentication"
	"rokett.me/atom/internal/uuid"
)

func logMetrics(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Call the httpsnoop.CaptureMetrics() function, passing in the next handler in
		// the chain along with the existing http.ResponseWriter and http.Request.
		metrics := httpsnoop.CaptureMetrics(next, w, r)

		// We don't really care about specific status codes, just the type
		status := strconv.Itoa(metrics.Code)
		switch status[0:1] {
		case "2":
			status = "2xx"
		case "3":
			status = "3xx"
		case "4":
			status = "4xx"
		case "5":
			status = "5xx"
		}

		httpRequestsTotal.WithLabelValues(r.URL.Path, r.Method, status).Inc()
		httpRequestDurationSeconds.WithLabelValues(r.URL.Path, r.Method).Observe(metrics.Duration.Seconds())
	})
}

func getClientIP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Retrieve the client IP from the remote address of the request
		clientIP, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			APIResponse := Response{
				Message: "unable to retrieve 'RemoteAddr' HTTP header",
				Error:   err.Error(),
			}

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		// If the request has passed through a proxy, the RemoteAddr may actually end up being the IP of the proxy.
		// Depending on how the proxy is configured, the X-Forwarded-For header may have been inserted to show the REAL client IP.
		// If it's there, we'll grab it and use it.
		// X-Forwarded-For could be a comma separated list, so we'll split it and then select the first entry which is the real client IP.
		// See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For for more info.
		if r.Header.Get("X-Forwarded-For") != "" {
			xForwardedFor := strings.Split(r.Header.Get("X-Forwarded-For"), ",")
			clientIP = strings.TrimSpace(xForwardedFor[0])
		}

		ctx := context.WithValue(r.Context(), clientIPCtxKey, clientIP)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func setTraceID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		traceID, err := uuid.NewV4()
		if err != nil {
			APIResponse := Response{
				Message: "unable to generate trace ID",
				Error:   err.Error(),
			}

			APIResponse.Send(http.StatusInternalServerError, w)

			return
		}

		ctx := context.WithValue(r.Context(), traceIDCtxKey, traceID.String())

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (app *application) validateAuth(reqRole string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// The clientIP is included in every log entry and in some metrics for later analysis
			clientIP := r.Context().Value(clientIPCtxKey).(string)

			// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
			traceID := r.Context().Value(traceIDCtxKey).(string)

			APIResponse := Response{
				TraceID: traceID,
			}

			req := fmt.Sprintf("%s %s", r.Method, r.URL)

			var missingToken bool
			sessionTokenPresent := true

			token, err := r.Cookie("session_token")
			if err != nil {
				if err != http.ErrNoCookie {
					app.logger.Error("error retrieving session_token cookie",
						"error", err,
						"client_ip", clientIP,
						"trace_id", traceID,
						"request", req,
					)

					APIResponse.Message = "error retrieving session_token cookie"
					APIResponse.Send(http.StatusBadRequest, w)
					return
				}

				missingToken = true
				sessionTokenPresent = false
			}

			var tokenValue string

			if r.Header.Get("vnd-atom-application-token") != "" {
				tokenValue = r.Header.Get("vnd-atom-application-token")

				// If we have a session token and a header token, and the values do not match, we need to error
				if !missingToken && token.Value != tokenValue {
					app.logger.Error("have both 'session_token' cookie and 'vnd-atom-application-token' header which do not match",
						"error", err,
						"client_ip", clientIP,
						"trace_id", traceID,
						"request", req,
					)

					APIResponse.Message = "have both 'session_token' cookie and 'vnd-atom-application-token' header which do not match"
					APIResponse.Send(http.StatusBadRequest, w)
					return
				}

				missingToken = false
			}

			if missingToken {
				app.logger.Error("token missing",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
					"request", req,
				)

				APIResponse.Send(http.StatusUnauthorized, w)
				return
			}

			if tokenValue == "" {
				tokenValue = token.Value
			}

			valid, user, err := authentication.IsTokenValid(tokenValue, app.db)
			if err != nil {
				app.logger.Error("unable to validate that session token is valid",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
					"request", req,
				)

				APIResponse.Message = "unable to validate that session token is valid"
				APIResponse.Send(http.StatusInternalServerError, w)
				return
			}
			if !valid {
				APIResponse.Message = "token has expired"
				APIResponse.Send(http.StatusUnauthorized, w)
				return
			}

			role := user.Role

			if sessionTokenPresent {
				ldapConn, err := authentication.ConnectLdap(app.config.Auth.Hosts, app.config.Auth.Port)
				if err != nil {
					app.logger.Error("unable to connect to LDAP directory",
						"error", err,
						"client_ip", clientIP,
						"trace_id", traceID,
						"request", req,
					)

					APIResponse.Message = "unable to connect to LDAP directory"
					APIResponse.Send(http.StatusInternalServerError, w)
					return
				}

				err = ldapConn.Bind(app.config.Auth.BindDN, app.config.Auth.BindPW)
				if err != nil {
					app.logger.Error("error during LDAP bind",
						"error", err,
						"client_ip", clientIP,
						"trace_id", traceID,
						"request", req,
					)

					APIResponse.Message = "error during LDAP bind"
					APIResponse.Send(http.StatusInternalServerError, w)
					return
				}

				enabled, err := authentication.IsUserEnabled(tokenValue, app.db, ldapConn, app.config.Auth.BaseDN)
				if err != nil {
					app.logger.Error("unable to validate that user is enabled",
						"error", err,
						"client_ip", clientIP,
						"trace_id", traceID,
						"request", req,
					)

					APIResponse.Message = "unable to validate that user is enabled"
					APIResponse.Send(http.StatusInternalServerError, w)
					return
				}
				if !enabled {
					APIResponse.Message = "user account is disabled"
					APIResponse.Send(http.StatusUnauthorized, w)
					return
				}

				role, err = authentication.UpdateUserRole(tokenValue, app.db, ldapConn, app.config.Auth.BaseDN)
				if err != nil {
					app.logger.Error("unable to update user's role",
						"error", err,
						"client_ip", clientIP,
						"trace_id", traceID,
						"request", req,
					)

					APIResponse.Message = "unable to update user's role"
					APIResponse.Send(http.StatusInternalServerError, w)
					return
				}
			}

			reqRoles := strings.Split(reqRole, ",")
			if !sliceContains(reqRoles, role) {
				app.logger.Error("user is unauthorised to perform this action",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
					"request", req,
					"username", user.Username,
				)

				APIResponse.Message = "unauthorised for this action"
				APIResponse.Send(http.StatusUnauthorized, w)
				return
			}

			ctx := context.WithValue(r.Context(), roleCtxKey, role)
			ctx = context.WithValue(ctx, usernameCtxKey, user.Username)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func (app *application) validateInteractiveAuth(reqRole string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// The clientIP is included in every log entry and in some metrics for later analysis
			clientIP := r.Context().Value(clientIPCtxKey).(string)

			// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
			traceID := r.Context().Value(traceIDCtxKey).(string)

			token, err := r.Cookie("session_token")
			if err != nil {
				app.logger.Error("unable to retrieve session token",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			valid, user, err := authentication.IsTokenValid(token.Value, app.db)
			if err != nil {
				app.logger.Error("unable to check session token is valid",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}
			if !valid {
				app.logger.Error("invalid session token",
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			ldapConn, err := authentication.ConnectLdap(app.config.Auth.Hosts, app.config.Auth.Port)
			if err != nil {
				app.logger.Error("unable to connect to LDAP directory",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			err = ldapConn.Bind(app.config.Auth.BindDN, app.config.Auth.BindPW)
			if err != nil {
				app.logger.Error("error during LDAP bind",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			enabled, err := authentication.IsUserEnabled(token.Value, app.db, ldapConn, app.config.Auth.BaseDN)
			if err != nil {
				app.logger.Error("unable to check user is enabled in ldap directory",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}
			if !enabled {
				app.logger.Error("user account is not enabled in ldap directory",
					"client_ip", clientIP,
					"trace_id", traceID,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			role, err := authentication.UpdateUserRole(token.Value, app.db, ldapConn, app.config.Auth.BaseDN)
			if err != nil {
				app.logger.Error("unable to update user's role",
					"error", err,
					"client_ip", clientIP,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			reqRoles := strings.Split(reqRole, ",")
			if !sliceContains(reqRoles, role) {
				app.logger.Error("user is unauthorised to perform this action",
					"error", err,
					"client_ip", clientIP,
					"trace_id", traceID,
					"username", user.Username,
				)

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			ctx := context.WithValue(r.Context(), roleCtxKey, role)
			ctx = context.WithValue(ctx, usernameCtxKey, user.Username)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func cacheControl(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.String(), "static") {
			w.Header().Add("Cache-Control", "public, max-age=31536000")
		}

		next.ServeHTTP(w, r)
	})
}
