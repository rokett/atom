package main

import (
	"encoding/json"
	"os"

	"github.com/nats-io/nats.go"
	model "rokett.me/atom/internal"
)

func (app *application) setupSubscriptions() (subscriptions []*nats.Subscription) {
	// Listen for requests from workers to send them the list of allowed scripts
	allowedScriptsReqSub, err := app.nc.QueueSubscribe("allowed_scripts:request", "server", func(m *nats.Msg) {
		app.logger.Debug(
			"publishing allowed scripts",
			"allowed_scripts", app.config.Server.AllowedScripts,
		)

		pl, err := json.Marshal(app.config.Server.AllowedScripts)
		if err != nil {
			app.logger.Error(
				"converting allowed scripts to json",
				"error", err,
			)
			return
		}

		app.nc.Publish(m.Reply, pl)
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "allowed_scripts:request",
			"error", err,
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, allowedScriptsReqSub)
	app.logger.Info(
		"subscribed",
		"nats_subject", "allowed_scripts:request",
	)

	// Listen for requests to check maintenance mode status
	mmSub, err := app.nc.Subscribe("check_maintenance_mode_status", func(m *nats.Msg) {
		enabled, err := app.savedConfig.IsMaintenanceModeEnabled()
		if err != nil {
			app.logger.Error("unable to check if maintenance mode is enabled",
				"nats_subscription", "check_maintenance_mode_status",
				"error", err,
			)
			return
		}

		if enabled {
			m.Respond([]byte("on"))
			return
		}

		m.Respond([]byte("off"))
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "check_maintenance_mode_status",
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, mmSub)
	app.logger.Info("subscribed",
		"nats_subject", "check_maintenance_mode_status",
	)

	// Listen for worker registration events
	wrSub, err := app.nc.QueueSubscribe("worker:register", "server", func(m *nats.Msg) {
		var worker model.Worker

		err := json.Unmarshal(m.Data, &worker)
		if err != nil {
			app.logger.Error(
				"decoding worker payload",
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		err = app.registerWorker(worker)
		if err != nil {
			app.logger.Error(
				"registering worker",
				"worker_name", worker.Name,
				"worker_server_name", worker.ServerName,
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		app.nc.Publish(m.Reply, []byte("true"))
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "worker:register",
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, wrSub)
	app.logger.Info("subscribed",
		"nats_subject", "worker:register",
	)

	// Listen for worker deregistration events
	wdSub, err := app.nc.QueueSubscribe("worker:deregister", "server", func(m *nats.Msg) {
		var worker model.Worker

		err := json.Unmarshal(m.Data, &worker)
		if err != nil {
			app.logger.Error(
				"decoding worker payload",
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		err = app.deregisterWorker(worker)
		if err != nil {
			app.logger.Error(
				"deregistering worker",
				"worker_name", worker.Name,
				"worker_server_name", worker.ServerName,
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		app.nc.Publish(m.Reply, []byte("true"))
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "worker:deregister",
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, wdSub)
	app.logger.Info("subscribed",
		"nats_subject", "worker:deregister",
	)

	// Listen for worker status update events
	wsSub, err := app.nc.QueueSubscribe("worker:status_update", "server", func(m *nats.Msg) {
		var worker model.Worker

		err := json.Unmarshal(m.Data, &worker)
		if err != nil {
			app.logger.Error(
				"decoding worker payload",
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		err = app.updateWorkerStatus(worker)
		if err != nil {
			app.logger.Error("updating worker status",
				"worker_name", worker.Name,
				"worker_server_name", worker.ServerName,
				"worker_status", worker.Status,
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		app.nc.Publish(m.Reply, []byte("true"))
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "worker:status_update",
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, wsSub)
	app.logger.Info(
		"subscribed",
		"nats_subject", "worker:status_update",
	)

	// Listen for worker job execution updates
	jeSub, err := app.nc.QueueSubscribe("job:execution_update", "server", func(m *nats.Msg) {
		var job model.JobExecution

		err := json.Unmarshal(m.Data, &job)
		if err != nil {
			app.logger.Error(
				"decoding job payload",
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		switch job.Status {
		case "RECEIVED_BY_WORKER":
			jobsQueued.Dec()
		case "STARTING":
			jobsRunning.Inc()
		case "ERROR_STARTING", "ERROR", "CANCELLED_TIMEOUT", "CANCELLED_MANUAL", "COMPLETED_WITH_WARNINGS", "COMPLETED":
			jobsRunning.Dec()
		}

		// Log job errors
		loggableStatus := map[string]bool{
			"UNAUTHORISED_SCRIPT": true,
			"ERROR_STARTING":      true,
			"EXPIRED":             true,
			"CANCELLED_TIMEOUT":   true,
		}

		if loggableStatus[job.Status] {
			app.logger.Error("problem during job execution",
				"status", job.Status,
				"job_id", job.ID,
				"started_at", job.StartedAt,
				"finished_at", job.FinishedAt,
				"worker_name", job.WorkerName,
				"worker_ip", job.WorkerIP,
				"worker_server_name", job.WorkerServerName,
				"worker_os", job.WorkerOs,
			)
		}

		err = app.updateJobExecution(job)
		if err != nil {
			app.logger.Error("job execution update",
				"job_id", job.ID,
				"worker_name", job.WorkerName,
				"worker_server_name", job.WorkerServerName,
				"error", err,
			)

			app.nc.Publish(m.Reply, []byte("false"))
			return
		}

		app.nc.Publish(m.Reply, []byte("true"))
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "job:execution_update",
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, jeSub)
	app.logger.Info(
		"subscribed",
		"nats_subject", "job:execution_update",
	)

	// Listen for worker heartbeat events
	heartbeatSub, err := app.nc.QueueSubscribe("worker:heartbeat", "server", func(m *nats.Msg) {
		err := app.workers.UpdateHeartbeat(string(m.Data))
		if err != nil {
			app.logger.Error(
				"updating worker heartbeat timestamp",
				"error", err,
			)
		}
	})
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "worker:heartbeat",
		)
		os.Exit(1)
	}
	if err != nil {
		app.logger.Error(
			"unable to setup subscription",
			"nats_subscription", "worker:heartbeat",
		)
		os.Exit(1)
	}
	subscriptions = append(subscriptions, heartbeatSub)
	app.logger.Info(
		"subscribed",
		"nats_subject", "worker:heartbeat",
	)

	return subscriptions
}
