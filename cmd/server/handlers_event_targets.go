package main

import (
	"net/http"
	"net/url"
	"strconv"

	"github.com/go-chi/chi/v5"
	model "rokett.me/atom/internal"
)

func (app *application) showEventTargets(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	eventTargets, err := app.eventTargets.List()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "event_targets.page.html", &templateData{
		ActivePage:      "event_targets",
		EventTargets:    eventTargets,
		Flash:           app.session.PopString(r, "flash"),
		IsAuthenticated: true,
		UserRole:        role,
	})
}

func (app *application) showEventTargetUpdatePage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	eventTarget, err := app.eventTargets.Get(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	form := url.Values{}
	form.Set("id", strconv.Itoa(id))
	form.Set("name", eventTarget.Name)
	form.Set("target", eventTarget.Target)
	form.Set("event_type", eventTarget.EventType)

	app.render(w, r, "event_target_detail.page.html", &templateData{
		Action:          "view",
		ActivePage:      "event_target_detail",
		FormData:        form,
		IsAuthenticated: true,
		UserRole:        role,
	})
}

func (app *application) showAddEventTargetPage(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	app.render(w, r, "event_target_detail.page.html", &templateData{
		Action:          "add",
		ActivePage:      "event_target_detail",
		IsAuthenticated: true,
		UserRole:        role,
	})
}

func (app *application) addEventTarget(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	et := model.EventTarget{
		Name:      r.PostForm.Get("name"),
		Target:    r.PostForm.Get("target"),
		EventType: r.PostForm.Get("event_type"),
	}

	ve, err := et.Validate()
	if err != nil {
		validationErrors := make(map[string]string)
		for _, v := range ve {
			validationErrors[v.Parameter] = v.Error
		}

		app.render(w, r, "event_target_detail.page.html", &templateData{
			Action:          "add",
			ActivePage:      "event_target_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			FormErrors:      validationErrors,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	et.ID, err = et.Upsert(app.db)
	if err != nil {
		app.logger.Error("error adding event target",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
		)

		app.render(w, r, "event_target_detail.page.html", &templateData{
			Action:          "add",
			ActivePage:      "event_target_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	http.Redirect(w, r, "/admin/event_targets", http.StatusSeeOther)
}

func (app *application) updateEventTarget(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	if r.PostForm.Get("action") == "delete" {
		app.render(w, r, "event_target_detail.page.html", &templateData{
			Action:          "delete",
			ActivePage:      "event_target_detail",
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	http.Redirect(w, r, "/admin/event_targets", http.StatusSeeOther)
}

func (app *application) deleteEventTarget(w http.ResponseWriter, r *http.Request) {
	// The role is needed to tweak the UI as appropriate for the users permission level
	role := r.Context().Value(roleCtxKey).(string)

	// The traceID is included in every log entry, and in HTTP responses, to allow for correlation of logs
	traceID := r.Context().Value(traceIDCtxKey).(string)

	// The clientIP is included in every log entry and in some metrics for later analysis
	clientIP := r.Context().Value(clientIPCtxKey).(string)

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	id, err := strconv.ParseInt(r.PostForm.Get("id"), 10, 64)
	if err != nil {
		app.serverError(w, err)
		return
	}

	_, err = app.db.Exec("DELETE FROM eventTargets WHERE id = @p1;", id)
	if err != nil {
		app.logger.Error("error removing event target",
			"error", err,
			"client_ip", clientIP,
			"trace_id", traceID,
			"event_target_id", id,
		)

		app.render(w, r, "event_target_detail.page.html", &templateData{
			Action:          "view",
			ActivePage:      "event_target_detail",
			ErrorMessage:    err.Error(),
			FormData:        r.PostForm,
			IsAuthenticated: true,
			UserRole:        role,
		})

		return
	}

	http.Redirect(w, r, "/admin/event_targets", http.StatusSeeOther)
}
