if (document.getElementById("schedules")) {
    Array.from(document.getElementsByClassName("schedule-row")).forEach(
        function(element, index, array) {
            element.addEventListener("click", function(event) {
                window.location.href = "/schedules/" + element.dataset.id
            })
        }
    );
}
