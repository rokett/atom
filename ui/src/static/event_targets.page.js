if (document.getElementById("event_targets")) {
    Array.from(document.getElementsByClassName("event_target-row")).forEach(
        function(element, index, array) {
            element.addEventListener("click", function(event) {
                window.location.href = "/admin/event_target/" + element.dataset.id
            })
        }
    );
}
