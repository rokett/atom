// Fade and remove flash message
if (document.getElementById("flash")) {
    if (document.getElementById("flash").parentElement.id !== "login") {
        fadeFlash()
    }
}

async function fadeFlash() {
    await wait(2000)
    document.getElementById("flash").classList.add("transition-opacity", "duration-1000", "ease-out", "opacity-0")
    await wait(950)
    document.getElementById("flash").classList.add("hidden")
}

// Helper functions
async function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

if (document.getElementById("toggle_filter_help")) {
    document.getElementById("toggle_filter_help").addEventListener("click", function(event) {
        document.getElementById("filter_help").classList.toggle("hidden")
    })
}
