if (document.getElementById("jobs")) {
    document.getElementById("jobs-timeframe").addEventListener("click", function(event) {
        toggleDatePicker()
    })

    Array.from(document.getElementsByClassName("job-row")).forEach(
        function(element, index, array) {
            element.addEventListener("click", function(event) {
                window.location.href = "/jobs/" + element.dataset.id
            })
        }
    )

    let job_timerange_label = document.getElementById("job_timerange_label")
    let filter = document.getElementById("filter")
    Array.from(document.getElementById("relative_time_ranges").getElementsByTagName("button")).forEach(
        function(element, index, array) {
            element.addEventListener("click", function(event) {
                let vals = filter.value.split(" ")

                // Remove any empty values; there will always be at least one empty value on the first run due to the way split works.
                // This filter returns the value itself, but if the value is undefined, null or empty it effectively evals to false, so that element is filtered out.
                vals = vals.filter((a) => a)

                // Remove 'from' if it exists; i.e we keep all elements in the array which DO NOT include the work 'from:'
                vals = vals.filter(val => val.includes("from:") == false)

                // Remove 'to' if it exists; i.e we keep all elements in the array which DO NOT include the work 'to:'
                vals = vals.filter(val => val.includes("to:") == false)

                let found = false
                vals.forEach((value, index, a) => {
                    if (value.includes("relative")) {
                        found = true
                        a[index] = "relative:" + event.target.textContent.replace(/ /gi, "_").toLowerCase()
                    }
                })

                if (found === false) {
                    vals.push("relative:" + event.target.textContent.replace(/ /gi, "_").toLowerCase())
                }

                filter.value = vals.join(" ")

                job_timerange_label.textContent = event.target.textContent

                toggleDatePicker()
            })
        }
    )

    document.getElementById("use_absolute_timerange").addEventListener("click", function(event) {
        let vals = filter.value.split(" ")

        // Remove any empty values; there will always be at least one empty value on the first run due to the way split works.
        // This filter returns the value itself, but if the value is undefined, null or empty it effectively evals to false, so that element is filtered out.
        vals = vals.filter((a) => a)

        // Remove relative if it exists; i.e we keep all elements in the array which DO NOT include the work 'relative:'
        vals = vals.filter(val => val.includes("relative:") == false)

        let foundFrom = false
        let foundTo = false
        vals.forEach((value, index, a) => {
            if (value.includes("from")) {
                foundFrom = true
                a[index] = "from:" + document.getElementById("from").value
            }

            if (value.includes("to")) {
                foundTo = true
                a[index] = "to:" + document.getElementById("to").value
            }
        })

        if (foundFrom === false && document.getElementById("from").value !== "") {
            vals.push("from:" + document.getElementById("from").value)
        }
        if (foundTo === false && document.getElementById("to").value !== "") {
            vals.push("to:" + document.getElementById("to").value)
        }

        filter.value = vals.join(" ")

        job_timerange_label.textContent = "from/to"

        toggleDatePicker()
    })
}

function toggleDatePicker() {
    document.getElementById("hiding-date-picker").classList.toggle("hidden")
    document.getElementById("showing-date-picker").classList.toggle("hidden")
    document.getElementById("jobs-timeframe-menu").classList.toggle("hidden")
}
