DECLARE @c NVARCHAR(50)

-- eventTargets table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'eventTargets'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'eventTargets')
        EXEC('ALTER TABLE eventTargets DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'eventTargets' AND COLUMN_NAME = 'created_at')
    ALTER TABLE eventTargets ADD CONSTRAINT DF_eventTargets_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'eventTargets'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'eventTargets')
        EXEC('ALTER TABLE eventTargets DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'eventTargets' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE eventTargets ADD CONSTRAINT DF_eventTargets_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'eventTargets_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER eventTargets_updated_at ON eventTargets AFTER UPDATE AS
            UPDATE eventTargets
            SET updated_at = GETUTCDATE()
            FROM eventTargets INNER JOIN deleted d
            ON eventTargets.id = d.id;');

-- jobExecutions table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'jobExecutions'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'jobExecutions')
        EXEC('ALTER TABLE jobExecutions DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'created_at')
    ALTER TABLE jobExecutions ADD CONSTRAINT DF_jobExecutions_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'jobExecutions'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'jobExecutions')
        EXEC('ALTER TABLE jobExecutions DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE jobExecutions ADD CONSTRAINT DF_jobExecutions_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobExecutions_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER jobExecutions_updated_at ON jobExecutions AFTER UPDATE AS
            UPDATE jobExecutions
            SET updated_at = GETUTCDATE()
            FROM jobExecutions INNER JOIN deleted d
            ON jobExecutions.id = d.id;');

-- jobs table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'jobs'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'jobs')
        EXEC('ALTER TABLE jobs DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'created_at')
    ALTER TABLE jobs ADD CONSTRAINT DF_jobs_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'jobs'))
   BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'jobs')
        EXEC('ALTER TABLE jobs DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE jobs ADD CONSTRAINT DF_jobs_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobs_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER jobs_updated_at ON jobs AFTER UPDATE AS
            UPDATE jobs
            SET updated_at = GETUTCDATE()
            FROM jobs INNER JOIN deleted d
            ON jobs.id = d.id;');

-- roles table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'roles'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'roles')
        EXEC('ALTER TABLE roles DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'roles' AND COLUMN_NAME = 'created_at')
    ALTER TABLE roles ADD CONSTRAINT DF_roles_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'roles'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'roles')
        EXEC('ALTER TABLE roles DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'roles' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE roles ADD CONSTRAINT DF_roles_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'roles_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER roles_updated_at ON roles AFTER UPDATE AS
            UPDATE roles
            SET updated_at = GETUTCDATE()
            FROM roles INNER JOIN deleted d
            ON roles.id = d.id;');

-- scripts table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'scripts'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'scripts')
        EXEC('ALTER TABLE scripts DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'scripts' AND COLUMN_NAME = 'created_at')
    ALTER TABLE scripts ADD CONSTRAINT DF_Scripts_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'scripts'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'scripts')
        EXEC('ALTER TABLE scripts DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'scripts' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE scripts ADD CONSTRAINT DF_Scripts_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'scripts_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER scripts_updated_at ON scripts AFTER UPDATE AS
            UPDATE scripts
            SET updated_at = GETUTCDATE()
            FROM scripts INNER JOIN deleted d
            ON scripts.id = d.id;');

-- users table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'users'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'users')
        EXEC('ALTER TABLE users DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'users' AND COLUMN_NAME = 'created_at')
    ALTER TABLE users ADD CONSTRAINT DF_users_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'users'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'users')
        EXEC('ALTER TABLE users DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'users' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE users ADD CONSTRAINT DF_users_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'users_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER users_updated_at ON users AFTER UPDATE AS
            UPDATE users
            SET updated_at = GETUTCDATE()
            FROM users INNER JOIN deleted d
            ON users.id = d.id;');

-- workers table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'workers'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'created_at') AND (sys.objects.name = N'workers')
        EXEC('ALTER TABLE workers DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'workers' AND COLUMN_NAME = 'created_at')
    ALTER TABLE workers ADD CONSTRAINT DF_workers_CreatedAt DEFAULT GETUTCDATE() FOR created_at;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'workers'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'updated_at') AND (sys.objects.name = N'workers')
        EXEC('ALTER TABLE workers DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'workers' AND COLUMN_NAME = 'updated_at')
    ALTER TABLE workers ADD CONSTRAINT DF_workers_UpdatedAt DEFAULT GETUTCDATE() FOR updated_at;

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'workers_updated_at' AND type = 'TR')
    EXECUTE ('ALTER TRIGGER workers_updated_at ON workers AFTER UPDATE AS
            UPDATE workers
            SET updated_at = GETUTCDATE()
            FROM workers INNER JOIN deleted d
            ON workers.id = d.id;');
