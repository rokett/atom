IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'scripts')
	CREATE TABLE scripts (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        script NVARCHAR(25) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'scripts_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER scripts_updated_at ON scripts AFTER UPDATE AS
		UPDATE scripts
		SET updated_at = GETDATE()
		FROM scripts INNER JOIN deleted d
		ON scripts.id = d.id;');
