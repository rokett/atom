DECLARE @c NVARCHAR(50)

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id WHERE sys.columns.column_id = sys.default_constraints.parent_column_id AND sys.columns.name = 'staleness_failures')
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id WHERE sys.columns.column_id = sys.default_constraints.parent_column_id AND sys.columns.name = 'staleness_failures'
        EXEC('ALTER TABLE workers DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'workers' AND COLUMN_NAME = 'staleness_failures')
    ALTER TABLE workers DROP COLUMN staleness_failures;
