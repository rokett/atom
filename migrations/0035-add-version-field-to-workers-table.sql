IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'workers' AND COLUMN_NAME = 'version')
    ALTER TABLE workers ADD version NVARCHAR(11) NOT NULL DEFAULT '';
