DECLARE @c NVARCHAR(200)

-- jobExecutions table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'worker_server_name') AND (sys.objects.name = N'jobExecutions'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'worker_server_name') AND (sys.objects.name = N'jobExecutions')
        EXEC('ALTER TABLE jobExecutions DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'worker_server_name')
    ALTER TABLE jobExecutions ADD CONSTRAINT DF_JobExecutions_WorkerServerName DEFAULT '' FOR worker_server_name;

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'worker_os') AND (sys.objects.name = N'jobExecutions'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'worker_os') AND (sys.objects.name = N'jobExecutions')
        EXEC('ALTER TABLE jobExecutions DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'worker_os')
    ALTER TABLE jobExecutions ADD CONSTRAINT DF_JobExecutions_WorkerOS DEFAULT '' FOR worker_os;

-- jobs table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'cronEntryID') AND (sys.objects.name = N'jobs'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'cronEntryID') AND (sys.objects.name = N'jobs')
        EXEC('ALTER TABLE jobs DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'cronEntryID')
    ALTER TABLE jobs ADD CONSTRAINT DF_Jobs_CronEntryID DEFAULT 0 FOR cronEntryID;

-- users table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'group_hash') AND (sys.objects.name = N'users'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'group_hash') AND (sys.objects.name = N'users')
        EXEC('ALTER TABLE users DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'users' AND COLUMN_NAME = 'group_hash')
    ALTER TABLE users ADD CONSTRAINT DF_Workers_GroupHash DEFAULT '' FOR group_hash;

-- workers table
IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'groups') AND (sys.objects.name = N'workers'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'groups') AND (sys.objects.name = N'workers')
        EXEC('ALTER TABLE workers DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'workers' AND COLUMN_NAME = 'groups')
    ALTER TABLE workers ADD CONSTRAINT DF_Workers_Groups DEFAULT '' FOR groups;
