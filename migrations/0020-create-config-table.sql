IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'config')
	CREATE TABLE config (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        cleanup_stale_workers BIT NOT NULL
            CONSTRAINT DF_config_CleanupStaleWorkers DEFAULT 0,
        created_at DATETIME2(3) NOT NULL
            CONSTRAINT DF_config_CreatedAt DEFAULT GETUTCDATE(),
        updated_at DATETIME2(3) NOT NULL
            CONSTRAINT DF_config_UpdatedAt DEFAULT GETUTCDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'config_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER config_updated_at ON config AFTER UPDATE AS
		UPDATE config
		SET updated_at = GETUTCDATE()
		FROM config INNER JOIN deleted d
		ON config.id = d.id;');
