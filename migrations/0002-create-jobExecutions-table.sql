IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'jobExecutions')
	CREATE TABLE jobExecutions (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        script NVARCHAR(25) NOT NULL,
        parameters NVARCHAR(1000),
        scope NVARCHAR(1000) NOT NULL,
        execution_id NVARCHAR(36) NOT NULL,
        [status] NVARCHAR(30) NOT NULL,
        execution_time_ms INT,
        [log] NVARCHAR(MAX),
        execution_requested_by NVARCHAR(50) NOT NULL,
        client_ip NVARCHAR(15),
        worker_name NVARCHAR(100),
        worker_ip NVARCHAR(15),
        trace_id NVARCHAR(36),
        started_at DATETIME2(3),
        finished_at DATETIME2(3),
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobExecutions_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER jobs_updated_at ON jobExecutions AFTER UPDATE AS
		UPDATE jobExecutions
		SET updated_at = GETDATE()
		FROM jobExecutions INNER JOIN deleted d
		ON jobExecutions.id = d.id;');
