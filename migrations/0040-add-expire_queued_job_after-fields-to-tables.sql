IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'expire_queued_job_after_duration')
    ALTER TABLE jobs ADD expire_queued_job_after_duration NVARCHAR(20) NOT NULL DEFAULT '';

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'expire_queued_job_after_timestamp')
    ALTER TABLE jobExecutions ADD expire_queued_job_after_timestamp DATETIME2(3) NOT NULL DEFAULT GETDATE();

IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'expire_queued_job_after_duration')
    ALTER TABLE jobExecutions ADD expire_queued_job_after_duration NVARCHAR(20) NOT NULL DEFAULT '';
