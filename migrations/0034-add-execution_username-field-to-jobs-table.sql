IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'execution_username')
    ALTER TABLE jobs ADD execution_username NVARCHAR(200) NOT NULL DEFAULT '';
