IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobExecutions' AND COLUMN_NAME = 'executor')
    UPDATE jobExecutions SET [executor] = 'powershell' WHERE [executor] = '';

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'executor')
    UPDATE jobs SET [executor] = 'powershell' WHERE [executor] = '';
