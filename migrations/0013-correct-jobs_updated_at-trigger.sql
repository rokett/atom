IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobs_updated_at' AND type = 'TR')
    EXECUTE ('DROP TRIGGER jobs_updated_at;');

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobExecutions_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER jobExecutions_updated_at ON jobExecutions AFTER UPDATE AS
		UPDATE jobExecutions
		SET updated_at = GETDATE()
		FROM jobExecutions INNER JOIN deleted d
		ON jobExecutions.id = d.id;');

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobs_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER jobs_updated_at ON jobs AFTER UPDATE AS
		UPDATE jobs
		SET updated_at = GETDATE()
		FROM jobs INNER JOIN deleted d
		ON jobs.id = d.id;');
