IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'workers')
	CREATE TABLE workers (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [name] NVARCHAR(200) NOT NULL,
        ip NVARCHAR(15) NOT NULL,
        os NVARCHAR(50) NOT NULL,
        server_name NVARCHAR(30) NOT NULL,
        [status] NVARCHAR(7) NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'workers_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER workers_updated_at ON workers AFTER UPDATE AS
		UPDATE workers
		SET updated_at = GETDATE()
		FROM workers INNER JOIN deleted d
		ON workers.id = d.id;');
