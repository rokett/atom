IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'roles')
	CREATE TABLE roles (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        [group] NVARCHAR(200) NOT NULL,
        [role] NVARCHAR(10) NOT NULL,
        preference int NOT NULL,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'roles_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER roles_updated_at ON roles AFTER UPDATE AS
		UPDATE roles
		SET updated_at = GETDATE()
		FROM roles INNER JOIN deleted d
		ON roles.id = d.id;');
