IF NOT EXISTS (SELECT 1 FROM config WHERE cleanup_stale_workers = 0 OR cleanup_stale_workers = 1)
    INSERT INTO config (cleanup_stale_workers) VALUES (0);
