DECLARE @c NVARCHAR(200)

IF EXISTS (SELECT sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'is_running') AND (sys.objects.name = N'jobs'))
    BEGIN
        SELECT @c = sys.default_constraints.name FROM sys.default_constraints INNER JOIN sys.columns ON sys.default_constraints.parent_object_id = sys.columns.object_id AND sys.default_constraints.parent_column_id = sys.columns.column_id INNER JOIN sys.objects ON sys.default_constraints.parent_object_id = sys.objects.object_id WHERE (sys.columns.name = N'is_running') AND (sys.objects.name = N'jobs')
        EXEC('ALTER TABLE jobs DROP CONSTRAINT ' + @c)
    END

IF EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'jobs' AND COLUMN_NAME = 'is_running')
    ALTER TABLE jobs DROP COLUMN is_running;
