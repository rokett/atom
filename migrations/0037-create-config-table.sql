IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'config')
	CREATE TABLE config (
        [option] NVARCHAR(100) PRIMARY KEY NOT NULL,
        [value] NVARCHAR(100) NOT NULL,
    );
