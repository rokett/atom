IF NOT EXISTS (SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME = 'jobs')
	CREATE TABLE jobs (
        id INT PRIMARY KEY IDENTITY NOT NULL,
        script NVARCHAR(25) NOT NULL,
        parameters NVARCHAR(1000),
        scope NVARCHAR(1000) NOT NULL,
        schedule NVARCHAR(150) NOT NULL,
        cronEntryID INT DEFAULT 0,
        created_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
        updated_at DATETIME2(3) NOT NULL DEFAULT GETDATE(),
    );

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'jobs_updated_at' AND type = 'TR')
	EXECUTE ('CREATE TRIGGER jobs_updated_at ON jobs AFTER UPDATE AS
		UPDATE jobs
		SET updated_at = GETDATE()
		FROM jobs INNER JOIN deleted d
		ON jobs.id = d.id;');
