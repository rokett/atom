package atom

import (
	"embed"
)

// Dist represents the website dist folder
//
//go:embed ui/dist
var UI embed.FS

// Migrations represents the migrations folder
//
//go:embed migrations
var Migrations embed.FS
