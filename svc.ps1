[cmdletbinding()]
    Param(
        [Parameter(Mandatory = $True)]
        [string]
        $Name,

        [Parameter()]
        [string]
        $VaultToken
    )

#Start-Transcript -Path "$PSScriptRoot\$trName"
#write-host $VaultToken
Write-Host $ENV:ATOM_JOB_ID
Write-Host $ENV:ATOM_JOB_REQUESTED_BY
Write-Host $ENV:ATOM_JOB_EXECUTION_USERNAME
#Get-Service -Name $Name

Write-Host "writing host"

Start-Sleep -Seconds 20

Write-Verbose "writing verbose" -Verbose

#Write-Error "Writing error 1" -ErrorAction Continue

#$DebugPreference = "Continue"
#Write-Debug "writing debug"

#Write-Information "writing information" -InformationAction Continue

#Write-Host "Press any key to continue ....."
#$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
#Write-Host "done that then - sleep now"
#Start-Sleep -Seconds 5

#Write-Warning "writing warning" -WarningAction Continue

#Write-Error "Writing error 2" -ErrorAction Continue

#Throw "error yo"
#Write-Host "finished"

<#
Write-Host "1"
start-sleep -seconds 5
Write-Host "2"
start-sleep -seconds 5
Write-Host "3"
start-sleep -seconds 5
Write-Host "4"
start-sleep -seconds 5
Write-Host "5"
start-sleep -seconds 5
Write-Host "6"
start-sleep -seconds 5
Write-Host "7"
start-sleep -seconds 5
Write-Host "8"
start-sleep -seconds 5
#>
#Stop-Transcript
