async function build() {
    const fs = require('fs')
    const fse = require('fs-extra')
    const crypto = require('crypto')
    const {minify} = require("terser")
    const {replaceInFileSync} = await import('replace-in-file')

    // Initialise dist folder
    try {
        if (fs.existsSync('ui/dist')) {
            fs.rmSync('ui/dist', {
                recursive: true,
                force: true
            })
        }

        fs.mkdirSync('ui/dist')
        fs.mkdirSync('ui/dist/static')
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    // Copy source files to dist
    try {
        fse.copySync('ui/src/html', 'ui/dist/html')
        fs.copyFileSync('ui/src/static/android-chrome-192x192.png', 'ui/dist/static/android-chrome-192x192.png')
        fs.copyFileSync('ui/src/static/android-chrome-512x512.png', 'ui/dist/static/android-chrome-512x512.png')
        fs.copyFileSync('ui/src/apple-touch-icon.png', 'ui/dist/apple-touch-icon.png')
        fs.copyFileSync('ui/src/favicon-16x16.png', 'ui/dist/favicon-16x16.png')
        fs.copyFileSync('ui/src/favicon-32x32.png', 'ui/dist/favicon-32x32.png')
        fs.copyFileSync('ui/src/favicon.ico', 'ui/dist/favicon.ico')
        fs.copyFileSync('ui/src/site.webmanifest', 'ui/dist/site.webmanifest')
        fs.copyFileSync('ui/src/html/api.page.html', 'ui/dist/html/api.page.html')
        fs.copyFileSync('node_modules/rapidoc/dist/rapidoc-min.js', 'ui/dist/static/rapidoc-min.js')
        fs.copyFileSync('docs/api.yaml', 'ui/dist/static/api.yaml')
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    var jsPaths = [
        'node_modules/htmx.org/dist/htmx.min.js',
        'ui/src/static/app.js',
        'ui/src/static/jobs.page.js',
        'ui/src/static/schedules.page.js',
        'ui/src/static/event_targets.page.js',
    ]

    var js = ""

    jsPaths.forEach(file => {
        js += fs.readFileSync(file, {
            encoding: 'utf8'
        })
    })

    // Bundle JS to a single file
    try {
        fs.writeFileSync('ui/tmp/app.js', js)
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    // Bundle CSS to a single file and compress
    var cssPaths = [
        'ui/tmp/app.min.css',
        'node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css',
        'node_modules/@fortawesome/fontawesome-free/css/solid.min.css'
    ]

    var css = ""

    cssPaths.forEach(file => {
        css += fs.readFileSync(file, {
            encoding: 'utf8'
        })
    })

    try {
        fs.writeFileSync('ui/tmp/app.css', css)
    } catch (err) {
        console.log(err)
        process.exit(1)
    }

    var staticFonts =[
        'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.ttf',
        'node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2'
    ]

    staticFonts.forEach(file => {
        let start = file.lastIndexOf('/') + 1
        let origFilename = file.substring(start)

        let parts = origFilename.split('.')

        let fileContents = fs.readFileSync(file, {})

        newFilename = parts[0] + '_' + crypto.createHash('md5').update(fileContents).digest('hex') + '.' + parts[1]

        fs.copyFileSync(file, 'ui/dist/static/' + newFilename)

        let re = new RegExp('../webfonts/' + origFilename, 'g')
        replaceInFileSync({
            files: 'ui/tmp/app.css',
            from: re,
            to: newFilename,
        })
    })

    var staticFiles =[
        'ui/src/logo.svg',
        'ui/tmp/app.js',
        'ui/tmp/app.css',
    ]

    staticFiles.forEach(file => {
        let start = file.lastIndexOf('/') + 1
        let origFilename = file.substring(start)

        let parts = origFilename.split('.')

        let fileContents = fs.readFileSync(file, {})

        newFilename = parts[0] + '_' + crypto.createHash('md5').update(fileContents).digest('hex') + '.' + parts[1]

        fs.copyFileSync(file, 'ui/dist/static/' + newFilename)

        let re = new RegExp(origFilename, 'g')
        replaceInFileSync({
            files: 'ui/dist/index.html',
            from: re,
            to: '/static/' + newFilename,
        })

        replaceInFileSync({
            files: 'ui/dist/html/base.layout.html',
            from: re,
            to: '/static/' + newFilename,
        })
    })
}

build()
