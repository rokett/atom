@echo off
SETLOCAL

set APP=Atom
set VERSION=0.31.1
set BINARY-WINDOWS-X64=%APP%_Worker_%VERSION%_Windows_amd64.exe
set BINARY-LINUX=%APP%_Worker_%VERSION%_Linux_amd64

REM Set build number from git commit hash
for /f %%i in ('git rev-parse HEAD') do set BUILD=%%i

set LDFLAGS=-ldflags "-X main.version=%VERSION% -X main.build=%BUILD% -s -w -extldflags '-static'"

goto build

:build
    echo "=== Building Docker image ==="
    rem docker build --no-cache -t rokett/atom:latest .
    rem docker build --no-cache -t rokett/atom:v%VERSION% .
    rem docker push rokett/atom:v%VERSION%
    rem docker push rokett/atom:latest

    echo "=== Building executable ==="
    set GOARCH=amd64

    call npx tailwindcss -i ui/src/static/app.css -o ui/tmp/app.css

    call npx cleancss -o ui/tmp/app.min.css ui/tmp/app.css

    call npm run build

    rd %cd%\ui\tmp /s /q

    echo "=== Building Windows x64 ==="
    go build -mod=vendor %LDFLAGS% -o %APP%_Server_%VERSION%_Windows_amd64.exe .\cmd\server
    go build -mod=vendor %LDFLAGS% -o %BINARY-WINDOWS-X64% .\cmd\worker

    echo "=== Building Linux x64 ==="
    set GOOS=linux
    set GOARCH=amd64

    go build -mod=vendor %LDFLAGS% -o %BINARY-LINUX% .\cmd\worker

    rd %cd%\ui\dist /s /q

    goto :clean

:clean
    set VERSION=
    set BUILD=
    set LDFLAGS=
    set GOARCH=
    set GOOS=

    goto :EOF
